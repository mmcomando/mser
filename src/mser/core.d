// D stdc bindings don't play well with wasm target

module mser.core;

extern(C):

version(WebAssembly) {
private:
    void printf()(const char* format, ...) { }
    void free (void* ptr);
    void* malloc (size_t size);

public:
    char* strdup( const char* str1);
    double strtod (const char* str, char** endptr);
    size_t strlen (const char* str);
    void* memcpy (void* destination, const void* source, size_t num);
    void* memset (void* ptr, int value, size_t num);
    char* getenv (const char* name);

    alias logf = printf;
    alias std_free = free;
    alias std_malloc = malloc;
} else {
    public import core.stdc.stdio : logf = printf;
    public import core.stdc.stdlib : std_free = free, getenv, std_malloc = malloc, strtod;
    public import core.stdc.string : memcpy, memset, strdup, strlen;
    // public import core.sys.posix.dlfcn;
}


