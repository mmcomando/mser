module mser.backend.libxml2;

import mser;
import mser.core;
import mser.bindings.libxml2;
import mser.serializer_helper;
import mser.serializer_template;


alias Serializer = SerializerTemplate!(SerializerImpl, DeserializerImpl, properties);

extern(C) SerializerReturnCode mser_create_libxml2(SerializerHandle* handle, MserCreateInput* input){
    return create_serializer!(Serializer)(handle, input);
}

private:

enum SerializerProperties properties = SerializerProperties.canSerialize |
    SerializerProperties.canDeserialize |
    SerializerProperties.none;

enum max_depth = 20;

struct DepthData {
    int num; // Element number to load from array/object
    xmlNodePtr item;
    const(char)[] key;
    bool is_array = false;

    void add_item_to_array(xmlNodePtr item_to_add){
        xmlAddChild(item, item_to_add);
    }
    void add_item_to_object(const(char)[] name, xmlNodePtr item_to_add){
        xmlAddChild(item, item_to_add);
    }
    xmlNodePtr get_item_from_array(int index){
        return get_nth_child(item, index);
    }
    xmlNodePtr get_item_from_object(const(char)[] name){
        for (xmlNode* child = item.children; child; child = child.next)
        {
            const(char)[] cname = child.name[0..strlen(child.name)];
            if(cname == name){
                return child;
            }
        }
        return null;
    }

}
xmlNodePtr get_nth_child(xmlNodePtr item, int index){
    int i;
    for (xmlNode* child = item.children; child; child = child.next)
    {
        if(child.type != xmlElementType.XML_ELEMENT_NODE){
            continue;
        }
        if(i == index){
            return child;
        }
        i++;
    }
    return null;
}

int countElements(xmlNodePtr item){
    int num;
    for (xmlNode* child = item.children; child; child = child.next)
    {
        if(child.type != xmlElementType.XML_ELEMENT_NODE){
            continue;
        }
        num++;
    }
    return num;
}

struct SerializerImpl {
    MallocFunc malloc;
    FreeFunc free;
    ubyte[] out_data;
    ubyte[] out_buffer;

    DeptContainer!DepthData state;


    void initialize() {
        out_data = null;
        out_buffer = null;
        state.initialize();
    }
    void finalize(){
        xmlDocPtr doc = xmlNewDoc("1.0");
        xmlDocSetRootElement(doc, state.root);
        xmlChar *xmlbuff;
        int buffer_size;
        xmlDocDumpFormatMemory(doc, &xmlbuff, &buffer_size, 1);
        ensureHasCapacity(out_buffer, out_data, buffer_size + 1, malloc, free);
        out_data[buffer_size] = '\0';
        foreach(i, c; xmlbuff[0..buffer_size]) {
            out_data[i] = c;
        }
        out_data = out_buffer[buffer_size..$];
        xmlFree(xmlbuff);
        xmlFreeDoc(doc);
    }

    void saveKey(const(char)[] key){
        state.current.key = key;
    }

    void saveSimpleElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        if(name == null){
            name = element.id;
        }
        auto str = elementToVar!(char[])(element, data);
        xmlNodePtr n = xmlNewNode(null, name.ptr);
        xmlNodeSetContent(n, str.ptr);
        state.addItem(n, name);
        state.lowerLevel();
    }

    void saveStartContainer(const(char)[] name, ref SerializerMetaElement element) {
        if(state.current && state.current.key != null){
            name = state.current.key;
            state.current.key = null;
        }
        xmlNodePtr n;
        if(name.length == 0){
            n = xmlNewNode(null, element.id.ptr);
        }else{
            n = xmlNewNode(null, name.ptr);
            xmlNewProp(n, "id", element.id.ptr);
        }
        state.addItem(n, name);
        state.current.is_array = true;
    }

    void saveEndContainer() {
        state.lowerLevel();
    }

    void saveStartArray(const(char)[] name, ref SerializerMetaElement element, ref size_t length) {
        saveStartContainer(name, element);
    }

    void saveEndArray() {
        state.lowerLevel();
    }

    void saveString(const(char)[] name, ref SerializerMetaElement element, size_t length, const (char)* ptr){
        if(name == null){
            name = element.id;
        }
        xmlNodePtr n = xmlNewNode(null, name.ptr);
        xmlNodeSetContent(n, ptr);
        state.addItem(n, name);
        state.lowerLevel();
    }

    void saveStartMap(const(char)[] name, ref SerializerMetaElement element, size_t length){
        saveStartArray(name, element, length);
    }

    void saveEndMap() {
        state.lowerLevel();
    }
}

struct DeserializerImpl {
    MallocFunc malloc;
    FreeFunc free;
    DeptContainer!DepthData state;
    ubyte[] serialized_data;
    xmlDocPtr doc;

    void initialize(ubyte[] serialized_data) {
        state.initialize();

        this.serialized_data = serialized_data;

        assert(state.level == -1);
        doc = xmlReadMemory(cast(char*)serialized_data.ptr, cast(int)serialized_data.length, null, null, 0);
        state.root = xmlDocGetRootElement(doc);
    }

    void finalize(){
        xmlFreeDoc(doc);
    }

    const(char)* loadKey(int index){
        xmlNodePtr nth_item = get_nth_child(state.current.item, index);
        return strdup(nth_item.name);
    }

    bool loadSimpleElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element){
        xmlNodePtr item = state.getItem(name);
        if(item == null){
            return false;
        }
        auto str = xmlNodeGetContent(item);
        double num = strtod(str, null);
        xmlFree(str);
        varToElement(element, data, num);
        return true;
    }
    bool loadStartContainer(const(char)[] name, ref SerializerMetaElement element){
        xmlNodePtr item = state.getItem(name);

        if(item == null){
            return false;
        }
        state.upLevel(item);
        return true;
    }

    bool loadStartArray(const(char)[] name, ref SerializerMetaElement element, ref size_t length){
        xmlNodePtr item = state.getItem(name);
        int is_array = true;
        if(item == null) {
            return false;
        }

        length = countElements(item);
        state.upLevel(item);
        state.current.is_array = true;
        return true;
    }

    bool loadString(const(char)[] name, ref SerializerMetaElement element, ref size_t length, ref char* ptr){
        xmlNodePtr item = state.getItem(name);
        if(item == null) {
            return false;
        }
        auto str = xmlNodeGetContent(item);
        if(str == null){
            return false;
        }
        length = strlen(str);
        ptr = cast(char*)malloc(length + 1);
        memcpy(ptr, str, length);
        ptr[length] = '\0';
        xmlFree(str);
        return true;
    }

    bool loadStartMap(const(char)[] name, ref SerializerMetaElement element, ref size_t length){
        xmlNodePtr item = state.getItem(name);
        if(item == null) {
            return false;
        }
        length = countElements(item);

        state.upLevel(item);
        state.current.is_array = true;
        return true;
    }

    void loadEndContainer(){ state.lowerLevel(); }
    void loadEndArray(){ state.lowerLevel(); }
    void loadEndMap(){ state.lowerLevel(); }
}