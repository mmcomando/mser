module mser.binary_compatible;

import mser;
import mser.core;
import mser.serializer_helper;
import mser.serializer_template;


/// Create binary_compatible serializer handle
extern(C) SerializerReturnCode mser_create_embedded_binary_compatible(SerializerHandle* handle_out, MserCreateInput* input){
    auto my_malloc = (input.malloc) ? input.malloc : &std_malloc;
    auto my_free = (input.free) ? input.free : &std_free;

    void* memory = my_malloc(SerializerHandleImpl.sizeof + SerializerDeserializerImpl.sizeof);
    assert(((cast(size_t)memory + SerializerHandleImpl.sizeof) % SerializerDeserializerImpl.alignof) == 0);

    bool enable_optimizations = true;
     // Environment variable for to disable save/load optimizations
    const char* opt_str = getenv("MSER_BC_DISABLE_OPTIMIZATIONS");
    if(opt_str != null && strlen(opt_str) == 1 && opt_str[0] == '1'){
        // Disable optimizations only if MSER_BC_DISABLE_OPTIMIZATIONS=1
        enable_optimizations = 0;
    }
    assert(mser_meta_validate(input.meta) == SerializerReturnCode.ok);

    SerializerDeserializerImpl* backend_impl = cast(SerializerDeserializerImpl*)(memory + SerializerHandleImpl.sizeof);
    *backend_impl = SerializerDeserializerImpl.init;
    backend_impl.serializer.metas_runtime_api = input.meta;
    backend_impl.deserializer.metas_runtime_api = input.meta;
    backend_impl.serializer.free = my_free;
    backend_impl.deserializer.free = my_free;
    backend_impl.serializer.malloc = my_malloc;
    backend_impl.deserializer.malloc = my_malloc;
    backend_impl.serializer.enable_optimizations = enable_optimizations;
    backend_impl.deserializer.enable_optimizations = enable_optimizations;
    assert(input.meta.meta_elements.length > 0);
    backend_impl.serializer.initialize_serializer();
    backend_impl.deserializer.initialize_deserializer();

    SerializerHandleImpl* handle = cast(SerializerHandleImpl*)memory;
    *handle = SerializerHandleImpl.init;

    handle.backend_impl = cast(void*)backend_impl;
    handle.destroy = &mser_ebc_destroy;
    handle.serialize = &mser_ebc_serialize;
    handle.deserialize = &mser_ebc_deserialize;
    handle.has_properties = &mser_ebc_has_properties;
    *handle_out = handle;
    return SerializerReturnCode.ok;
}

/// Destroy binary_compatible serializer under handle
extern(C) SerializerReturnCode mser_ebc_destroy(SerializerHandle handle){
    SerializerDeserializerImpl* impl = cast(SerializerDeserializerImpl*)handle.backend_impl;
    impl.serializer.deinitialize_serializer();
    impl.deserializer.deinitialize_deserializer();
    handle.backend_impl = null;
    impl.serializer.free(handle);
    return SerializerReturnCode.ok;
}

/// Check properties
extern(C) SerializerReturnCode mser_ebc_has_properties(SerializerHandle handle, SerializerProperties properties){
    enum SerializerProperties propertiesImpl = SerializerProperties.canSerialize |
        SerializerProperties.canDeserialize |
        SerializerProperties.isBinary;

    if(cast(ulong)properties >= (SerializerProperties.max<< 1) ){
        return SerializerReturnCode.operationNotSupportedByImpl;
    }
    if((propertiesImpl & properties) != properties) {
        return SerializerReturnCode.propertiesNotSupported;
    }
    return SerializerReturnCode.ok;
}

/// Serialize
extern(C) SerializerReturnCode mser_ebc_serialize(SerializerHandle handle, SerializerInput* input, SerializerOutput* output){
    SerializerDeserializerImpl* impl = cast(SerializerDeserializerImpl*)handle.backend_impl;
    impl.serializer.user_data = input.user_data;
    impl.serializer.initialize_serialization();
    impl.serializer.serialize_value(input.meta_index, input.runtime_data_ptr);
    output.out_buffer = impl.serializer.out_buffer.ptr;
    output.out_buffer_length = impl.serializer.out_buffer.length - impl.serializer.out_data.length;
    return SerializerReturnCode.ok;
}

/// Deserialize
extern(C) SerializerReturnCode mser_ebc_deserialize(SerializerHandle handle, DeserializerInput* input, DeserializerOutput* output){
    SerializerDeserializerImpl* impl = cast(SerializerDeserializerImpl*)handle.backend_impl;
    impl.deserializer.user_data = input.user_data;
    SerializerReturnCode status_code = impl.deserializer.initialize_deserialization(input.in_buffer[0..input.in_buffer_length]);
    if(status_code != SerializerReturnCode.ok){
        return status_code;
    }
    status_code = impl.deserializer.deserialize_value(input.meta_index, input.runtime_data_ptr);
    if(status_code != SerializerReturnCode.ok){
        return status_code;
    }
    return SerializerReturnCode.ok;
}

/// Additional function to get public API meta from binary_compatible data
extern(C) SerializerMeta mser_embedded_binary_compatible_get_meta(ubyte* data_ptr, size_t data_length){
    MetaFormatElement[] metas_format = get_format_meta_from_data(data_ptr[0..data_length], &std_malloc);
    SerializerMeta serialize_meta = format_to_runtime_meta(metas_format, &std_malloc);
    free_metas(metas_format, &std_free);
    return serialize_meta;
}

private:

immutable(char[4]) format_header_bytes = "MBC\0"; /// MarvelousBinaryCompatible(format)
immutable(ubyte[3]) format_version = [0, 0, 2];

alias LengthSmallStr = ubyte;
alias CustomContainerIndex = ubyte;

/// Struct containing serializer and deserializer
struct SerializerDeserializerImpl{
    SerializerImpl serializer;
    DeserializerImpl deserializer;
}

/// Flags describing properties of object
/// Used to speed up serialization and deserialization
enum PropertyFlag : ubyte {
    none = 0,
    trivialCopy = 1,
    trivialCopyArray = 2,
    trivialCopyFromParent = 4, // Object is considered as trivialCopy by parent object (not the case when on_load is used)
    noTrivialCopyFromParent = 8,
    checked = 16,
}

/// Format specific meta description
struct MetaFormatElement {
    SerializerElementType type;
    PropertyFlag properties;
    MetaIndex runtime_meta_index = -1;
    int sizeTrivialCopy = -1;
    int size;
    int meta_save_size = -1;
    SerializerAllocationCallback allocate;
    SerializerFreeCallback free;
    MetaFormatSubobject[] subobjects;
    const(char)[] id;
}
/// Format specific subobject description
struct MetaFormatSubobject {
    int offset;
    int runtime_offset;
    MetaIndex meta_index;
    MetaIndex runtime_meta_index = -1;
    const(char)[] name;
    int meta_save_size = -1;
}

/// Create format specific meta description, from public API meta
MetaFormatElement[] runtime_to_format_meta(SerializerMeta* metas_runtime_api, MallocFunc malloc){
    int meta_elements_num = cast(int)metas_runtime_api.meta_elements.length;
    MetaFormatElement* b_meta_ptr = cast(MetaFormatElement*)malloc(meta_elements_num * MetaFormatElement.sizeof);
    MetaFormatElement[] metas_format = b_meta_ptr[0..meta_elements_num];
    foreach(ref m; metas_format) m=MetaFormatElement.init;

    foreach(i, ref runtime_meta_api; metas_runtime_api.meta_elements){
        MetaFormatElement* format_meta = &metas_format[i];
        format_meta.id = runtime_meta_api.id;
        format_meta.size = runtime_meta_api.size;
        format_meta.type = runtime_meta_api.type;
        format_meta.runtime_meta_index = cast(MetaIndex)i;
        format_meta.meta_save_size = 0;
        format_meta.meta_save_size += SerializerElementType.sizeof; // Format variable type
        format_meta.meta_save_size += 4; // Size of value
        format_meta.meta_save_size += LengthSmallStr.sizeof; // Size of value id length
        format_meta.meta_save_size += runtime_meta_api.id.length; // Value id
        assert(runtime_meta_api.id.length <= LengthSmallStr.max);
        format_meta.meta_save_size += 4; // Subobjects num

        format_meta.allocate = (runtime_meta_api.on_allocate) ? runtime_meta_api.on_allocate : &default_allocate;
        format_meta.free = (runtime_meta_api.on_free) ? runtime_meta_api.on_free : &default_free;

        size_t subobjects_num = runtime_meta_api.subobjects.length;
        MetaFormatSubobject* subobjects_ptr = cast(MetaFormatSubobject*)malloc(subobjects_num * MetaFormatSubobject.sizeof);
        format_meta.subobjects = subobjects_ptr[0..subobjects_num];
        foreach(ref m; format_meta.subobjects) m=MetaFormatSubobject.init;

        foreach(kk, ref runtime_subobject; runtime_meta_api.subobjects){
            MetaFormatSubobject* subobject_format = &format_meta.subobjects[kk];
            subobject_format.offset = runtime_subobject.offset;
            subobject_format.runtime_offset = runtime_subobject.offset;
            subobject_format.meta_index = runtime_subobject.meta_index;
            subobject_format.runtime_meta_index = runtime_subobject.meta_index;
            subobject_format.name = runtime_subobject.name;
            subobject_format.meta_save_size = 0;
            subobject_format.meta_save_size += MetaIndex.sizeof; // Meta index
            subobject_format.meta_save_size += 4; // Offset of value
            subobject_format.meta_save_size += LengthSmallStr.sizeof; // Size of subobject name length
            subobject_format.meta_save_size += runtime_subobject.name.length; // Subobject name
            assert(runtime_subobject.name.length <= LengthSmallStr.max);

            format_meta.meta_save_size += subobject_format.meta_save_size + 4;
        }
    }

    return metas_format;
}


// Helper functions
static uint loadInt(ref ubyte* ptr){
    uint val;
    memcpy(&val, ptr, 4);
    ptr += 4;
    return val;
}
static MetaIndex loadMetaIndex(ref ubyte* ptr){
    MetaIndex val;
    memcpy(&val, ptr, MetaIndex.sizeof);
    ptr += MetaIndex.sizeof;
    return val;
}
static ubyte loadUbyte(ref ubyte* ptr){
    ubyte val;
    memcpy(&val, ptr, 1);
    ptr += 1;
    return val;
}
static const(char)[] loadSmallStr(ref ubyte* ptr){
    LengthSmallStr size = loadUbyte(ptr);
    char* str_ptr = cast(char*)ptr;
    ptr += size;
    return str_ptr[0..size];
}


MetaFormatElement[] get_format_meta_from_data(ubyte[] data, MallocFunc malloc){
    if(data.length < 4 + 4 + 4 + 4){
        // fprintf(stderr, "Start of header to small\n");
        return null;
    }
    ubyte* ptr = data.ptr;

    // Title
    bool format_magic_ok = true;
    format_magic_ok &= ptr[0] == format_header_bytes[0];
    format_magic_ok &= ptr[1] == format_header_bytes[1];
    format_magic_ok &= ptr[2] == format_header_bytes[2];
    format_magic_ok &= ptr[3] == format_header_bytes[3];
    ptr += 4;
    if(format_magic_ok == false){
        // fprintf(stderr, "Wrong format magic number\n");
        return null;
    }

    // Version
    bool version_ok = true;
    version_ok &= ptr[0] == 0;
    version_ok &= ptr[1] == format_version[0];
    version_ok &= ptr[2] == format_version[1];
    version_ok &= ptr[3] == format_version[2];
    ptr += 4;
    if(version_ok == false){
        // fprintf(stderr, "Wrong format version\n");
        return null;
    }
    int header_size = loadInt(ptr);
    int meta_elements_num = loadInt(ptr);

    if(data.length < 4 + 4 + 4 + header_size){
        // fprintf(stderr, "Header to small\n");
        return null;
    }

    MetaFormatElement* b_meta_ptr = cast(MetaFormatElement*)malloc(meta_elements_num * MetaFormatElement.sizeof);
    MetaFormatElement[] metas_format = b_meta_ptr[0..meta_elements_num];
    foreach(ref m; metas_format) m=MetaFormatElement.init;

    foreach(data_meta_index, ref meta; metas_format){
        ubyte* ptr_meta = ptr;
        int meta_size = loadInt(ptr_meta);
        meta.type = cast(SerializerElementType)loadUbyte(ptr_meta);
        meta.size = loadInt(ptr_meta);
        meta.id = loadSmallStr(ptr_meta);

        int subobjects_num = loadInt(ptr_meta);
        MetaFormatSubobject* subobjects_ptr = cast(MetaFormatSubobject*)malloc(subobjects_num * MetaFormatSubobject.sizeof);
        meta.subobjects = subobjects_ptr[0..subobjects_num];
        foreach(ref m; meta.subobjects) m=MetaFormatSubobject.init;

        foreach(ref subobject; meta.subobjects){
            ubyte* ptr_subobject = ptr_meta;
            int subobject_size = loadInt(ptr_subobject);
            subobject.meta_index = loadMetaIndex(ptr_subobject);
            subobject.offset = loadInt(ptr_subobject);
            subobject.name = loadSmallStr(ptr_subobject);
            // subobject.meta_id = loadSmallStr(ptr_subobject);
            ptr_meta += subobject_size + 4;
        }

        ptr += meta_size + 4;
    }
    return metas_format;
}

const(char)[] copyStr(const(char)[] str, MallocFunc malloc){
    char*  copy_ptr = cast(char*)malloc(str.length + 1);
    memcpy(copy_ptr, str.ptr, str.length);
    copy_ptr[str.length] = '\0';
    const(char)[] copy = copy_ptr[0..str.length];
    return copy;

}

/// Create public API meta, from format specific meta
SerializerMeta format_to_runtime_meta(MetaFormatElement[] metas_format, MallocFunc malloc){
    SerializerMetaElement* b_meta_ptr = cast(SerializerMetaElement*)malloc(metas_format.length * SerializerMetaElement.sizeof);
    SerializerMetaElement[] metas_runtime = b_meta_ptr[0..metas_format.length];
    SerializerMeta serializer_meta = SerializerMeta(metas_runtime);
    foreach(ref m; metas_runtime) m = SerializerMetaElement.init;

    foreach(i, ref meta_runtime; metas_runtime){
        MetaFormatElement* meta_format = &metas_format[i];
        meta_runtime.meta_index = cast(MetaIndex)i;
        meta_runtime.id = copyStr(meta_format.id, malloc);
        meta_runtime.type = meta_format.type;
        meta_runtime.size = meta_format.size;
        meta_runtime.alignment = 1;

        size_t subobjects_num = meta_format.subobjects.length;
        SerializerSubobject* subobjects_ptr = cast(SerializerSubobject*)malloc(subobjects_num * SerializerSubobject.sizeof);
        meta_runtime.subobjects = subobjects_ptr[0..subobjects_num];
        foreach(ref m; meta_runtime.subobjects) m = SerializerSubobject.init;

        foreach(kk, ref subobject_runtime; meta_runtime.subobjects){
            MetaFormatSubobject* subobject_format = &meta_format.subobjects[kk];
            subobject_runtime.meta_id = null;
            subobject_runtime.name = copyStr(subobject_format.name, malloc);
            subobject_runtime.offset = subobject_format.offset;
            subobject_runtime.meta_index = subobject_format.meta_index;
        }
    }

    return serializer_meta;
}

void free_metas(MetaFormatElement[] metas, FreeFunc free){
    foreach(ref m; metas){
        free(m.subobjects.ptr);
    }
    free(metas.ptr);
}
void markPropertiesFromRuntimeMeta(ref SerializerMeta metas_runtime_api, MetaFormatElement[] metas, bool serialize) {
    // Mark elements which can not be trivially copied by parent (have their memory location taken from callback like: on_save)
    foreach(i, ref meta_runtime_api; metas_runtime_api.meta_elements){
        bool reduce_optimization = false;
        reduce_optimization |= serialize == true && meta_runtime_api.on_save;
        reduce_optimization |= serialize == false && meta_runtime_api.on_load;
        if(reduce_optimization){
            metas[i].properties |= PropertyFlag.noTrivialCopyFromParent;
        }
    }
}
/// Check if simple memcpy can be used to serialize/deserialize this object
void checkTrivialCopy(MetaFormatElement[] metas, MetaIndex meta_index){
    MetaFormatElement* meta = &metas[meta_index];
    if(meta.properties & PropertyFlag.checked){
        return;// Already checked
    }

    // Prevent infinite recurrence
    // It might prevent some properties
    meta.properties |= PropertyFlag.checked;

    // Helper function
    bool isTrivialCopyFromParent(MetaIndex index) {
        checkTrivialCopy(metas, index);
        return (metas[index].properties & PropertyFlag.trivialCopyFromParent) > 0;
    }

    PropertyFlag flags = cast(PropertyFlag)(PropertyFlag.trivialCopy | PropertyFlag.trivialCopyFromParent);
    if(meta.properties & PropertyFlag.noTrivialCopyFromParent){
        flags = PropertyFlag.trivialCopy;
    }

    switch(meta.type) {
        case SerializerElementType.signedInteger:
        case SerializerElementType.unsignedInteger:
        case SerializerElementType.floatingPointNumber:
        case SerializerElementType.binaryFixed:
            meta.properties |= flags;
            meta.sizeTrivialCopy = meta.size;
            return;
        default: break;
    }

    if(meta.type == SerializerElementType.arrayFixed){
        MetaFormatElement* submeta = &metas[meta.subobjects[0].meta_index];
        if(isTrivialCopyFromParent(meta.subobjects[0].meta_index) == false){
            return;
        }
        meta.properties |= flags;
        meta.sizeTrivialCopy = meta.size;
        return;
    }

    if(meta.type == SerializerElementType.container) {
        int total_size;
        foreach(ref subobject; meta.subobjects){
            MetaFormatElement* submeta = &metas[subobject.meta_index];
            if(isTrivialCopyFromParent(subobject.meta_index) == false){
                return;
            }
            total_size += submeta.size;
        }

        // TODO: This might not be enough to check if object is trivially copyable if there are memory overlaps in subobjects, prohibit it?
        if(total_size != meta.size){
            return; // If there are holes in container we cannot load it trivially
        }
        meta.properties |= flags;
        meta.sizeTrivialCopy = meta.size;
        return;
    }

    if(meta.type == SerializerElementType.array){
        MetaFormatElement* submeta = &metas[meta.subobjects[0].meta_index];
        if(isTrivialCopyFromParent(meta.subobjects[0].meta_index) == false){
            return;
        }
        // TODO: check alignment, ex. one byte variable with alignment(4)
        meta.properties |= PropertyFlag.trivialCopyArray;
        meta.sizeTrivialCopy = submeta.size;
        return;
    }
}

/// Implementation of serialization
struct SerializerImpl {
    SerializerMeta* metas_runtime_api;
    MetaFormatElement[] metas_format;
    MallocFunc malloc;
    FreeFunc free;
    bool enable_optimizations = true;

    ubyte[] out_data;
    ubyte[] out_buffer;

    void* user_data;
    ubyte[] header_data; /// Format header, constant for all serializations


    // Helper functions
    void copyInt(ref ubyte* ptr, uint val){
        memcpy(ptr, &val, 4);
        ptr += 4;
    }
    void copyMetaIndex(ref ubyte* ptr, MetaIndex val){
        memcpy(ptr, &val, MetaIndex.sizeof);
        ptr += MetaIndex.sizeof;
    }
    void copyUbyte(ref ubyte* ptr, ubyte val){
        memcpy(ptr, &val, 1);
        ptr += 1;
    }
    void copySmallStr(ref ubyte* ptr, const(char)[] val){
        assert(val.length <= LengthSmallStr.max);
        LengthSmallStr size = cast(LengthSmallStr)val.length;
        memcpy(ptr, &size, LengthSmallStr.sizeof);
        ptr += LengthSmallStr.sizeof;
        memcpy(ptr, val.ptr, size);
        ptr += size;
    }

    // Initialize serializer data, compute format meta
    void initialize_serializer(){
        metas_format = runtime_to_format_meta(metas_runtime_api, malloc);

        if(enable_optimizations){
            markPropertiesFromRuntimeMeta(*metas_runtime_api, metas_format, true);
            foreach(i, ref format_meta; metas_format){
                checkTrivialCopy(metas_format, cast(MetaIndex)i);
            }
        }

        int meta_elements_num = cast(int)metas_runtime_api.meta_elements.length;
        int header_size;
        header_size += 4; // Start bytes
        header_size += 4; // Version
        header_size += 4; // Size of meta without first 12 bytes (this size not included )
        header_size += 4; // Meta elements num
        foreach(i, ref meta_runtime_api; metas_runtime_api.meta_elements){
            header_size += 4; // Size of this meta element
            header_size += metas_format[i].meta_save_size;
        }
        header_size += 4; // Header and data separator

        ubyte* ptr = cast(ubyte*)malloc(header_size);
        header_data = ptr[0..header_size];
        foreach(ref b; header_data) b = 0;

        // Title
        memcpy(ptr, format_header_bytes.ptr, 4);
        ptr += 4;

        // Version
        ptr[0] = 0; // Reserved byte
        ptr[1] = format_version[0]; // Format version byte 0
        ptr[2] = format_version[1]; // Format version byte 1
        ptr[3] = format_version[2]; // Format version byte 2
        ptr += 4;

        // size_of_meta
        copyInt(ptr, header_size - 12);
        // meta_elements_num
        copyInt(ptr, meta_elements_num);

        // logf("meta_elements_num(%d)\n", cast(int)metas_runtime_api.meta_elements.length);
        foreach(i, ref meta_runtime_api; metas_runtime_api.meta_elements){
            // Size of this element
            copyInt(ptr, metas_format[i].meta_save_size);
            // Format variable type
            copyUbyte(ptr, meta_runtime_api.type);
            // Size of value
            copyInt(ptr, meta_runtime_api.size);
            // Value id
            copySmallStr(ptr, meta_runtime_api.id);
            // Number of subobjects
            copyInt(ptr, cast(int)meta_runtime_api.subobjects.length);

            foreach(kk, ref mser_subobject; meta_runtime_api.subobjects){
                // Size of this subobject
                copyInt(ptr, metas_format[i].subobjects[kk].meta_save_size);
                // Meta index
                copyMetaIndex(ptr, mser_subobject.meta_index);
                // Offset of value
                copyInt(ptr, mser_subobject.offset);
                // Subobject name
                copySmallStr(ptr, mser_subobject.name);
                // // Subobject meta_id
                // copySmallStr(ptr, mser_subobject.meta_id);
            }
        }

        // Separator
        ptr[0] = 0xFF;
        ptr[1] = 0xFF;
        ptr[2] = 0xFF;
        ptr[3] = 0xFF;
        ptr += 4;
        assert(ptr-header_data.ptr == header_size);
    }

    void deinitialize_serializer(){
        free_metas(metas_format, free);
        free(header_data.ptr);
    }

    void initialize_serialization(){
        out_data = null;
        out_buffer = null;
        ensureHasCapacity(out_buffer, out_data, header_data.length, malloc, free);
        memcpy(out_data.ptr, header_data.ptr, header_data.length);
        out_data = out_data[header_data.length..$];
    }

    void serializeSize(size_t length64){
        assert(length64 < int.max);
        int length= cast(int)length64;
        ensureHasCapacity(out_buffer, out_data, length.sizeof, malloc, free);
        memcpy(out_data.ptr, &length, length.sizeof);
        out_data = out_data[length.sizeof..$];
    }

    // Context struct for user callbacks
    static struct ContextCallbacks {
        SerializerImpl* serializer;
        MetaIndex meta_index_A;
        MetaIndex meta_index_B;
        size_t length;
    }


    extern(C) static SerializerReturnCode saveCustomStringCallback(SerializerImpl* serializer, ubyte* str, size_t length){
        serializer.serializeSize(length);
        ensureHasCapacity(serializer.out_buffer, serializer.out_data, length, serializer.malloc, serializer.free);
        memcpy(serializer.out_data.ptr, str, length);
        serializer.out_data = serializer.out_data[length..$];
        return SerializerReturnCode.ok;
    }

    extern(C) static SerializerReturnCode saveCustomArrayStartCallback(ContextCallbacks* context, size_t length){
        context.length = length;
        context.serializer.serializeSize(length);
        return SerializerReturnCode.ok;
    }

    extern(C) static SerializerReturnCode saveCustomArrayElementCallback(ContextCallbacks* context, void* runtime_data_ptr){
        context.serializer.serialize_value(context.meta_index_B, runtime_data_ptr);
        return SerializerReturnCode.ok;
    }

    extern(C) static SerializerReturnCode saveContainerCustomStartCallback(ContextCallbacks* context, size_t length){
        context.length = length;
        context.serializer.serializeSize(length);
        return SerializerReturnCode.ok;
    }

    extern(C) static SerializerReturnCode saveContainerCustomElementCallback(ContextCallbacks* context, MetaIndex name_index, void* runtime_data_ptr){
        SerializerMetaElement* map_meta = &context.serializer.metas_runtime_api.meta_elements[context.meta_index_A];
        SerializerSubobject* name_ref = &map_meta.subobjects[name_index];

        CustomContainerIndex index = cast(CustomContainerIndex)name_index;
        ensureHasCapacity(context.serializer.out_buffer, context.serializer.out_data, CustomContainerIndex.sizeof, context.serializer.malloc, context.serializer.free);
        memcpy(context.serializer.out_data.ptr, &index, CustomContainerIndex.sizeof);
        context.serializer.out_data = context.serializer.out_data[CustomContainerIndex.sizeof..$];

        context.serializer.serialize_value(name_ref.meta_index, runtime_data_ptr);
        return SerializerReturnCode.ok;
    }

    extern(C) static SerializerReturnCode saveCustomCallback(ContextCallbacks* context, void* runtime_data_ptr){
        context.serializer.serialize_value(context.meta_index_A, runtime_data_ptr, false);
        return SerializerReturnCode.ok;
    }

    void serialize_value(int meta_index, const(void)* runtime_data_ptr, bool callCustomCallback = true){
        SerializerMetaElement* meta_runtime = &metas_runtime_api.meta_elements[meta_index];
        MetaFormatElement* format_meta = metas_format.ptr + meta_index;
        if(meta_runtime.on_save && callCustomCallback){
            ContextCallbacks context = ContextCallbacks(&this, meta_runtime.meta_index); // Reuse context type
            SerializeCustomInput input = SerializeCustomInput(CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, cast(void*)runtime_data_ptr, user_data, &context));
            input.serialize = cast(SerializeLoadSaveCallback)&saveCustomCallback;
            meta_runtime.on_save(&input);
            return;
        }

        if((format_meta.properties & PropertyFlag.trivialCopy) > 0){
            int size = format_meta.sizeTrivialCopy;
            ensureHasCapacity(out_buffer, out_data, size, malloc, free);
            memcpy(out_data.ptr,runtime_data_ptr, size);
            out_data = out_data[size..$];
            return;
        }

        switch(meta_runtime.type) {
            case SerializerElementType.none: break;
            case SerializerElementType.signedInteger:
            case SerializerElementType.unsignedInteger:
            case SerializerElementType.floatingPointNumber:
            case SerializerElementType.binaryFixed:
                ensureHasCapacity(out_buffer, out_data, meta_runtime.size, malloc, free);
                memcpy(out_data.ptr, runtime_data_ptr, meta_runtime.size);
                out_data = out_data[meta_runtime.size..$];
                return;

            default: break;
        }

        if(meta_runtime.type == SerializerElementType.container){
            foreach(ref SerializerSubobject reference; meta_runtime.subobjects) {
                MetaIndex index = reference.meta_index;
                serialize_value(index, runtime_data_ptr + reference.offset);
            }
            return;
        }

        if(meta_runtime.type == SerializerElementType.array || meta_runtime.type == SerializerElementType.arrayFixed){
            SerializerSubobject* data_ref = &meta_runtime.subobjects[0];
            SerializerSubobject* length_ref = meta_runtime.subobjects.ptr + 1; // Exists only for array type
            MetaIndex index = data_ref.meta_index;
            SerializerMetaElement* data_element = &metas_runtime_api.meta_elements[index];

            int length;
            ubyte* values_ptr;
            if(meta_runtime.type == SerializerElementType.array){
                values_ptr = *cast(ubyte**)(runtime_data_ptr+data_ref.offset);
                length = *cast(int*)(runtime_data_ptr+length_ref.offset);
                serializeSize(length);
            }else if(meta_runtime.type == SerializerElementType.arrayFixed){
                values_ptr = cast(ubyte*)runtime_data_ptr;
                length = meta_runtime.size / data_element.size;
            }else{
                assert(0);
            }

            if((format_meta.properties & PropertyFlag.trivialCopyArray) > 0){
                int size = format_meta.sizeTrivialCopy * length;
                ensureHasCapacity(out_buffer, out_data, size, malloc, free);
                memcpy(out_data.ptr, values_ptr, size);
                out_data = out_data[size..$];
                return;
            }

            foreach(i; 0..length) {
                serialize_value(index, values_ptr + i*data_element.size);
            }
            return;
        }

        if(meta_runtime.type == SerializerElementType.arrayCustom){
            SerializerSubobject* data_ref = &meta_runtime.subobjects[0];
            MetaIndex index = data_ref.meta_index;
            ContextCallbacks context = ContextCallbacks(&this, meta_runtime.meta_index, index);

            SerializeArrayInput input = SerializeArrayInput(CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, cast(void*)runtime_data_ptr, user_data, &context));
            input.serialize_start = cast(SerializerLoadArrayStartCallback)&saveCustomArrayStartCallback;
            input.serialize_element = cast(SerializeLoadSaveArrayElementCallback)&saveCustomArrayElementCallback;

            meta_runtime.on_save_array(&input);
            return;
        }

        if(meta_runtime.type == SerializerElementType.string || meta_runtime.type == SerializerElementType.binary){
            SerializerSubobject* ptr_ref = &meta_runtime.subobjects[0];
            SerializerSubobject* length_ref = &meta_runtime.subobjects[1];

            size_t length = *cast(size_t*)(runtime_data_ptr + length_ref.offset);
            char* str = *cast(char**)(runtime_data_ptr + ptr_ref.offset);

            serializeSize(length);
            ensureHasCapacity(out_buffer, out_data, length, malloc, free);
            memcpy(out_data.ptr, str, length);
            out_data = out_data[length..$];
            return;
        }

        if(meta_runtime.type == SerializerElementType.stringCustom){
            SerializeStringInput input = SerializeStringInput(CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, cast(void*)runtime_data_ptr, user_data, &this));
            input.serialize_string = cast(SerializeSaveStringElementCallback)&saveCustomStringCallback;

            meta_runtime.on_save_string(&input);
            return;
        }

        if(meta_runtime.type == SerializerElementType.containerCustom){
            ContextCallbacks context = ContextCallbacks(&this, meta_runtime.meta_index, -1);

            SerializeContainerCustomInput input = SerializeContainerCustomInput(CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, cast(void*)runtime_data_ptr, user_data, &context));
            input.serialize_start = cast(SerializerSaveContainerStartCallback)&saveContainerCustomStartCallback;
            input.serialize_element = cast(SerializerSaveContainerNameElementCallback)&saveContainerCustomElementCallback;

            meta_runtime.on_save_container(&input);
            return;
        }

        if(meta_runtime.type == SerializerElementType.pointer){
            MetaFormatElement* pointee = &metas_format[meta_runtime.subobjects[0].meta_index];
            ubyte* pointer = *cast(ubyte**)runtime_data_ptr;
            ubyte present = (pointer) ? 1 : 0;

            ensureHasCapacity(out_buffer, out_data, 1, malloc, free);
            memcpy(out_data.ptr, &present, present.sizeof);
            out_data = out_data[present.sizeof..$];

            if(present == false) {
                return;
            }
            serialize_value(meta_runtime.subobjects[0].meta_index, pointer);
            return;
        }


        logf("serialize not compatible(%d)\n", meta_runtime.type);
    }
}

// Deserialization implementation
struct DeserializerImpl {
    SerializerMeta* metas_runtime_api; // Runtime meta described by public API
    MetaFormatElement[] metas_runtime; // Runtime meta described in format specific manner
    MetaFormatElement[] metas_format; // Meta taken from input data, described in format specific manner
    MallocFunc malloc;
    FreeFunc free;
    bool enable_optimizations = true;

    void* user_data;
    ubyte[] data;


    bool sameSubobject(ref MetaFormatSubobject subobject_format, ref MetaFormatSubobject runtime_subobject, bool check_name, bool check_offset, bool check_type){
        if(check_name && subobject_format.name != runtime_subobject.name){
            return false;
        }
        if(check_offset && subobject_format.offset != runtime_subobject.offset){
            return false;
        }
        if(check_type == false){
            return true;
        }
        if(subobject_format.runtime_meta_index == -1){
            return false;
        }

        MetaFormatElement* format_meta = &metas_format[subobject_format.meta_index];
        MetaFormatElement* meta_runtime = &metas_runtime[runtime_subobject.runtime_meta_index];

        if(format_meta.type != meta_runtime.type){
            return false;
        }
        return true;
    }

    // Check if two metas are the same, required to assure we don't do bad copies when trivialCopy property is set
    bool sameMeta(ref MetaFormatElement format_meta, ref MetaFormatElement meta_runtime){
        if(format_meta.type != meta_runtime.type){
            return false;
        }
        if(format_meta.subobjects.length != meta_runtime.subobjects.length){
            return false;
        }
        if(format_meta.sizeTrivialCopy != meta_runtime.sizeTrivialCopy){
            return false;
        }
        switch(format_meta.type){
            case SerializerElementType.none: return true;
            case SerializerElementType.signedInteger: return true;
            case SerializerElementType.unsignedInteger: return true;
            case SerializerElementType.floatingPointNumber: return true;

            case SerializerElementType.arrayCustom: return false;
            case SerializerElementType.stringCustom: return false;
            case SerializerElementType.containerCustom: return false;

            case SerializerElementType.container:
                foreach(i, ref subobject_format; format_meta.subobjects){
                    MetaFormatSubobject* runtime_subobject = &meta_runtime.subobjects[i];
                    if(sameSubobject(subobject_format, *runtime_subobject, true, true, true) == false){
                        return false;
                    }
                }
                break;
            case SerializerElementType.array:
                if(sameSubobject(format_meta.subobjects[0], meta_runtime.subobjects[0], false, false, true) == false){
                    return false;
                }
                break;
            case SerializerElementType.arrayFixed:
                if(sameSubobject(format_meta.subobjects[0], meta_runtime.subobjects[0], false, false, true) == false){
                    return false;
                }
                break;
            case SerializerElementType.string:
                if(sameSubobject(format_meta.subobjects[0], meta_runtime.subobjects[0], false, true, false) == false ||
                    sameSubobject(format_meta.subobjects[1], meta_runtime.subobjects[1], false, true, true) == false){
                    return false;
                }
                break;

            default: return false;
        }

        return true;
    }

    void checkProperties(){
        foreach(i, ref format_meta; metas_format){
            checkTrivialCopy(metas_format, cast(MetaIndex)i);
        }

        foreach(i, ref  MetaFormatElement format_meta; metas_format){
            if(format_meta.runtime_meta_index == -1){
                continue;
            }
            MetaFormatElement* meta_runtime = &metas_runtime[format_meta.runtime_meta_index];
            // Disable properties which are not present on both format and runtime sides
            format_meta.properties = format_meta.properties & meta_runtime.properties;
            if(sameMeta(format_meta, *meta_runtime) == false){
                format_meta.properties &= ~(PropertyFlag.trivialCopyArray | PropertyFlag.trivialCopyFromParent | PropertyFlag.trivialCopy);
                continue;
            }
        }
        // logf("--\n");
        // foreach(i, ref  MetaFormatElement format_meta; metas_format){
        //     logf("checked(%d), trivialCopySize(%2d), trivialCopy(%d), trivialCopyFromParent(%d), noTrivialCopyFromParent(%d), trivialCopyArray(%d), id(%.*s)\n",
        //         cast(int)((format_meta.properties & PropertyFlag.checked) > 0),
        //         format_meta.sizeTrivialCopy,
        //         cast(int)((format_meta.properties & PropertyFlag.trivialCopy) > 0),
        //         cast(int)((format_meta.properties & PropertyFlag.trivialCopyFromParent) > 0),
        //         cast(int)((format_meta.properties & PropertyFlag.noTrivialCopyFromParent) > 0),
        //         cast(int)((format_meta.properties & PropertyFlag.trivialCopyArray) > 0),
        //         cast(int)format_meta.id.length,
        //         format_meta.id.ptr,
        //     );
        // }
    }

    void updateMeta(ref MetaFormatElement meta){
        meta.runtime_meta_index = -1;
        foreach(i, ref meta_runtime_api; metas_runtime_api.meta_elements){
            if(meta_runtime_api.id == meta.id){
                meta.runtime_meta_index = cast(MetaIndex)i;
                meta.allocate = metas_runtime[i].allocate;
                meta.free = metas_runtime[i].free;

                switch(meta.type){
                    case SerializerElementType.none:
                    case SerializerElementType.signedInteger:
                    case SerializerElementType.unsignedInteger:
                    case SerializerElementType.floatingPointNumber:
                    case SerializerElementType.binaryFixed:
                    case SerializerElementType.stringFixed_reserved:
                    case SerializerElementType.pointer:
                        assert(meta_runtime_api.size == meta.size); // TODO: Integers conversion
                        break;
                    case SerializerElementType.string:
                    case SerializerElementType.arrayFixed:
                    case SerializerElementType.container:
                    case SerializerElementType.pointerCustom_reserved:
                    case SerializerElementType.stringCustom:
                    case SerializerElementType.containerCustom:
                    case SerializerElementType.arrayCustom:
                    case SerializerElementType.binaryCustom_reserved:
                    case SerializerElementType.binary:
                    case SerializerElementType.array:
                        break;
                    default:assert(0);
                }
                return;
            }

        }
    }

    void updateSubobject(MetaIndex meta_index_parent, ref MetaFormatSubobject sub){
        sub.runtime_meta_index = -1;
        if(meta_index_parent == -1){
            return;
        }
        SerializerElementType type_runtime = metas_runtime_api.meta_elements[meta_index_parent].type;
        bool load_meta_index = type_runtime == SerializerElementType.array || type_runtime == SerializerElementType.arrayFixed;

        // Subobject type is determined by meta index (ex. in arrays)
        if(load_meta_index){
            if(sub.meta_index == -1){
                return;
            }
            const(char)[] id = metas_format[sub.meta_index].id;
            int type  = metas_format[sub.meta_index].type;
            foreach(i, ref el; metas_runtime_api.meta_elements){
                if(el.id == id){
                    if(el.type == type){
                        sub.runtime_meta_index = cast(MetaIndex)i;
                    }
                    return;
                }
            }
            return;
        }
        // Subobject type is determined by subobject name (ex. in containers)
        foreach(i, ref subobject; metas_runtime_api.meta_elements[meta_index_parent].subobjects){
            if(subobject.name == sub.name){
                if(type_runtime == SerializerElementType.containerCustom){
                    sub.runtime_meta_index = cast(MetaIndex)i; // TODO: in containerCustom it is more like key, it is confusing, maybe improve it somehow?
                } else {
                    sub.runtime_meta_index = cast(MetaIndex)subobject.meta_index;
                }
                sub.runtime_offset = subobject.offset;
                return;
            }
        }
    }

    void initialize_deserializer(){
        metas_runtime = runtime_to_format_meta(metas_runtime_api, malloc);

        if(enable_optimizations){
            markPropertiesFromRuntimeMeta(*metas_runtime_api, metas_runtime, false);
            // Check properties of meta elements
            foreach(i, ref meta; metas_runtime){
                checkTrivialCopy(metas_runtime, cast(MetaIndex)i);
            }
        }
    }

    void deinitialize_deserializer(){
        free_metas(metas_runtime, free);
    }

    SerializerReturnCode initialize_deserialization(ubyte[] data_in){
        data = data_in;

        metas_format = get_format_meta_from_data(data, malloc);

        if(metas_format.length == 0){
            return SerializerReturnCode.notEnoughData;
        }

        foreach(ref meta; metas_format){
            updateMeta(meta);
        }
        foreach(ref meta; metas_format){
            foreach(ref subobject; meta.subobjects){
                updateSubobject(meta.runtime_meta_index, subobject);
            }
        }
        if(enable_optimizations){
            checkProperties();
        }
        ubyte* ptr = data.ptr + 8;
        int header_size = loadInt(ptr);
        data = data[header_size + 12 .. $];
        return SerializerReturnCode.ok;
    }

    SerializerReturnCode deserialize_value(MetaIndex meta_index, void* runtime_data_ptr){
        foreach(i, ref meta; metas_format){
            if (meta.runtime_meta_index == meta_index){
                SerializerReturnCode status_code = deserialize_value_impl(cast(MetaIndex)i, cast(MetaIndex)i, runtime_data_ptr);
                if(status_code == SerializerReturnCode.ok){
                    assert(data.length == 0);
                }
                free_metas(metas_format, free);
                metas_format = null;
                return status_code;
            }
        }
        return SerializerReturnCode.metaNotFound;
    }

    static struct ContextCallbacks {
        DeserializerImpl* serializer;
        MetaIndex meta_index_A;
        MetaIndex meta_index_B;
        size_t length;
        SerializerReturnCode status_code = SerializerReturnCode.ok;
    }

    extern(C) static SerializerReturnCode loadCustomArrayElementCallback(ContextCallbacks* context, void* runtime_data_ptr){
        context.status_code = context.serializer.deserialize_value_impl(context.meta_index_B, context.serializer.metas_format[context.meta_index_B].runtime_meta_index, runtime_data_ptr);
        return context.status_code;
    }

    extern(C) static SerializerReturnCode loadContainerCustomNameCallback(ContextCallbacks* context, MetaIndex* name_index){
        CustomContainerIndex index;
        if(context.status_code != SerializerReturnCode.ok || context.serializer.data.length < index.sizeof){
            context.status_code = SerializerReturnCode.notEnoughData;
            context.meta_index_B = -1;
            *name_index = -1;
            return context.status_code;
        }
        memcpy(&index, context.serializer.data.ptr, index.sizeof);
        context.serializer.data = context.serializer.data[index.sizeof..$];
        context.meta_index_B = index;

        MetaFormatElement* map_meta = &context.serializer.metas_format[context.meta_index_A];
        MetaIndex runtime_meta_index = map_meta.subobjects[context.meta_index_B].runtime_meta_index;
        *name_index = runtime_meta_index;
        context.status_code = SerializerReturnCode.ok;
        return context.status_code;
    }

    extern(C) static SerializerReturnCode loadContainerCustomElementCallback(ContextCallbacks* context, void* runtime_data_ptr){
        if(context.status_code != SerializerReturnCode.ok || context.meta_index_B  == -1){
            context.status_code = SerializerReturnCode.notEnoughData;
            return context.status_code;
        }
        MetaFormatElement* map_meta = &context.serializer.metas_format[context.meta_index_A];
        MetaFormatElement* map_meta_runtime = &context.serializer.metas_runtime[map_meta.runtime_meta_index];
        MetaIndex meta_index = map_meta.subobjects[context.meta_index_B].meta_index;
        MetaIndex meta_index_runtime;
        if(map_meta.subobjects[context.meta_index_B].runtime_meta_index == -1 || runtime_data_ptr is null)meta_index_runtime = -1;
        else meta_index_runtime = map_meta_runtime.subobjects[map_meta.subobjects[context.meta_index_B].runtime_meta_index].meta_index;
        context.status_code = context.serializer.deserialize_value_impl(meta_index, meta_index_runtime, runtime_data_ptr);
        return context.status_code;
    }

    extern(C) static SerializerReturnCode loadCustomCallback(ContextCallbacks* context, void* runtime_data_ptr){
        context.status_code = context.serializer.deserialize_value_impl(context.meta_index_A, context.serializer.metas_format[context.meta_index_A].runtime_meta_index, runtime_data_ptr, false);
        return context.status_code;
    }

    SerializerReturnCode deserialize_value_impl(MetaIndex meta_index, MetaIndex runtime_meta_index, void* runtime_data_ptr, bool callCustomCallback = true){
        MetaFormatElement* meta = metas_format.ptr + meta_index;
        if(runtime_meta_index == -1){
            SerializerReturnCode status_code = skipData(meta_index);
            return status_code;
        }
        SerializerMetaElement* meta_runtime = metas_runtime_api.meta_elements.ptr + runtime_meta_index;
        if(callCustomCallback && runtime_data_ptr && meta_runtime.on_load){
            ContextCallbacks context = ContextCallbacks(&this, meta_index);
            DeserializeCustomInput input = DeserializeCustomInput(CallbackCommonData(metas_runtime_api, meta_index, runtime_data_ptr, user_data, &context));
            input.deserialize = cast(SerializeLoadSaveCallback)&loadCustomCallback;
            meta_runtime.on_load(&input);
            return context.status_code;
        }
        scope(exit){
            if(runtime_data_ptr && meta_runtime.on_load_finish){
                meta_runtime.on_load_finish(metas_runtime_api, runtime_data_ptr, runtime_meta_index, user_data);
            }
        }

        bool same_meta = meta.runtime_meta_index == runtime_meta_index;
        if(same_meta && (meta.properties & PropertyFlag.trivialCopy) > 0){
            if(data.length < meta.sizeTrivialCopy){
                return SerializerReturnCode.notEnoughData;
            }
            memcpy(runtime_data_ptr, data.ptr, meta.sizeTrivialCopy);
            data = data[meta.sizeTrivialCopy..$];
            return SerializerReturnCode.ok;
        }

        SerializerReturnCode delegate(MetaIndex meta_index, MetaIndex runtime_meta_index, void* runtime_data_ptr) deleg;

        switch(meta.type) {
            case SerializerElementType.none: break;
            case SerializerElementType.signedInteger:
            case SerializerElementType.unsignedInteger:
            case SerializerElementType.floatingPointNumber:
            case SerializerElementType.binaryFixed:
                if(data.length < meta.size){
                    return SerializerReturnCode.notEnoughData;
                }
                convertNumberToNumber(meta.type, meta.size, data.ptr, meta_runtime.type, meta_runtime.size, runtime_data_ptr);
                data = data[meta.size..$];
                return SerializerReturnCode.ok;

            case SerializerElementType.containerCustom:
                deleg = &loadContainerCustom;
                break;
            case SerializerElementType.container:
                deleg = &loadContainer;
                break;
            case SerializerElementType.string:
            case SerializerElementType.binary:
                deleg = &loadString;
                break;
            case SerializerElementType.stringCustom:
                deleg = &loadStringCustom;
                break;
            case SerializerElementType.array:
            case SerializerElementType.arrayFixed:
            case SerializerElementType.arrayCustom:
                deleg = &loadArray;
                break;
            case SerializerElementType.pointer:
                deleg = &loadPointer;
                break;

            default:
                return SerializerReturnCode.metaElementNotFound;
        }
        assert(runtime_meta_index != -1);
        assert(runtime_data_ptr != null);
        SerializerReturnCode status_code = deleg(meta_index, runtime_meta_index, runtime_data_ptr);
        if(status_code != SerializerReturnCode.ok){
            return status_code;
        }

        return SerializerReturnCode.ok;
    }

    SerializerReturnCode skipData(MetaIndex meta_index) {
        MetaFormatElement* meta = metas_format.ptr + meta_index;

        switch(meta.type) {
            case SerializerElementType.none: break;
            case SerializerElementType.signedInteger:
            case SerializerElementType.unsignedInteger:
            case SerializerElementType.floatingPointNumber:
            case SerializerElementType.binaryFixed:
            case SerializerElementType.arrayFixed:
                if(data.length < meta.size){
                    return SerializerReturnCode.notEnoughData;
                }
                data = data[meta.size..$];
                return SerializerReturnCode.ok;
            case SerializerElementType.containerCustom:
                int length;
                if(data.length < length.sizeof){
                    return SerializerReturnCode.notEnoughData;
                }
                memcpy(&length, data.ptr, length.sizeof);
                data = data[length.sizeof..$];
                foreach(i; 0..length) {
                    CustomContainerIndex name_index;
                    if(data.length < name_index.sizeof){
                        return SerializerReturnCode.notEnoughData;
                    }
                    memcpy(&name_index, data.ptr, name_index.sizeof);
                    data = data[name_index.sizeof..$];
                    SerializerReturnCode status_code = skipData(meta.subobjects[name_index].meta_index);
                    if(status_code != SerializerReturnCode.ok){
                        return status_code;
                    }
                }
                return SerializerReturnCode.ok;
            case SerializerElementType.container:
                foreach(ref MetaFormatSubobject reference; meta.subobjects) {
                    SerializerReturnCode status_code = skipData(reference.meta_index);
                    if(status_code != SerializerReturnCode.ok){
                        return status_code;
                    }
                }
                return SerializerReturnCode.ok;
            case SerializerElementType.string:
            case SerializerElementType.binary:
            case SerializerElementType.stringCustom:
                int length;
                if(data.length < length.sizeof){
                    return SerializerReturnCode.notEnoughData;
                }
                memcpy(&length, data.ptr, length.sizeof);
                data = data[length.sizeof..$];
                if(data.length < length){
                    return SerializerReturnCode.notEnoughData;
                }
                data = data[length..$];
                return SerializerReturnCode.ok;
            case SerializerElementType.array:
            case SerializerElementType.arrayCustom:
                int length;
                if(data.length < length.sizeof){
                    return SerializerReturnCode.notEnoughData;
                }
                memcpy(&length, data.ptr, length.sizeof);
                data = data[length.sizeof..$];
                foreach(i; 0..length) {
                    SerializerReturnCode status_code = skipData(meta.subobjects[0].meta_index);
                    if(status_code != SerializerReturnCode.ok){
                        return status_code;
                    }
                }
                return SerializerReturnCode.ok;
            case SerializerElementType.pointer:
                ubyte present;
                if(data.length < present.sizeof){
                    return SerializerReturnCode.notEnoughData;
                }
                memcpy(&present, data.ptr, present.sizeof);
                data = data[present.sizeof..$];

                if(present == false) {
                    return SerializerReturnCode.ok;
                }

                SerializerReturnCode status_code = skipData(meta.subobjects[0].meta_index);
                if(status_code != SerializerReturnCode.ok){
                    return status_code;
                }
                return SerializerReturnCode.ok;

            default:
                break;
        }
        return SerializerReturnCode.metaElementNotFound;
    }

    SerializerReturnCode loadContainer(MetaIndex meta_index, MetaIndex runtime_meta_index, void* runtime_data_ptr) {
        MetaFormatElement* meta = metas_format.ptr + meta_index;
        SerializerMetaElement* meta_runtime = metas_runtime_api.meta_elements.ptr + meta.runtime_meta_index;

        // Clear data before use, as some fields might not be used
        // TODO: Probably not all fields should be cleared, only missing ones
        memset(runtime_data_ptr, 0, meta_runtime.size);

        foreach(ddd, ref MetaFormatSubobject reference; meta.subobjects) {
            bool has_runtime = reference.runtime_meta_index != -1;
            SerializerReturnCode status_code;
            if(has_runtime){
                void* ptr = runtime_data_ptr + reference.runtime_offset;
                status_code = deserialize_value_impl(reference.meta_index, reference.runtime_meta_index, ptr);
            }else{
                status_code = skipData(reference.meta_index);
            }
            if(status_code != SerializerReturnCode.ok){
                foreach(ref MetaFormatSubobject reference_destroy; meta.subobjects[0..ddd]){
                    MetaIndex index_destroy = reference_destroy.meta_index;
                    void* ptr_destroy;
                    if(reference_destroy.runtime_meta_index != -1){
                        ptr_destroy = runtime_data_ptr + reference_destroy.runtime_offset;
                    }
                    destroy_value(index_destroy, ptr_destroy);
                }
                return status_code;
            }
        }
        return SerializerReturnCode.ok;
    }

    // Load array/arrayCustom to array/arrayFixed/arrayCustom
    SerializerReturnCode loadArray(MetaIndex meta_index, MetaIndex runtime_meta_index, void* runtime_data_ptr) {
        MetaFormatElement* meta = metas_format.ptr + meta_index;
        SerializerMetaElement* meta_runtime = metas_runtime_api.meta_elements.ptr + runtime_meta_index;

        MetaFormatSubobject* data_ref = meta.subobjects.ptr + 0;
        MetaFormatElement* data_format = metas_format.ptr + data_ref.meta_index;
        MetaFormatSubobject* length_ref = meta.subobjects.ptr + 1; // Exists only if format type is array

        SerializerSubobject* data_ref_runtime = meta_runtime.subobjects.ptr + 0;
        SerializerMetaElement* data_runtime = metas_runtime_api.meta_elements.ptr + data_ref_runtime.meta_index;
        SerializerSubobject* length_ref_runtime = meta_runtime.subobjects.ptr + 1; // Exists only if runtime type is array
        MetaIndex runtime_data_index = data_ref_runtime.meta_index;

        int length; // How many elements are saved in input data
        if(meta.type == SerializerElementType.array || meta.type == SerializerElementType.arrayCustom) {
            if(data.length < length.sizeof){
                return SerializerReturnCode.notEnoughData;
            }
            memcpy(&length, data.ptr, length.sizeof);
            data = data[length.sizeof..$];
        } else {
            length = meta.size / data_format.size;
        }

        ubyte* array_data_ptr;
        int length_load; // How many elements we should load to runtime data
        if(meta_runtime.type == SerializerElementType.array) {
            CallbackCommonData cd = CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, runtime_data_ptr, user_data, malloc);
            array_data_ptr = cast(ubyte*)(meta.allocate(&cd, length*data_runtime.size));
            memset(array_data_ptr, 0, length*data_runtime.size);
            length_load = length;
        } else if(meta_runtime.type == SerializerElementType.arrayFixed) {
            array_data_ptr = cast(ubyte*)runtime_data_ptr;
            length_load = meta_runtime.size / data_runtime.size;
        } else if(meta_runtime.type == SerializerElementType.arrayCustom){
            MetaIndex index = data_ref.meta_index;
            ContextCallbacks context = ContextCallbacks(&this, meta_index, index, length);

            DeserializeArrayInput input = DeserializeArrayInput(CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, runtime_data_ptr, user_data, &context));
            input.length = length;
            input.deserialize_element = cast(SerializeLoadSaveArrayElementCallback)&loadCustomArrayElementCallback;

            meta_runtime.on_load_array(&input);
            return context.status_code;
        } else{
            assert(0);
        }

        int length_walk_over; // How many elements we should skip from input data
        if(length_load > length){
            length_load = length;
        }else{
            length_walk_over = length - length_load;

        }

        bool same_meta = meta.runtime_meta_index == runtime_meta_index;
        if(same_meta && (meta.properties & PropertyFlag.trivialCopyArray) > 0){
            size_t bytes_to_copy = meta.sizeTrivialCopy * length_load;
            if(data.length < bytes_to_copy){
                if(meta.type == SerializerElementType.array){
                    CallbackCommonData cd = CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, runtime_data_ptr, user_data, free);
                    meta.free(&cd, array_data_ptr);
                }
                return SerializerReturnCode.notEnoughData;
            }
            memcpy(array_data_ptr, data.ptr, bytes_to_copy);
            memset(array_data_ptr + bytes_to_copy, 0, meta.sizeTrivialCopy * length_walk_over);
            if(meta_runtime.type == SerializerElementType.array){
                *cast(size_t*)&runtime_data_ptr[length_ref_runtime.offset] = length;
                *cast(void**)&runtime_data_ptr[data_ref_runtime.offset] = array_data_ptr;
            }
            data = data[meta.sizeTrivialCopy * length..$];

            return SerializerReturnCode.ok;
        }

        foreach(i; 0..length_load) {
            ubyte* ptr = array_data_ptr + i*data_runtime.size;
            SerializerReturnCode status_code = deserialize_value_impl(data_ref.meta_index, runtime_data_index, ptr);
            if(status_code != SerializerReturnCode.ok){
                foreach(ddd; 0..i) {
                    ubyte* ptr_destroy = (array_data_ptr) ? array_data_ptr + ddd*data_runtime.size : null;
                    if(ddd >= length_load){
                        ptr_destroy = null;
                    }
                    destroy_value(data_ref.meta_index, ptr_destroy);
                }
                if(meta.type == SerializerElementType.array){
                    CallbackCommonData cd = CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, runtime_data_ptr, user_data, free);
                    meta.free(&cd, array_data_ptr);
                }
                return status_code;
            }
        }
        foreach(i; 0..length_walk_over) {
            SerializerReturnCode status_code = skipData(data_ref.meta_index);
            if(status_code != SerializerReturnCode.ok){
                foreach(ddd; 0..length_load) {
                    ubyte* ptr_destroy = (array_data_ptr) ? array_data_ptr + ddd*data_runtime.size : null;
                    if(ddd >= length_load){
                        ptr_destroy = null;
                    }
                    destroy_value(data_ref.meta_index, ptr_destroy);
                }
                if(meta.type == SerializerElementType.array){
                    CallbackCommonData cd = CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, runtime_data_ptr, user_data, free);
                    meta.free(&cd, array_data_ptr);
                }
                return status_code;
            }
        }

        if(meta_runtime.type == SerializerElementType.array){
            *cast(size_t*)&runtime_data_ptr[length_ref_runtime.offset] = length;
            *cast(void**)&runtime_data_ptr[data_ref_runtime.offset] = array_data_ptr;
        }

        return SerializerReturnCode.ok;
    }

    SerializerReturnCode loadString(MetaIndex meta_index, MetaIndex runtime_meta_index, void* runtime_data_ptr) {
        MetaFormatElement* meta = metas_format.ptr + meta_index;
        SerializerMetaElement* meta_runtime = metas_runtime_api.meta_elements.ptr + runtime_meta_index;

        bool add_null = meta.type == SerializerElementType.string;
        char** str = cast(char**)(runtime_data_ptr + meta_runtime.subobjects[0].offset);
        size_t* length_location = cast(size_t*)(runtime_data_ptr + meta_runtime.subobjects[1].offset);


        int length;
        if(data.length < length.sizeof){
            return SerializerReturnCode.notEnoughData;
        }
        memcpy(&length, data.ptr, length.sizeof);
        if(length>0){
            if(data.length < length.sizeof + length){
                return SerializerReturnCode.notEnoughData;
            }
            CallbackCommonData cd = CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, runtime_data_ptr, user_data, malloc);
            *str = cast(char*)meta.allocate(&cd, length + cast(int)add_null);
            memcpy(*str, data.ptr + length.sizeof, length);
            if(add_null) (*str)[length] = '\0';
        }else{
            *str = null;
        }
        data = data[length.sizeof + length..$];
        *length_location = length;
        return SerializerReturnCode.ok;
    }

    SerializerReturnCode loadStringCustom(MetaIndex meta_index, MetaIndex runtime_meta_index, void* runtime_data_ptr) {
        MetaFormatElement* meta = metas_format.ptr + meta_index;
        SerializerMetaElement* meta_runtime = metas_runtime_api.meta_elements.ptr + runtime_meta_index;

        int length;
        char* str = cast(char*)data.ptr + length.sizeof;

        if(data.length < length.sizeof){
            return SerializerReturnCode.notEnoughData;
        }
        memcpy(&length, data.ptr, length.sizeof);
        if(data.length < length.sizeof + length){
            return SerializerReturnCode.notEnoughData;
        }
        data = data[length.sizeof + length..$];

        DeserializeStringInput input = DeserializeStringInput(CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, runtime_data_ptr, user_data, null));
        input.length = length;
        input.str = str;

        meta_runtime.on_load_string(&input);
        return SerializerReturnCode.ok;
        }

    SerializerReturnCode loadContainerCustom(MetaIndex meta_index, MetaIndex runtime_meta_index, void* runtime_data_ptr) {
        MetaFormatElement* meta = metas_format.ptr + meta_index;
        SerializerMetaElement* meta_runtime = metas_runtime_api.meta_elements.ptr + runtime_meta_index;

        int length;
        if(data.length < length.sizeof){
            return SerializerReturnCode.notEnoughData;
        }
        memcpy(&length, data.ptr, length.sizeof);
        data = data[length.sizeof..$];

        ContextCallbacks context = ContextCallbacks(&this, meta_index, -1, length);

        DeserializeContainerCustomInput input = DeserializeContainerCustomInput(CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, runtime_data_ptr, user_data, &context));
        input.length = length;
        input.deserialize_name = cast(SerializerLoadContainerNameCallback)&loadContainerCustomNameCallback;
        input.deserialize_element = cast(SerializerLoadContainerElementCallback)&loadContainerCustomElementCallback;

        meta_runtime.on_load_container(&input);
        return context.status_code;
    }

    SerializerReturnCode loadPointer(MetaIndex meta_index, MetaIndex runtime_meta_index, void* runtime_data_ptr) {
        MetaFormatElement* meta = metas_format.ptr + meta_index;
        SerializerMetaElement* meta_runtime = metas_runtime_api.meta_elements.ptr + runtime_meta_index;

        ubyte** pointer = cast(ubyte**)runtime_data_ptr;
        ubyte present;
        if(data.length < present.sizeof){
            return SerializerReturnCode.notEnoughData;
        }
        memcpy(&present, data.ptr, present.sizeof);
        data = data[present.sizeof..$];

        *pointer = null;
        if(present == false) {
            return SerializerReturnCode.ok;
        }

        MetaFormatElement* pointee = &metas_format[meta.subobjects[0].meta_index];

        CallbackCommonData cd = CallbackCommonData(metas_runtime_api, meta_runtime.meta_index, runtime_data_ptr, user_data, malloc);
        ubyte* pointee_data_ptr = cast(ubyte*)meta.allocate(&cd, pointee.size);
        memset(pointee_data_ptr, 0, pointee.size);

        SerializerReturnCode status_code = deserialize_value_impl(meta.subobjects[0].meta_index, meta.subobjects[0].runtime_meta_index, pointee_data_ptr);
        if(status_code != SerializerReturnCode.ok){
            cd.context = free;
            meta.free(&cd, pointee_data_ptr);
            return status_code;
        }
        *pointer = pointee_data_ptr;
        return SerializerReturnCode.ok;
    }

    SerializerReturnCode destroy_value(MetaIndex meta_index, void* runtime_data_ptr){
        if(runtime_data_ptr == null){
            return SerializerReturnCode.ok;
        }
        MetaFormatElement* meta = metas_format.ptr + meta_index;
        if(meta.runtime_meta_index == -1){
            return SerializerReturnCode.ok;
        }

        return mser_destroy_variable(metas_runtime_api, meta.runtime_meta_index, runtime_data_ptr, free);
    }
}
