module mser.backend.json;

import mser;
import mser.core;
import mser.serializer_helper;
import mser.serializer_template;


alias Serializer = SerializerTemplate!(SerializerImpl, DeserializerImpl, properties);

extern(C) SerializerReturnCode mser_create_embedded_json(SerializerHandle* handle, MserCreateInput* input){
    return create_serializer!(Serializer)(handle, input);
}

private:

enum SerializerProperties properties = SerializerProperties.canSerialize;

struct SerializerImpl {
    __gshared string spaces = "                                                ";
    MallocFunc malloc;
    FreeFunc free;
    ubyte[] out_buffer;
    ubyte[] out_data;
    int level;

    void initialize() {}
    void finalize(){}

    void saveKey(const(char)[] key){
        to_buffer(spaces[0..2*level], '"', key, "\":\n");
    }

    bool saveSimpleElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        long num = 0;
        memcpy(&num, data.ptr, element.size);
        if(name.length){
            to_buffer(spaces[0..2*level], '"', name, "\":", num, ",\n");
        }else{
            to_buffer(spaces[0..2*level], num, ",\n");
        }
        return true;
    }

    bool saveStartContainer(const(char)[] name, ref SerializerMetaElement element) {
        if(name.length){
            to_buffer(spaces[0..2*level], '"', name, "\": {\n");
        }else{
            to_buffer(spaces[0..2*level], "{\n");
        }




        // ensureHasCapacity(out_buffer, out_data, 1000, malloc, free);
        // print_to_buffer_and_advance(out_data, "aaaa", 123);
        level++;
        return true;
    }

    void saveEndContainer() {
        level--;
        to_buffer(spaces[0..2*level], "},\n");
    }

    bool saveStartArray(const(char)[] name, ref SerializerMetaElement element, ref size_t length) {
        if(name.length){
            to_buffer(spaces[0..2*level], '"', name, "\": [\n");
        }else{
            to_buffer(spaces[0..2*level], "[\n");
        }
        level++;
        return true;
    }

    void saveEndArray() {
        level--;
        to_buffer(spaces[0..2*level], "],\n");
    }

    void saveString(const(char)[] name, ref SerializerMetaElement element, size_t length, const (char)* ptr){
        char[2] ending = [',', 0];
        if(name.length){
            to_buffer(spaces[0..2*level], '"', name, "\": ", '"', ptr[0..length], "\",\n");
        }else{
            to_buffer(spaces[0..2*level], '"', ptr[0..length], "\",\n");
        }
    }

    void saveStartMap(const(char)[] name, ref SerializerMetaElement element, size_t length){
        saveStartContainer(name, element);
    }

    void saveEndMap() {
        saveEndContainer();
    }

    void to_buffer(Args...)(Args args){
        size_t cap = get_safe_print_capacity(args);
        ensureHasCapacity(out_buffer, out_data, cap, malloc, free);
        print_to_buffer_and_advance(out_data, args);
    }
}

// Not supported
struct DeserializerImpl {
    MallocFunc malloc;
    FreeFunc free;
    void initialize(ubyte[] out_data) {}
    void finalize(){}
    const(char)* loadKey(int index){return null;}
    bool loadSimpleElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element){return true;}
    bool loadStartContainer(const(char)[] name, ref SerializerMetaElement element){return true;}
    void loadEndContainer(){}
    bool loadStartArray(const(char)[] name, ref SerializerMetaElement element, ref size_t length){return true;}
    void loadEndArray(){}
    bool loadString(const(char)[] name, ref SerializerMetaElement element, ref size_t length, ref char* ptr){return true;}
    bool loadStartMap(const(char)[] name, ref SerializerMetaElement element, ref size_t length){return true;}
    void loadEndMap(){}
}
