module mser.backend.binary;

import mser;
import mser.core;
import mser.serializer_helper;
import mser.serializer_template;


alias Serializer = SerializerTemplate!(SerializerImpl, DeserializerImpl, properties);

extern(C) SerializerReturnCode mser_create_embedded_binary(SerializerHandle* handle, MserCreateInput* input){
    return create_serializer!(Serializer)(handle, input);
}
private:

enum SerializerProperties properties = SerializerProperties.canSerialize |
    SerializerProperties.canDeserialize |
    SerializerProperties.isBinary;

struct SerializerImpl {
    MallocFunc malloc;
    FreeFunc free;
    ubyte[] out_data;
    ubyte[] out_buffer;


    void initialize() {
        out_data = null;
        out_buffer = null;
    }
    void finalize(){}

    void saveSimpleElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        assert(element.type == SerializerElementType.signedInteger || element.type == SerializerElementType.unsignedInteger|| element.type == SerializerElementType.floatingPointNumber);
        ensureHasCapacity(out_buffer, out_data, element.size, malloc, free);
        memcpy(out_data.ptr, data.ptr, element.size);
        out_data = out_data[element.size..$];
    }

    void saveStartContainer(const(char)[] name, ref SerializerMetaElement element) {}
    void saveEndContainer() {}

    void saveKey(const(char)[] key){
        saveStringImpl(key.length, key.ptr);
    }

    void saveStartArray(const(char)[] name, ref SerializerMetaElement element, ref size_t length) {
        assert(length <= int.max);
        ensureHasCapacity(out_buffer, out_data, int.sizeof, malloc, free);
        int len = cast(int)length;
        memcpy(out_data.ptr, &len, int.sizeof);
        out_data = out_data[int.sizeof..$];
    }

    void saveEndArray() {}

    void saveStringImpl(size_t length, const (char)* ptr){
        assert(length <= int.max);
        ensureHasCapacity(out_buffer, out_data, int.sizeof + length, malloc, free);
        int len = cast(int)length;
        memcpy(out_data.ptr, &len, int.sizeof);
        memcpy(out_data.ptr + int.sizeof, ptr, len);
        out_data = out_data[(int.sizeof + len)..$];
    }
    void saveString(const(char)[] name, ref SerializerMetaElement element, size_t length, const (char)* ptr){
        saveStringImpl(length, ptr);
    }

    void saveStartMap(const(char)[] name, ref SerializerMetaElement element, size_t length){
        saveStartArray(name, element, length);
    }

    void saveEndMap() {}
}


struct DeserializerImpl {
    MallocFunc malloc;
    FreeFunc free;
    ubyte[] serialized_data;

    void initialize(ubyte[] serialized_data) {
        this.serialized_data = serialized_data;
    }

    void finalize(){}

    bool loadSimpleElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element){
        assert(element.type == SerializerElementType.signedInteger || element.type == SerializerElementType.unsignedInteger || element.type == SerializerElementType.floatingPointNumber);
        memcpy(data.ptr, serialized_data.ptr, element.size);
        serialized_data = serialized_data[element.size..$];
        return true;
    }

    bool loadStartContainer(const(char)[] name, ref SerializerMetaElement element){return true;}
    void loadEndContainer(){}

    const(char)* loadKey(int index){
        size_t length;
        char* ptr;
        loadStringImpl(length, ptr);
        return ptr;
    }

    bool loadStartArray(const(char)[] name, ref SerializerMetaElement element, ref size_t length){
        int len;
        memcpy(&len, serialized_data.ptr, int.sizeof);
        length = len;
        serialized_data = serialized_data[int.sizeof..$];
        return true;
    }

    void loadEndArray(){}

    void loadStringImpl(ref size_t length, ref char* ptr){
        int len;
        memcpy(&len, serialized_data.ptr, int.sizeof);
        length = len;

        ptr = cast(char*)malloc(length + 1);
        memcpy(ptr, serialized_data.ptr + int.sizeof, length);
        ptr[length] = '\0';
        serialized_data = serialized_data[(int.sizeof + len)..$];
    }

    bool loadString(const(char)[] name, ref SerializerMetaElement element, ref size_t length, ref char* ptr){
        loadStringImpl(length, ptr);
        return true;
    }

    bool loadStartMap(const(char)[] name, ref SerializerMetaElement element, ref size_t length){
        return loadStartArray(name, element, length);
    }

    void loadEndMap(){}
}
