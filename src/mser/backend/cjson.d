module mser.backend.cjson;

import mser;
import mser.core;
import mser.bindings.cjson;
import mser.serializer_helper;
import mser.serializer_template;


alias Serializer = SerializerTemplate!(SerializerImpl, DeserializerImpl, properties);

extern(C) SerializerReturnCode mser_create_cjson(SerializerHandle* handle, MserCreateInput* input){
    return create_serializer!(Serializer)(handle, input);
}

private:

alias Binder = LibBinder!(mser.bindings.cjson, false);
// alias Binder = LibBinder!(mser.bindings.cjson, true);

enum SerializerProperties properties = SerializerProperties.canSerialize |
        SerializerProperties.supportsNotPresentData |
        SerializerProperties.canDeserialize;



struct DepthData {
    int num; // Element number to load from array/object
    cJSON* item;
    const(char)[] key;
    bool is_array = false;

    void add_item_to_array(cJSON* item_to_add){
        Binder.cJSON_AddItemToArray(item, item_to_add);
    }
    void add_item_to_object(const(char)[] name, cJSON* item_to_add){
        Binder.cJSON_AddItemToObject(item, name.ptr, item_to_add);
    }
    cJSON* get_item_from_array(int index){
        return Binder.cJSON_GetArrayItem(item, index);
    }
    cJSON* get_item_from_object(const(char)[] name){
        return Binder.cJSON_GetObjectItemCaseSensitive(item, name.ptr);
    }

}

struct SerializerImpl {
    MallocFunc malloc;
    FreeFunc free;
    ubyte[] out_data;
    ubyte[] out_buffer;
    DeptContainer!DepthData state;

    void initialize() {
        out_data = null;
        out_buffer = null;
        Binder.initialize("/nix/store/cvx7rdkjlqp2nb32y8dz63c3v6mwvhf1-cjson-1.7.14/lib/libcjson.so");
        state.initialize();
    }

    void finalize(){
        auto text = Binder.cJSON_Print(state.root);
        size_t size = strlen(text);
        ensureHasCapacity(out_buffer, out_data, cast(int)size + 1, malloc, free);
        memcpy(out_buffer.ptr, text, size);
        out_data = out_buffer[size..$];
        out_buffer[size] = 0;
        Binder.cJSON_free(text);
        Binder.cJSON_Delete(state.root);
    }


    void saveKey(const(char)[] key){
        state.current.key = key;
    }

    void saveSimpleElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element){
        cJSON* item;
        if(element.type==SerializerElementType.floatingPointNumber){
            double num;
            convertNumberToNumber(element.type, element.size, data.ptr, element.type, num.sizeof, &num);
            item = Binder.cJSON_CreateNumber(num);
        }else{
            long num;
            convertNumberToNumber(element.type, element.size, data.ptr, element.type, num.sizeof, &num);
            item = Binder.cJSON_CreateNumber(num);
        }
        state.addItem(item, name);
        state.lowerLevel();
    }

    void saveStartContainer(const(char)[] name, ref SerializerMetaElement element){
        cJSON* item = Binder.cJSON_CreateObject();
        state.addItem(item, name);
    }

    void saveEndContainer(){
        state.lowerLevel();
    }

    void saveStartArray(const(char)[] name, ref SerializerMetaElement element, ref size_t length){
        cJSON* item = Binder.cJSON_CreateArray();
        state.addItem(item, name);
        state.current.is_array = true;
    }

    void saveEndArray(){
        state.lowerLevel();
    }

    void saveString(const(char)[] name, ref SerializerMetaElement element, size_t length, const (char)* ptr){
        char[1024] buf = void;
        char* buf_malloc;
        scope(exit)free(buf_malloc);
        char* cstr_ptr;
        if(length < buf.length) {
            cstr_ptr = buf.ptr;
        } else {
            buf_malloc = cast(char*)malloc(length + 1);
            cstr_ptr = buf_malloc;
        }

        memcpy(cstr_ptr, ptr, length);
        cstr_ptr[length] = '\0';
        cJSON* item = Binder.cJSON_CreateString(cstr_ptr);
        state.addItem(item, name);
        state.lowerLevel();
    }

    void saveStartMap(const(char)[] name, ref SerializerMetaElement element, size_t length){
        cJSON* item = Binder.cJSON_CreateObject();
        state.addItem(item, name);
    }

    void saveEndMap(){
        state.lowerLevel();
    }
}

struct DeserializerImpl {
    MallocFunc malloc;
    FreeFunc free;
    ubyte[] serialized_data;
    DeptContainer!DepthData state;

    void initialize(ubyte[] serialized_data) {
        Binder.initialize("/nix/store/cvx7rdkjlqp2nb32y8dz63c3v6mwvhf1-cjson-1.7.14/lib/libcjson.so");
        state.initialize();

        this.serialized_data = serialized_data;
        // assert(state.level == -1);
        cJSON* item = Binder.cJSON_ParseWithLength(cast(char*)serialized_data.ptr, serialized_data.length);
        state.root = item;
    }

    void finalize(){
        Binder.cJSON_Delete(state.root);
    }

    const(char)* loadKey(int index){
        cJSON* current = state.current.item;
        // assert(current != null && Binder.cJSON_IsObject(current));
        cJSON* item = Binder.cJSON_GetArrayItem(current, index);
        state.current.num = index;
        if(item == null){
            return null;
        }
        return strdup(item.string_);
    }

    bool loadSimpleElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element){
        cJSON* item = state.getItem(name);
        if(item == null){
            return false;
        }
        double num = Binder.cJSON_GetNumberValue(item);
        varToElement(element, data, num);
        return true;
    }

    bool loadStartContainer(const(char)[] name, ref SerializerMetaElement element){
        cJSON* item = state.getItem(name);

        if(item == null){
            return false;
        }
        state.upLevel(item);
        return true;

    }

    bool loadStartArray(const(char)[] name, ref SerializerMetaElement element, ref size_t length){
        cJSON* item = state.getItem(name);
        int is_array = Binder.cJSON_IsArray(item);
        if(item == null || is_array == 0) {
            return false;
        }
        length = Binder.cJSON_GetArraySize(item);
        state.upLevel(item);
        state.current.is_array = true;
        return true;
    }

    bool loadString(const(char)[] name, ref SerializerMetaElement element, ref size_t length, ref char* ptr){
        cJSON* item = state.getItem(name);
        if(item == null) {
            return false;
        }
        const (char)* str = Binder.cJSON_GetStringValue(item);
        if(str == null){
            return false;
        }
        length = strlen(str);
        ptr = cast(char*)malloc(length + 1);
        memcpy(ptr, str, length);
        ptr[length] = '\0';
        return true;
    }

    bool loadStartMap(const(char)[] name, ref SerializerMetaElement element, ref size_t length){
        cJSON* item = state.getItem(name);
        if(item == null) {
            return false;
        }
        length = Binder.cJSON_GetArraySize(item);

        state.upLevel(item);
        state.current.is_array = true;
        return true;
    }

    void loadEndContainer(){ state.lowerLevel(); }
    void loadEndArray(){ state.lowerLevel(); }
    void loadEndMap(){ state.lowerLevel(); }
}
