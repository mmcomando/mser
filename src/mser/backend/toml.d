module mser.backend.toml;

import mser;
import mser.core;
import mser.serializer_helper;
import mser.serializer_template;


alias Serializer = SerializerTemplate!(SerializerImpl, DeserializerImpl, properties);

extern(C) SerializerReturnCode mser_create_toml(SerializerHandle* handle, MserCreateInput* input){
    return create_serializer!(Serializer)(handle, input);
}

private:

enum SerializerProperties properties = SerializerProperties.canSerialize;

enum ItemType{
    none,
    string_,
    integer,
    float_,
    array,
    container,
}
struct Item2{
    ItemType type;
    Item2*[] subitems;
    const(char)[] key;
    long val_int;
    double val_float;
    char[] val_str;

    void destroy(){
        foreach(ref Item2* item; subitems){
            item.destroy();
        }

        std_free(val_str.ptr);
        std_free(subitems.ptr);
        std_free(&this);
    }
}

Item2* alloc_item(ItemType type){
    Item2* item = cast(Item2*)std_malloc(Item2.sizeof);
    *item = Item2.init;
    item.type = type;
    return item;
}


struct MyDepthContainer {
    enum max_depth = 40;
    Item2*[max_depth] depth_data;
    Item2* root;
    Item2* current;
    const(char)[] next_key;
    int level;


    void initialize(){
        foreach(ref el; depth_data) el = null;
        level=-1;
        root = null;
        current = null;
    }

    void upLevel(Item2* item){
        level++;
        depth_data[level] = item;
        current = item;
    }

    void lowerLevel(){
        current = null;
        level--;
        current = (level >= 0) ? depth_data[level] : null;
    }

    void addItem(Item2* item, const(char)[] name){
        assert(item.type != ItemType.none);
        if(level < 0){
            root = item;
            upLevel(item);
            return;
        }
        if(next_key != null){
            name = current.key;
            name = next_key;
            next_key = null;
        }
        if(current.type == ItemType.array){
            assert(name == null);
        } else if(current.type == ItemType.container){
            assert(name != null);
        } else {
            assert(0, "Add item to item which can't store other objects");
        }
        item.key = name;
        append_to_arr(current.subitems, item);
        if(is_simple_var(item.type) == false){
            upLevel(item);
        }
    }
}


bool is_leaf(Item2* item){
    if(is_simple_var(item.type)){
        return true;
    }
    if(item.type != ItemType.array){
        return false;
    }
    if(item.subitems.length == 0){
        return true;
    }
    return is_leaf(item.subitems[0]);
}

bool contains_dot(const(char)[] str){
    foreach(c; str){
        if(c == '.'){
            return true;
        }
    }
    return false;
}


bool is_simple_var(ItemType type){
    switch(type){
    case ItemType.integer:
    case ItemType.float_:
    case ItemType.string_:
        return true;
    default:
        break;
    }
    return false;
}

struct SerializerImpl {
    MyDepthContainer state;
    MallocFunc malloc;
    FreeFunc free;
    ubyte[] out_buffer;
    ubyte[] out_data;

    void print_item(ref Item2 item, bool full_name, bool only_val = false){
        int count_leafs(Item2*[] subitems){
            int num;
            foreach(ref Item2* subitem; item.subitems){
                num += is_leaf(subitem);
            }
            return num;
        }
        if(item.type == ItemType.container || item.type == ItemType.array){
            if(item.subitems.length == 0){
                return;
            }
        }
        if(is_simple_var(item.type)){
            if(only_val == false){
                if(state.level >= 1)to_buffer("  ");
                to_buffer(item.key, " = ");
            }
            if(item.type == ItemType.string_){
                to_buffer('"', item.val_str, '"');
            } else if(item.type == ItemType.integer){
                to_buffer(item.val_int);
            } else if(item.type == ItemType.float_){
                to_buffer(item.val_float);
            }

            if(only_val == false){
                to_buffer("\n");
            }
        }
        if(item.type == ItemType.container){
            state.upLevel(&item);
            if(full_name && count_leafs(item.subitems) > 0) print_full_name(false, true);

            foreach(ref Item2* subitem; item.subitems){
                if(is_leaf(subitem)){
                    print_item(*subitem, false);
                }
            }
            foreach(ref Item2* subitem; item.subitems){
                if(is_leaf(subitem) == false){
                    print_item(*subitem, true);
                }
            }
            state.lowerLevel();
        }
        if(item.type == ItemType.array){
            bool subs_are_value = is_leaf(item.subitems[0]);
            if(subs_are_value){
                if(full_name) print_full_name(false, true);
                if(item.key.length) to_buffer("  ", item.key, " = ");
                to_buffer("[");

                foreach(i, ref Item2* subitem; item.subitems){
                    if(i > 0){
                        to_buffer(", ");
                    }
                    print_item(*subitem, false, true);
                }

                to_buffer("]");
                if(item.key.length) to_buffer("\n");
                return;
            }

            state.upLevel(&item);

            to_buffer("\n");
            foreach(ref Item2* subitem; item.subitems){
                if(full_name) print_full_name(true, true);
                print_item(*subitem, false);
                to_buffer("\n");
            }
            state.lowerLevel();
        }

    }

    void initialize() {
        out_data = null;
        out_buffer = null;
        state.initialize();
    }
    void finalize(){
        to_buffer("\n");
        print_item(*state.root, true);
        state.root.destroy();
    }


    void print_full_name(bool array, bool new_line){
        if(state.level <= 0){
            return;
        }
        if(array)to_buffer("[[");
        if(!array)to_buffer("[");

        bool last_was_arr = false;
        foreach(i, Item2* item; state.depth_data[0..state.level + 1]){
            if(i == 0){
                continue;
            }

            if(last_was_arr){
                if(item.type != ItemType.array){
                last_was_arr = false;
                }
                continue;
            }

            if(item.type != ItemType.array){
                last_was_arr = false;
            } else{
                last_was_arr = true;

            }
            if(i > 1){
                to_buffer(".");
            }
            bool add_par = contains_dot(item.key);
            if(add_par)to_buffer("\"");
            to_buffer(item.key);
            if(add_par)to_buffer("\"");

        }
        if(array)to_buffer("]]");
        if(!array)to_buffer("]");
        if(new_line)to_buffer("\n");

    }

    void saveKey(const(char)[] key){
        state.next_key = key;
    }

    void saveSimpleElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        Item2* item = alloc_item(ItemType.none);
        if(element.type == SerializerElementType.signedInteger || element.type == SerializerElementType.unsignedInteger){
            long num = 0;
            memcpy(&num, data.ptr, element.size);
            item.val_int = num;
            item.type = ItemType.integer;
        } else if(element.type == SerializerElementType.floatingPointNumber){
            if(element.size == 4){
                float num = 0;
                memcpy(&num, data.ptr, element.size);
                item.val_float = num;
            }else if(element.size == 8){
                double num = 0;
                memcpy(&num, data.ptr, element.size);
                item.val_float = num;
            }else{
                assert(0);
            }
            item.type = ItemType.float_;
        }
        state.addItem(item, name);
    }

    void saveStartContainer(const(char)[] name, ref SerializerMetaElement element) {
        Item2* item = alloc_item(ItemType.container);
        state.addItem(item, name);
    }

    void saveStartArray(const(char)[] name, ref SerializerMetaElement element, ref size_t length) {
        Item2* item = alloc_item(ItemType.array);
        state.addItem(item, name);
    }

    void saveString(const(char)[] name, ref SerializerMetaElement element, size_t length, const (char)* ptr){
        Item2* item = alloc_item(ItemType.string_);
        state.addItem(item, name);

        char* copy = cast(char*)malloc(length+1);
        memcpy(copy, ptr, length);
        copy[length] = 0;
        item.val_str = copy[0..length];
    }

    void saveStartMap(const(char)[] name, ref SerializerMetaElement element, size_t length){
        saveStartContainer(name, element);
    }


    void saveEndArray() {state.lowerLevel(); }
    void saveEndContainer() {state.lowerLevel(); }
    void saveEndMap() {state.lowerLevel(); }

    void to_buffer(Args...)(Args args) {
        size_t cap = get_safe_print_capacity(args);
        ensureHasCapacity(out_buffer, out_data, cap, malloc, free);
        print_to_buffer_and_advance(out_data, args);
    }
}

// Not supported
struct DeserializerImpl {
    MallocFunc malloc;
    FreeFunc free;
    void initialize(ubyte[] out_data) {}
    void finalize(){}
    const(char)* loadKey(int index){return null;}
    bool loadSimpleElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element){return true;}
    bool loadStartContainer(const(char)[] name, ref SerializerMetaElement element){return true;}
    void loadEndContainer(){}
    bool loadStartArray(const(char)[] name, ref SerializerMetaElement element, ref size_t length){return true;}
    void loadEndArray(){}
    bool loadString(const(char)[] name, ref SerializerMetaElement element, ref size_t length, ref char* ptr){return true;}
    bool loadStartMap(const(char)[] name, ref SerializerMetaElement element, ref size_t length){return true;}
    void loadEndMap(){}
}
