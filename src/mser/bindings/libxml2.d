// Minimal hand written bindings to make a POC xml serializer
module mser.bindings.libxml2;


extern(C):

// Functions to save data to xml document
xmlNodePtr xmlNewNode(xmlNsPtr ns,const xmlChar *name);
void xmlNodeSetContent(xmlNodePtr cur, const xmlChar *content);
xmlDocPtr xmlNewDoc(const xmlChar *version_);
xmlNodePtr xmlDocSetRootElement (xmlDocPtr doc, xmlNodePtr root);
void xmlDocDumpFormatMemory(xmlDocPtr cur, xmlChar **mem, int *size, int format);
xmlNodePtr xmlNewChild (xmlNodePtr parent, xmlNsPtr ns, const xmlChar * name, const xmlChar * content);
xmlNodePtr xmlAddChild(xmlNodePtr parent, xmlNodePtr cur);
xmlNodePtr xmlNewTextChild(xmlNodePtr parent, xmlNsPtr ns, const xmlChar * name, const xmlChar * content);
xmlAttrPtr xmlNewProp(xmlNodePtr node, const xmlChar * name, const xmlChar * value);

// Functions to load data from xml document
xmlDocPtr xmlReadMemory  (const char * buffer, int size, const char * URL, const char * encoding, int options);
xmlNodePtr xmlDocGetRootElement (const xmlDoc * doc);
xmlChar * xmlNodeGetContent (const xmlNode * cur);

// Free functions
void xmlFreeDoc(xmlDocPtr cur);
// xmlFree might have different forms based on libxml2 build options, but this works for nixos build
alias xmlFreeFunc = void function(void*);
__gshared extern xmlFreeFunc xmlFree;

// Basic types
struct xmlNs;
struct xmlAttr;
struct xmlDoc;

alias xmlNsPtr = void*;
alias xmlNodePtr = xmlNode*;
alias xmlDocPtr = xmlDoc*;
alias xmlAttrPtr = xmlAttr*;
alias xmlChar = char;

struct xmlNode {
    void * _private;
    xmlElementType type;
    const xmlChar * name;
    xmlNode * children;
    xmlNode * last;
    xmlNode * parent;
    xmlNode * next;
    xmlNode * prev;
    xmlDoc * doc;
    xmlNs * ns;
    xmlChar * content;
    xmlAttr * properties;
    xmlNs * nsDef;
    void * psvi;
    ushort line;
    ushort extra;
}

enum xmlParserOption {
    XML_PARSE_RECOVER = 1,
    XML_PARSE_NOENT = 2,
    XML_PARSE_DTDLOAD = 4,
    XML_PARSE_DTDATTR = 8,
    XML_PARSE_DTDVALID = 16,
    XML_PARSE_NOERROR = 32,
    XML_PARSE_NOWARNING = 64,
    XML_PARSE_PEDANTIC = 128,
    XML_PARSE_NOBLANKS = 256,
    XML_PARSE_SAX1 = 512,
    XML_PARSE_XINCLUDE = 1024,
    XML_PARSE_NONET = 2048,
    XML_PARSE_NODICT = 4096,
    XML_PARSE_NSCLEAN = 8192,
    XML_PARSE_NOCDATA = 16384,
    XML_PARSE_NOXINCNODE = 32768,
    XML_PARSE_COMPACT = 65536,
    XML_PARSE_OLD10 = 131072,
    XML_PARSE_NOBASEFIX = 262144,
    XML_PARSE_HUGE = 524288,
    XML_PARSE_OLDSAX = 1048576,
    XML_PARSE_IGNORE_ENC = 2097152,
    XML_PARSE_BIG_LINES = 4194304,
}
enum xmlElementType {
    XML_ELEMENT_NODE = 1,
    XML_ATTRIBUTE_NODE = 2,
    XML_TEXT_NODE = 3,
    XML_CDATA_SECTION_NODE = 4,
    XML_ENTITY_REF_NODE = 5,
    XML_ENTITY_NODE = 6,
    XML_PI_NODE = 7,
    XML_COMMENT_NODE = 8,
    XML_DOCUMENT_NODE = 9,
    XML_DOCUMENT_TYPE_NODE = 10,
    XML_DOCUMENT_FRAG_NODE = 11,
    XML_NOTATION_NODE = 12,
    XML_HTML_DOCUMENT_NODE = 13,
    XML_DTD_NODE = 14,
    XML_ELEMENT_DECL = 15,
    XML_ATTRIBUTE_DECL = 16,
    XML_ENTITY_DECL = 17,
    XML_NAMESPACE_DECL = 18,
    XML_XINCLUDE_START = 19,
    XML_XINCLUDE_END = 20,
    XML_DOCB_DOCUMENT_NODE = 21,
}
