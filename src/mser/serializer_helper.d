/// Useful functions for backend implementations
module mser.serializer_helper;

import std.algorithm : max;
import std.meta;
import std.traits;

import mser;
import mser.core;

package:

SerializerHandleImpl* createImpl(T)(MserCreateInput* input){
    alias Bridge = SerializerHandleImplBridge!T;

    auto my_malloc = (input.malloc) ? input.malloc : &std_malloc;
    auto my_free = (input.free) ? input.free : &std_free;

    T* backend_impl = cast(T*)my_malloc(T.sizeof);
    *backend_impl = T.init;
    backend_impl.meta = input.meta;
    backend_impl.free = my_free;
    backend_impl.malloc =  my_malloc;

    assert(input.meta.meta_elements.length > 0);

    SerializerHandleImpl* handle = cast(SerializerHandleImpl*)my_malloc(SerializerHandleImpl.sizeof);
    *handle = SerializerHandleImpl.init;

    handle.backend_impl = cast(void*)backend_impl;
    handle.destroy = &Bridge.destroy;
    handle.serialize = &Bridge.serialize;
    handle.deserialize = &Bridge.deserialize;
    handle.has_properties = &Bridge.has_properties;
    return handle;
}


struct SerializerHandleImplBridge(T){
    extern(C):

    static SerializerReturnCode destroy(SerializerHandle handle){
        T* impl = cast(T*)handle.backend_impl;
        auto free = impl.free;
        free(cast(void*)handle.backend_impl);
        free(cast(void*)handle);
        return SerializerReturnCode.ok;
    }
    static SerializerReturnCode serialize(SerializerHandle handle, SerializerInput* input, SerializerOutput* output){
        T* impl = cast(T*)handle.backend_impl;
        return impl.serialize(input, output);
    }

    static SerializerReturnCode deserialize(SerializerHandle handle, DeserializerInput* input, DeserializerOutput* output){
        T* impl = cast(T*)handle.backend_impl;
        return impl.deserialize(input, output);
    }

    static SerializerReturnCode has_properties(SerializerHandle handle, SerializerProperties properties){
        T* impl = cast(T*)handle.backend_impl;

        if(cast(ulong)properties >= (SerializerProperties.max<< 1) ){
            return SerializerReturnCode.operationNotSupportedByImpl;
        }
        if((impl.properties & properties) != properties) {
            return SerializerReturnCode.propertiesNotSupported;
        }
        return SerializerReturnCode.ok;
    }
}

struct DeptContainer(T) {
    alias Item = typeof(T.item);
    enum max_depth = 40;
    T[max_depth] depth_data;
    T* current;
    int level;
    Item root;

    void initialize(){
        foreach(ref el; depth_data) el = T.init;
        level=-1;
        root = null;
        current = null;
    }

    void upLevel(Item item){
        level++;
        current = &depth_data[level];
        current.item = item;
        current.is_array = false;
        current.num = 0;
    }

    void lowerLevel(){
        current.item = null;
        level--;
        current = (level >= 0) ? &depth_data[level] : null;
    }

    void addItem(Item item, const(char)[] name){
        if(level < 0){
            root = item;
            upLevel(item);
            return;
        }
        if(current.key != null){
            name = current.key;
        }
        if(current.is_array){
            current.add_item_to_array(item);
        } else {
            assert(name != null);
            current.add_item_to_object(name, item);
        }
        if(current.key != null){
            current.key = null;
        }
        upLevel(item);
    }


    Item getItem(const(char)[] name){
        assert(root != null);
        if(level < 0){
            return root;
        }
        if(current == null){
            return null;
        }

        if(current.is_array){
            int index = current.num;
            Item item = current.get_item_from_array(index);
            current.num++;
            return item;
        }

        assert(name != null);
        assert(current.is_array == false);
        Item item = current.get_item_from_object(name);
        return item;
    }
}

extern(C) void* default_allocate(CallbackCommonData* input, size_t length){
    MallocFunc allocate = cast(MallocFunc)input.context;
    return allocate(length);
}

extern(C) void default_free(CallbackCommonData* input, void* data){
    FreeFunc free = cast(FreeFunc)input.context;
    free(data);
}

SerializerAllocationCallback getAllocate(ref SerializerMetaElement element){
    return (element.on_allocate) ? element.on_allocate : &default_allocate;
}

SerializerFreeCallback getFree(ref SerializerMetaElement element){
    return (element.on_free) ? element.on_free : &default_free;
}

void ensureHasCapacity(ref ubyte[] out_buffer, ref ubyte[] out_slice, size_t required_capacity, MallocFunc malloc, FreeFunc free){
    if(out_slice.length >= required_capacity){
        return;
    }
    size_t out_slice_begin = out_buffer.length - out_slice.length;
    size_t new_size = max(out_buffer.length*2, out_slice_begin + required_capacity);
    ubyte* ptr = cast(ubyte*)malloc(new_size);
    memcpy(ptr, out_buffer.ptr, out_slice_begin);
    free(out_buffer.ptr);
    out_buffer = ptr[0..new_size];
    out_slice = ptr[out_slice_begin..new_size];
}

void append_to_arr(T)(ref T[] arr, auto ref T el){
    size_t old_size = arr.length*T.sizeof;
    size_t new_size = old_size + T.sizeof;
    T* ptr = cast(T*)std_malloc(new_size);
    memcpy(ptr, arr.ptr, old_size);
    ptr[arr.length] = el;
    std_free(arr.ptr);
    arr = ptr[0..arr.length + 1];
}

void convertNumberToNumber(SerializerElementType from_type, int from_size, void* from, SerializerElementType to_type, int to_size, void* to) {
    if(from_type == to_type && from_size == to_size){
        memcpy(to, from, from_size);
        return;
    }
    union Helper {
        double d;
        float f;

        long i64;
        int i32;
        short i16;
        byte i8;

        ulong u64;
        uint u32;
        ushort u16;
        ubyte u8;
    }

    static void toNumber(T)(T from, Helper* u_to, SerializerElementType to_type, int to_size){
        if(to_type == SerializerElementType.floatingPointNumber){
            switch(to_size) {
                case 4: u_to.f = from; break;
                case 8: u_to.d = from; break;
                default:assert(0);
            }
        } else if(to_type == SerializerElementType.signedInteger){
            switch(to_size) {
                case 1: u_to.i8 = clampCast!byte(from); break;
                case 2: u_to.i16 = clampCast!short(from); break;
                case 4: u_to.i32 = clampCast!int(from); break;
                case 8: u_to.i64 = clampCast!long(from); break;
                default:assert(0);
            }
        } else if(to_type == SerializerElementType.unsignedInteger){
            switch(to_size) {
                case 1: u_to.u8 = clampCast!ubyte(from); break;
                case 2: u_to.u16 = clampCast!ushort(from); break;
                case 4: u_to.u32 = clampCast!uint(from); break;
                case 8: u_to.u64 = clampCast!ulong(from); break;
                default:assert(0);
            }
        } else {assert(0);}
    }

    Helper u;
    Helper* u_to = cast(Helper*)to;

    memcpy(&u, from, from_size);
    if(from_type == SerializerElementType.floatingPointNumber){
        if(from_size == 4){
            u.d = u.f; // From now on use only u.d as source number
        }
        toNumber(u.d, u_to, to_type, to_size);
    } else if(from_type == SerializerElementType.signedInteger){
        switch(from_size) {
            case 1: u.i64 = u.i8; break;
            case 2: u.i64 = u.i16; break;
            case 4: u.i64 = u.i32; break; // From now on use only u.i64 as source number
            default:break;
        }
        toNumber(u.i64, u_to, to_type, to_size);
    } else if(from_type == SerializerElementType.unsignedInteger){
        switch(from_size) {
            case 1: u.u64 = u.u8; break;
            case 2: u.u64 = u.u16; break;
            case 4: u.u64 = u.u32; break; // From now on use only u.i64 as source number
            default:break;
        }
        toNumber(u.u64, u_to, to_type, to_size);
    } else {assert(0);}
}

auto elementToVar(T)(ref SerializerMetaElement element, ubyte[] data){
    static if(is(T==long)){
        assert(data.length >= element.size);

        long num;
        memcpy(&num, data.ptr, element.size);
        if(element.type == SerializerElementType.signedInteger){
            switch(element.size) {
                case 1: num = clampCast!byte(*cast(byte*)(&num)); break;
                case 2: num = clampCast!short(*cast(short*)(&num)); break;
                case 4: num = clampCast!int(*cast(int*)(&num)); break;
                case 8: num = clampCast!long(*cast(long*)(&num)); break;
                default:assert(0);
            }
        } else if(element.type == SerializerElementType.unsignedInteger){
            switch(element.size) {
                case 1: num = clampCast!ubyte(*cast(ubyte*)(&num)); break;
                case 2: num = clampCast!ushort(*cast(ushort*)(&num)); break;
                case 4: num = clampCast!uint(*cast(uint*)(&num)); break;
                case 8: num = clampCast!ulong(*cast(ulong*)(&num)); break;
                default:assert(0);
            }
        } else {
            assert(0);
        }
        return num;
    }else static if(is(T==double)){
        switch(element.type) {
            case SerializerElementType.floatingPointNumber:
                break;
            default:
                assert(0);
        }

        assert(data.length >= element.size);

        T num;
        double num_double;
        if(element.size == 8){
            double num_data;
            memcpy(&num_data, data.ptr, element.size);
            num = num_data;
        } else if(element.size == 4){
            float num_data;
            memcpy(&num_data, data.ptr, element.size);
            num = num_data;
        } else {
            assert(0);
        }
        return num;
    }else static if(is(T==char[])){
        char[64] str;
        if(element.type==SerializerElementType.floatingPointNumber){
            double num;
            convertNumberToNumber(element.type, element.size, data.ptr, element.type, num.sizeof, &num);
            NumberConverter nc;
            char[] slice = nc.numToStr(num);
            memcpy(str.ptr, slice.ptr, slice.length);
        }else{
            long num;
            convertNumberToNumber(element.type, element.size, data.ptr, element.type, num.sizeof, &num);
            NumberConverter nc;
            char[] slice = nc.numToStr(num);
            memcpy(str.ptr, slice.ptr, slice.length);
        }
        return str;
    }
}

size_t get_safe_print_capacity(Args...)(Args args){
    size_t cap = 1;
    foreach(ref arg; args) {
        alias T = typeof(arg);
        static if(is(T == string) || is(T == const(char)[]) || is(T == char[])) {
            cap += arg.length;
        } else static if(is(T == int) || is(T == long)){
            cap += 32;
        } else static if(is(T == double)){
            cap += 64;
        } else static if(is(T == char)){
            cap += 1;
        } else {
            pragma(msg, T.stringof);
            static assert(0, "Type not supported");
        }
    }
    return cap;
}

void print_to_buffer_and_advance(Args...)(ref ubyte[] out_buffer, Args args) {
    auto original = out_buffer;
    foreach(ref arg; args) {
        alias T = typeof(arg);
        static if(is(T == string) || is(T == const(char)[]) || is(T == char[])) {
            memcpy(out_buffer.ptr, arg.ptr, arg.length);
            out_buffer = out_buffer[arg.length..$];
        } else static if(is(T == int) || is(T == long)){
            NumberConverter nc;
            char[] slice = nc.numToStr(cast(long)arg);
            memcpy(out_buffer.ptr, slice.ptr, slice.length);
            out_buffer = out_buffer[slice.length..$];
        } else static if(is(T == double)){
            NumberConverter nc;
            char[] slice = nc.numToStr(arg);
            memcpy(out_buffer.ptr, slice.ptr, slice.length);
            out_buffer = out_buffer[slice.length..$];
        } else static if(is(T == char)){
            out_buffer[0] = arg;
            out_buffer = out_buffer[1..$];
        } else {
            pragma(msg, typeof(arg).stringof);
            static assert(0, "Type not supported");
        }
    }
    original[original.length - out_buffer.length] = '\0';
}

struct NumberConverter {
    ubyte[64] buf;
    int next_char_index = 62;

    char[] numToStr(long num, int digits=0) return {
        immutable chars = "0123456789abcdef";
        long val = (num > 0) ? num : -num;
        do {
            buf[next_char_index] = chars[cast(size_t)(val % 10)]; next_char_index--;
            val /= 10;
            digits--;
        } while(val > 0 || digits > 0);
        if(num < 0) {
            buf[next_char_index] = '-'; next_char_index--;
        }
        return cast(char[])buf[next_char_index+1..$];
    }

    char[] numToStr(double num) return {
        long int_part = cast(long)num;
        long float_part = cast(long)((num - int_part) * 1_000_000); // 6 numbers after 0

        numToStr(float_part, 6);
        buf[next_char_index] = '.'; next_char_index--;
        numToStr(int_part);
        return  cast(char[])buf[next_char_index+1..$];
    }

}

TO clampCast(TO, FROM)(FROM var){
    if(cast(double)var > cast(double)TO.max){
        return TO.max;
    }
    if(cast(double)var < cast(double)TO.min){
        return TO.min;
    }
    return cast(TO)var;

}

void varToElement(T)(ref SerializerMetaElement element, ubyte[] data, T var){
    static if(is(T==double)){
        if(element.type == SerializerElementType.signedInteger){
            switch(element.size) {
                case 1: (*cast(byte*)(data.ptr)) = clampCast!byte(var); break;
                case 2: (*cast(short*)(data.ptr)) = clampCast!short(var); break;
                case 4: (*cast(int*)(data.ptr)) = clampCast!int(var); break;
                case 8: (*cast(long*)(data.ptr)) = clampCast!long(var); break;
                default:assert(0);
            }
            return;
        }
        if(element.type == SerializerElementType.unsignedInteger){
            switch(element.size) {
                case 1: (*cast(ubyte*)(data.ptr)) = clampCast!ubyte(var); break;
                case 2: (*cast(ushort*)(data.ptr)) = clampCast!ushort(var); break;
                case 4: (*cast(uint*)(data.ptr)) = clampCast!uint(var); break;
                case 8: (*cast(ulong*)(data.ptr)) = clampCast!ulong(var); break;
                default:assert(0);
            }
            return;
        }
        if(element.type == SerializerElementType.floatingPointNumber){
            switch(element.size) {
                case 4: (*cast(float*)(data.ptr)) = var; break;
                case 8: (*cast(double*)(data.ptr)) = var; break;
                default:assert(0);
            }
            return;
        }
        assert(0);
    }
}

size_t getSizeInHex(ubyte[] data){
    return 2 + data.length * 2;
}

void binaryToHex(ubyte[] data, char[] hex){
    immutable char[16] lookup_table = ['0', '1','2', '3','4', '5','6', '7','8', '9','A', 'B','C', 'D','E', 'F'];
    foreach(i, ubyte b; data){
        hex[2 + i*2 + 0] = lookup_table[(b & 0xF0) >> 4];
        hex[2 + i*2 + 1] = lookup_table[(b & 0x0F) >> 0];
    }
    hex[0] = '0';
    hex[1] = 'x';
}

size_t getSizeInBinary(char[] hex){
    if(hex.length < 2){
        return 0;
    }
    return (hex.length - 2) / 2;
}

void hexToBinary(char[] hex, ubyte[] data){
    ubyte toNum(char c){
        switch(c){
        case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':
            return cast(ubyte)(c - '0');
        case 'A':case 'B':case 'C':case 'D':case 'E':case 'F':
            return cast(ubyte)(10 + c - 'A');
        default: assert(0);
        }
    }
    assert(hex[0] == '0');
    assert(hex[1] == 'x');
    foreach(i, ref ubyte b; data){
        b = cast(ubyte)((toNum(hex[2 + i*2 + 0]) << 4) | (toNum(hex[2 + i*2 + 1]) << 0));
    }
}

/// Helper to automaticly store function pointers from shared library, based on function definitions in D mudule
/// When static linking is used makes aliases to functions from module
struct LibBinder(alias module_, bool dynamic)
{
    static if(dynamic){
        static void* lib_handle;
        template opDispatch(string memberStr){
            alias type = typeof(&__traits(getMember, module_, memberStr));
            static type opDispatch = null;
        }

        static void initialize(const(char)* lib_c_str) {
            if(lib_handle){
                return;
            }
            lib_handle = dlopen(lib_c_str, RTLD_LAZY);
            if(lib_handle == null){
                return;
            }
            foreach(memberStr; __traits(allMembers, module_)) {
                alias member = __traits(getMember, module_, memberStr);
                static if (__traits(isStaticFunction, member)){
                    alias type = typeof(&__traits(getMember, module_, memberStr));
                    opDispatch!memberStr = cast(type)dlsym(lib_handle, memberStr.ptr);
                }
            }
        }
    }else{
        template opDispatch(string memberStr){
            alias opDispatch = __traits(getMember, module_, memberStr);
        }
        static void initialize(const(char)* lib_c_str) {}
    }
}
