/// Implementation of public interface functions

module mser.serializer_impl;

import mser;
import mser.core;
import mser.serializer_helper;


extern(C):


SerializerReturnCode mser_destroy(SerializerHandle* handle){
    assert(handle != null);
    assert(*handle != null);
    (**handle).destroy(*handle);
    *handle = null;
    return SerializerReturnCode.ok;
}

SerializerReturnCode mser_serialize(SerializerHandle handle, SerializerInput* input, SerializerOutput* output){
    assert(handle != null);
    assert(input != null);
    assert(output != null);
    return handle.serialize(handle, input, output);
}

SerializerReturnCode mser_deserialize(SerializerHandle handle, DeserializerInput* input, DeserializerOutput* output){
    assert(handle != null);
    assert(input != null);
    assert(output != null);
    return handle.deserialize(handle, input, output);
}

SerializerReturnCode mser_has_properties(SerializerHandle handle, SerializerProperties properties){
    assert(handle != null);
    return handle.has_properties(handle, properties);
}

SerializerReturnCode mser_destroy_variable(SerializerMeta* serializer_meta, MetaIndex meta_index, void* runtime_data_ptr, FreeFunc free){
    if(runtime_data_ptr == null){
        return SerializerReturnCode.ok;
    }
    SerializerMetaElement* meta = &serializer_meta.meta_elements[meta_index];

    CallbackCommonData cd_free = CallbackCommonData(serializer_meta, meta_index, runtime_data_ptr, null, free);
    DestroyInput destroy_input =  DestroyInput(CallbackCommonData(serializer_meta, meta_index, runtime_data_ptr, null, null));

    switch(meta.type) {
        case SerializerElementType.none: break;
        case SerializerElementType.signedInteger:
        case SerializerElementType.unsignedInteger:
        case SerializerElementType.floatingPointNumber:
        case SerializerElementType.binaryFixed:
        case SerializerElementType.arrayCustom:
        case SerializerElementType.stringCustom:
        case SerializerElementType.containerCustom:
            if(meta.on_destroy){
                meta.on_destroy(&destroy_input);
            }
            return SerializerReturnCode.ok;

        default: break;
    }

    if(meta.type == SerializerElementType.container){
        foreach(ref SerializerSubobject reference; meta.subobjects) {
            MetaIndex index = reference.meta_index;
            void* ptr = runtime_data_ptr + reference.offset;
            mser_destroy_variable(serializer_meta, index, ptr, free);
        }
        if(meta.on_destroy){
            meta.on_destroy(&destroy_input);
        }
        return SerializerReturnCode.ok;
    }

    if(meta.type == SerializerElementType.array || meta.type == SerializerElementType.arrayFixed){
        SerializerSubobject* data_ref = &meta.subobjects[0];
        SerializerSubobject* length_ref = meta.subobjects.ptr + 1; // Exists only for array type
        SerializerMetaElement* data_runtime = serializer_meta.meta_elements.ptr + data_ref.meta_index;


        size_t length;
        void* array_data_ptr;
        if(meta.type == SerializerElementType.arrayFixed){
            length = meta.size / data_runtime.size;
            array_data_ptr = cast(ubyte*)runtime_data_ptr;
        }else{
            length = *cast(size_t*)&runtime_data_ptr[length_ref.offset];
            array_data_ptr = *cast(void**)&runtime_data_ptr[data_ref.offset];
        }

        foreach(i; 0..length) {
            void* ptr = (array_data_ptr) ? array_data_ptr + i*data_runtime.size : null;
            mser_destroy_variable(serializer_meta, data_ref.meta_index, ptr, free);
        }
        if(meta.on_destroy){
            meta.on_destroy(&destroy_input);
        }
        if(meta.type != SerializerElementType.arrayFixed){
            getFree(*meta)(&cd_free, array_data_ptr);
        }
        return SerializerReturnCode.ok;
    }

    // if(meta.type == SerializerElementType.arrayCustom) {}
    // if(meta.type == SerializerElementType.stringCustom) {}
    // if(meta.type == SerializerElementType.containerCustom) {}

    if(meta.type == SerializerElementType.string || meta.type == SerializerElementType.binary){
        char** str = cast(char**)(runtime_data_ptr + meta.subobjects[0].offset);
        getFree(*meta)(&cd_free, *str);
        if(meta.on_destroy){
            meta.on_destroy(&destroy_input);
        }
        return SerializerReturnCode.ok;
    }

    if(meta.type == SerializerElementType.pointer){
        ubyte** pointer = cast(ubyte**)runtime_data_ptr;
        getFree(*meta)(&cd_free, *pointer);
        return SerializerReturnCode.ok;
    }

    return SerializerReturnCode.metaElementNotFound;
}

SerializerReturnCode mser_meta_compute_indices(SerializerMeta* meta){
    foreach(i, ref SerializerMetaElement meta_element; meta.meta_elements){
        meta_element.meta_index = cast(MetaIndex)i;
        foreach(ref SerializerSubobject subobject; meta_element.subobjects){
            bool found = subobject.meta_id == null;
            foreach(i_inner, ref SerializerMetaElement meta_element_inner; meta.meta_elements){
                // logf("(%s) == (%s)\n", subobject.meta_id.ptr, meta_element_inner.id.ptr);
                if(subobject.meta_id == meta_element_inner.id){
                    subobject.meta_index = cast(MetaIndex)i_inner;
                    found = true;
                    break;
                }
            }
            if(found == false){
                // logf("%s\n", subobject.meta_id.ptr);
                return SerializerReturnCode.metaElementNotFound;
            }
        }
    }

    return SerializerReturnCode.ok;
}

SerializerReturnCode mser_meta_get_index(SerializerMeta* meta, const(char)* id, size_t id_length, MetaIndex* index){
    *index = MetaIndex.min;
    foreach(i, ref SerializerMetaElement meta_element; meta.meta_elements){
        if(meta_element.id == id[0..id_length]){
            *index = cast(MetaIndex)i;
            return SerializerReturnCode.ok;
        }
    }
    return SerializerReturnCode.metaElementNotFound;
}

SerializerReturnCode mser_meta_validate(SerializerMeta* metas){
    SerializerReturnCode ret_code = SerializerReturnCode.ok;

    if(metas.meta_elements.length == 0){
        logf("mser meta validate error; SerializerMeta with zero elements\n");
        ret_code = SerializerReturnCode.badParameter;
    }

    foreach(i, ref SerializerMetaElement meta; metas.meta_elements){
        if(meta.meta_index != cast(MetaIndex)i){
            logf("mser meta validate error, index(%d); Invalid index(%d)\n", cast(int)i, cast(int)meta.meta_index);
            ret_code = SerializerReturnCode.badParameter;
        }
        switch(meta.type){
            case SerializerElementType.none:
            case SerializerElementType.signedInteger:
            case SerializerElementType.unsignedInteger:
            case SerializerElementType.floatingPointNumber:
            case SerializerElementType.binaryFixed:
                if(meta.subobjects.length != 0){
                    logf("mser meta validate error, index(%d); type(%d) should have zero subobjects\n", cast(int)i, cast(int)meta.type);
                    ret_code = SerializerReturnCode.badParameter;
                }
                break;
            case SerializerElementType.binaryCustom_reserved:
            case SerializerElementType.stringFixed_reserved:
            case SerializerElementType.pointerCustom_reserved:
                    logf("mser meta validate error, index(%d); type(%d) is reserved and should not be used\n", cast(int)i, cast(int)meta.type);
                    ret_code = SerializerReturnCode.badParameter;
                    break;
            case SerializerElementType.binary:
            case SerializerElementType.array:
            case SerializerElementType.string:
                if(meta.subobjects.length != 2){
                    logf("mser meta validate error, index(%d); type(%d) should have two subobjects (pointer location and length location)\n", cast(int)i, cast(int)meta.type);
                    ret_code = SerializerReturnCode.badParameter;
                }
                break;
            case SerializerElementType.arrayCustom:
                if(meta.subobjects.length != 1){
                    logf("mser meta validate error, index(%d); type(arrayCustom) should have one subobject (array element type)\n", cast(int)i);
                    ret_code = SerializerReturnCode.badParameter;
                }
                if(meta.on_save_array == null || meta.on_load_array == null){
                    logf("mser meta validate error, index(%d); type(arrayCustom) on_save_array and on_load_array callbacks should be defined\n", cast(int)i);
                    ret_code = SerializerReturnCode.badParameter;
                }
                break;
            case SerializerElementType.stringCustom:
                if(meta.subobjects.length != 0){
                    logf("mser meta validate error, index(%d); type(stringCustom) should have zero subobjects\n", cast(int)i);
                    ret_code = SerializerReturnCode.badParameter;
                }
                if(meta.on_save_string == null || meta.on_load_string == null){
                    logf("mser meta validate error, index(%d); type(stringCustom) on_save_string and on_load_string callbacks should be defined\n", cast(int)i);
                    ret_code = SerializerReturnCode.badParameter;
                }
                break;
            case SerializerElementType.arrayFixed:
                if(meta.subobjects.length != 1){
                    logf("mser meta validate error, index(%d); type(arrayFixed) should have one subobject (array element type), second subobject shouldn't be used, now elements number is calculated by (object.size/subobjects[0].size)\n", cast(int)i);
                    ret_code = SerializerReturnCode.badParameter;
                }
                break;
            case SerializerElementType.pointer:
                if(meta.subobjects.length != 1){
                    logf("mser meta validate error, index(%d); type(pointer) should have one subobject (pointee type)\n", cast(int)i);
                    ret_code = SerializerReturnCode.badParameter;
                }
                break;
            default: break;
        }

        if(
            meta.type != SerializerElementType.binary &&
            meta.type != SerializerElementType.array &&
            meta.type != SerializerElementType.string
        ){
            foreach(sub_i, ref sub; meta.subobjects){
                if(sub.meta_index < 0 || sub.meta_index >= metas.meta_elements.length){
                    logf("mser meta validate error, index(%d); subobject_index(%d) has wrong meta_index(%d)\n", cast(int)i, cast(int)sub_i, cast(int)sub.meta_index);
                    ret_code = SerializerReturnCode.badParameter;
                }
            }
        }

        if(meta.type == SerializerElementType.arrayFixed){
            int element_size = metas.meta_elements[meta.subobjects[0].meta_index].size;
            if((meta.size % element_size) != 0){
                logf("mser meta validate error, index(%d); type(arrayFixed) size of fixed array(%d) is not multiple of element size(%d)\n", cast(int)i, meta.size, element_size);
                ret_code = SerializerReturnCode.badParameter;
            }
        }
    }
    return ret_code;
}

const(char)* mser_enum_str_SerializerElementType(SerializerElementType val){
    if(val > SerializerElementType.max){
        return "UNKNOWN";
    }
    final switch(val){
        case SerializerElementType.none: return "none";
        case SerializerElementType.signedInteger: return "signedInteger";
        case SerializerElementType.unsignedInteger: return "unsignedInteger";
        case SerializerElementType.floatingPointNumber: return "floatingPointNumber";
        case SerializerElementType.binary: return "binary";
        case SerializerElementType.binaryCustom_reserved: return "binaryCustom_reserved";
        case SerializerElementType.binaryFixed: return "binaryFixed";
        case SerializerElementType.array: return "array";
        case SerializerElementType.arrayCustom: return "arrayCustom";
        case SerializerElementType.arrayFixed: return "arrayFixed";
        case SerializerElementType.container: return "container";
        case SerializerElementType.containerCustom: return "containerCustom";
        case SerializerElementType.string: return "string";
        case SerializerElementType.stringCustom: return "stringCustom";
        case SerializerElementType.stringFixed_reserved: return "stringFixed_reserved";
        case SerializerElementType.pointer: return "pointer";
        case SerializerElementType.pointerCustom_reserved: return "pointerCustom_reserved";
    }
    // return "UNKNOWN";
}

const(char)* mser_enum_str_SerializerReturnCode(SerializerReturnCode val){
    if(val > SerializerReturnCode.max){
        return "UNKNOWN";
    }
    final switch(val){
        case SerializerReturnCode.ok: return "ok";
        case SerializerReturnCode.notInitialized: return "notInitialized";
        case SerializerReturnCode.alreadyInitialized: return "alreadyInitialized";
        case SerializerReturnCode.noSerializerBackend: return "noSerializerBackend";
        case SerializerReturnCode.operationNotSupportedByImpl: return "operationNotSupportedByImpl";
        case SerializerReturnCode.allocationError: return "allocationError";
        case SerializerReturnCode.badParameter: return "badParameter";
        case SerializerReturnCode.serImplInitializationError: return "serImplInitializationError";
        case SerializerReturnCode.propertiesNotSupported: return "propertiesNotSupported";
        case SerializerReturnCode.metaElementNotFound: return "metaElementNotFound";
        case SerializerReturnCode.notEnoughData: return "notEnoughData";
        case SerializerReturnCode.metaNotFound: return "metaNotFound";
    }
    // return "UNKNOWN";
}

const(char)* mser_enum_str_SerializerProperties(SerializerProperties val){
    enum maxCombined = (SerializerProperties.max << 1) - 1;
    if(val > maxCombined){
        return "UNKNOWN";
    }
    switch(val){
        case SerializerProperties.none: return "none";
        case SerializerProperties.canSerialize: return "canSerialize";
        case SerializerProperties.canDeserialize: return "canDeserialize";
        case SerializerProperties.isBinary: return "isBinary";
        case SerializerProperties.supportsNotPresentData: return "supportsNotPresentData";
        default: break;
    }
    return "MULTIPLE_FLAGS";
}
