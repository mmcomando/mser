/// Template to handle common backend operations
/// Uses struct SerializerImpl to implement backend specific operations
/// Backend doesn't have to be developed using this template but might make it much easier to do
module mser.serializer_template;

import mser;
import mser.core;
import mser.serializer_helper;



extern(C) SerializerReturnCode create_serializer(SerializerBackend)(SerializerHandle* handle, MserCreateInput* input){
    assert(handle != null);
    assert(*handle == null, "Already created");
    assert(mser_meta_validate(input.meta) == SerializerReturnCode.ok);
    SerializerHandleImpl* handle_impl = createImpl!SerializerBackend(input);
    assert(handle_impl != null);
    *handle = cast(SerializerHandle)handle_impl;
    return SerializerReturnCode.ok;
}

struct SerializerTemplate(SerializerImpl, DeserializerImpl, SerializerProperties prop) {
    static assert(__traits(hasMember, SerializerImpl, "initialize"), "SerializerImpl has to have function 'initialize'");
    static assert(__traits(hasMember, SerializerImpl, "finalize"), "SerializerImpl has to have function 'finalize'");
    static assert(__traits(hasMember, SerializerImpl, "saveSimpleElement"), "SerializerImpl has to have function 'saveSimpleElement'");
    static assert(__traits(hasMember, SerializerImpl, "saveStartContainer"), "SerializerImpl has to have function 'saveStartContainer'");
    static assert(__traits(hasMember, SerializerImpl, "saveEndContainer"), "SerializerImpl has to have function 'saveEndContainer'");
    static assert(__traits(hasMember, SerializerImpl, "saveStartArray"), "SerializerImpl has to have function 'saveStartArray'");
    static assert(__traits(hasMember, SerializerImpl, "saveEndArray"), "SerializerImpl has to have function 'saveEndArray'");

    static assert(__traits(hasMember, DeserializerImpl, "initialize"), "DeserializerImpl has to have function 'initialize'");
    static assert(__traits(hasMember, DeserializerImpl, "finalize"), "DeserializerImpl has to have function 'finalize'");
    static assert(__traits(hasMember, DeserializerImpl, "loadSimpleElement"), "DeserializerImpl has to have function 'loadSimpleElement'");
    static assert(__traits(hasMember, DeserializerImpl, "loadStartContainer"), "DeserializerImpl has to have function 'loadStartContainer'");
    static assert(__traits(hasMember, DeserializerImpl, "loadEndContainer"), "DeserializerImpl has to have function 'loadEndContainer'");
    static assert(__traits(hasMember, DeserializerImpl, "loadStartArray"), "DeserializerImpl has to have function 'loadStartArray'");
    static assert(__traits(hasMember, DeserializerImpl, "loadEndArray"), "DeserializerImpl has to have function 'loadEndArray'");

    enum SerializerProperties properties = prop;
    SerializerImpl serializer_impl;
    DeserializerImpl deserializer_impl;
    SerializerMeta* meta;
    MallocFunc malloc;
    FreeFunc free;
    void* user_data;

    SerializerReturnCode serialize(SerializerInput* input, SerializerOutput* output)
    {
        user_data = input.user_data;
        serializer_impl.free = free;
        serializer_impl.malloc = malloc;
        serializer_impl.initialize();
        ubyte[] runtime_data = cast(ubyte[])(input.runtime_data_ptr[0..meta.meta_elements[input.meta_index].size]);
        saveElement("", runtime_data, meta.meta_elements[input.meta_index]);
        serializer_impl.finalize();

        output.out_buffer = serializer_impl.out_buffer.ptr;
        output.out_buffer_length = serializer_impl.out_buffer.length - serializer_impl.out_data.length;
        return SerializerReturnCode.ok;
    }

    alias ThisType = SerializerTemplate!(SerializerImpl, DeserializerImpl, prop);
    static struct ContextCustom{
            ThisType* serializer;
            const(char)[] name;
            ubyte[] data;
            MetaIndex meta_index;
            bool loaded;
    }

    extern(C) static SerializerReturnCode saveCustomCallback(ContextCustom* context, void* runtime_data_ptr){
        SerializerMetaElement element = context.serializer.meta.meta_elements[context.meta_index];
        element.on_save = null;
        ubyte* ptr = cast(ubyte*)runtime_data_ptr;
        context.serializer.saveElement(context.name, ptr[0..element.size], element);
        return SerializerReturnCode.ok;
    }

    void saveElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        if(element.on_save){
            ContextCustom context = ContextCustom(&this, name, data, element.meta_index);
            SerializeCustomInput input = SerializeCustomInput(CallbackCommonData(meta, element.meta_index, data.ptr, user_data, &context));
            input.serialize = cast(SerializeLoadSaveCallback)&saveCustomCallback;
            element.on_save(&input);
            return;
        }
        switch(element.type) {
            case SerializerElementType.none: break;
            case SerializerElementType.signedInteger:
            case SerializerElementType.unsignedInteger:
            case SerializerElementType.floatingPointNumber:
                serializer_impl.saveSimpleElement(name, data, element);
                break;
            case SerializerElementType.container:
                saveContainer(name, data, element);
                break;
            case SerializerElementType.array:
                saveArray(name, data, element);
                break;
            case SerializerElementType.arrayCustom:
                saveCustomArray(name, data, element);
                break;
            case SerializerElementType.arrayFixed:
                saveFixedArray(name, data, element);
                break;
            case SerializerElementType.string:
                saveString(name, data, element);
                break;
            case SerializerElementType.stringCustom:
                saveCustomString(name, data, element);
                break;
            case SerializerElementType.containerCustom:
                saveContainerCustom(name, data, element);
                break;
            case SerializerElementType.pointer:
                savePointer(name, data, element);
                break;
            case SerializerElementType.binary:
                saveBinary(name, data, element);
                break;
            case SerializerElementType.binaryFixed:
                saveFixedBinary(name, data, element);
                break;
            default: assert(0);
        }
    }


    void saveContainer(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        serializer_impl.saveStartContainer(name, element);

        foreach(SerializerSubobject reference; element.subobjects) {
            MetaIndex index = reference.meta_index;
            saveElement(reference.name, data[reference.offset..$], meta.meta_elements[index]);
        }
        serializer_impl.saveEndContainer();
    }

    void saveFixedArray(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        SerializerSubobject data_ref = element.subobjects[0];
        MetaIndex index = data_ref.meta_index;
        SerializerMetaElement data_element = meta.meta_elements[index];
        size_t elements_num = element.size / data_element.size;

        serializer_impl.saveStartArray(name, element, elements_num);
        foreach(i; 0..elements_num) {
            ubyte[] data_to_save = data.ptr[(i+0)*data_element.size..(i+1)*data_element.size];
            saveElement(data_ref.name, data_to_save, meta.meta_elements[index]);
        }

        serializer_impl.saveEndArray();
    }

    void saveArray(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        SerializerSubobject data_ref = element.subobjects[0];
        SerializerSubobject length_ref = element.subobjects[1];

        ubyte* data_ptr = *cast(ubyte**)&data[data_ref.offset];
        size_t length = *cast(size_t*)&data[length_ref.offset];
        MetaIndex index = data_ref.meta_index;
        SerializerMetaElement data_element = meta.meta_elements[index];

        serializer_impl.saveStartArray(name, data_element, length);
        foreach(i; 0..length) {
            ubyte[] data_to_save = data_ptr[(i+0)*data_element.size..(i+1)*data_element.size];
            saveElement(data_ref.name, data_to_save, meta.meta_elements[index]);
        }

        serializer_impl.saveEndArray();
    }

    static struct ContextCustomArray {
            ThisType* serializer;
            const(char)[] name;
            ubyte[] data;
            MetaIndex meta_index_array;
            MetaIndex meta_index_element;
            size_t length;
    }

    extern(C) static SerializerReturnCode saveCustomArrayStartCallback(ContextCustomArray* context, size_t length){
        context.length = length;
        SerializerMetaElement array = context.serializer.meta.meta_elements[context.meta_index_array];
        context.serializer.serializer_impl.saveStartArray(context.name, array, context.length);
        return SerializerReturnCode.ok;
    }

    extern(C) static SerializerReturnCode saveCustomArrayElementCallback(ContextCustomArray* context, void* runtime_data_ptr){
        SerializerMetaElement element = context.serializer.meta.meta_elements[context.meta_index_element];
        ubyte[] data_to_save = cast(ubyte[])runtime_data_ptr[0..element.size];
        context.serializer.saveElement(null, data_to_save, element);
        return SerializerReturnCode.ok;
    }

    void saveCustomArray(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {

        SerializerSubobject data_ref = element.subobjects[0];
        MetaIndex index = data_ref.meta_index;
        ContextCustomArray context = ContextCustomArray(&this, name, data, element.meta_index, index);

        SerializeArrayInput input = SerializeArrayInput(CallbackCommonData(meta, element.meta_index, data.ptr, user_data, &context));
        input.serialize_start = cast(SerializerLoadArrayStartCallback)&saveCustomArrayStartCallback;
        input.serialize_element = cast(SerializeLoadSaveArrayElementCallback)&saveCustomArrayElementCallback;

        element.on_save_array(&input);
        serializer_impl.saveEndArray();
    }

    void saveString(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        SerializerSubobject ptr_ref = element.subobjects[0];
        SerializerSubobject length_ref = element.subobjects[1];

        char** ptr_location = cast(char**)(data.ptr + ptr_ref.offset);
        size_t length = *cast(size_t*)(data.ptr + length_ref.offset); // Might be smaller than size_t
        serializer_impl.saveString(name, element, length, *ptr_location);
    }

    extern(C) static SerializerReturnCode saveCustomStringCallback(ContextCustomArray* context, const (char)* str, size_t length){
        SerializerMetaElement element = context.serializer.meta.meta_elements[context.meta_index_array];
        context.serializer.serializer_impl.saveString(context.name, element, length, str);
        return SerializerReturnCode.ok;
    }

    void saveCustomString(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        // Reuse ContextCustomArray for custom String as it has all required fields for saving
        ContextCustomArray context = ContextCustomArray(&this, name, data, element.meta_index, -1);

        SerializeStringInput input = SerializeStringInput(CallbackCommonData(meta, element.meta_index, data.ptr, user_data, &context));
        input.serialize_string = cast(SerializeSaveStringElementCallback)&saveCustomStringCallback;

        element.on_save_string(&input);
    }

    static struct ContextMap {
            ThisType* serializer;
            const(char)[] name;
            ubyte[] data;
            MetaIndex meta_index_map;
            MetaIndex meta_index_name;
            size_t length;
            int current_load_index;
    }

    extern(C) static SerializerReturnCode saveMapStartCallback(ContextMap* context, size_t length){
        context.length = length;
        SerializerMetaElement map = context.serializer.meta.meta_elements[context.meta_index_map];
        context.serializer.serializer_impl.saveStartMap(context.name, map, context.length);
        return SerializerReturnCode.ok;
    }

    extern(C) static SerializerReturnCode saveContainerCustomElementCallback(ContextMap* context, MetaIndex name_index, void* runtime_data_ptr){
        SerializerMetaElement container_custom = context.serializer.meta.meta_elements[context.meta_index_map];
        SerializerSubobject key_name = container_custom.subobjects[name_index];
        MetaIndex meta_index = key_name.meta_index;

        SerializerMetaElement name = context.serializer.meta.meta_elements[context.meta_index_name];
        context.serializer.serializer_impl.saveKey(key_name.name);

        SerializerMetaElement element = context.serializer.meta.meta_elements[meta_index];
        ubyte[] data_to_save = cast(ubyte[])runtime_data_ptr[0..element.size];
        context.serializer.saveElement(null, data_to_save, element);
        return SerializerReturnCode.ok;
    }

    void saveContainerCustom(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        ContextMap context = ContextMap(&this, name, data, element.meta_index, 0);

        SerializeContainerCustomInput input = SerializeContainerCustomInput(CallbackCommonData(meta, element.meta_index, data.ptr, user_data, &context));
        input.serialize_start = cast(SerializerSaveContainerStartCallback)&saveMapStartCallback;
        input.serialize_element = cast(SerializerSaveContainerNameElementCallback)&saveContainerCustomElementCallback;

        element.on_save_container(&input);
        serializer_impl.saveEndMap();
    }

    void savePointer(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        ubyte* pointer = *cast(ubyte**)data.ptr;
        if(pointer == null) {
            return;
        }
        SerializerMetaElement* pointee = &meta.meta_elements[element.subobjects[0].meta_index];
        saveElement(name, pointer[0..pointee.size], *pointee);
    }

    void saveBinary(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        SerializerSubobject ptr_ref = element.subobjects[0];
        SerializerSubobject length_ref = element.subobjects[1];

        ubyte* ptr_location = *cast(ubyte**)(data.ptr + ptr_ref.offset);
        size_t length = *cast(size_t*)(data.ptr + length_ref.offset); // Might be smaller than size_t

        ubyte[] binary = ptr_location[0..length];
        size_t hex_length = getSizeInHex(binary);

        char* hex_ptr = cast(char*)malloc(hex_length + 1);
        binaryToHex(binary, hex_ptr[0..hex_length]);
        hex_ptr[hex_length] = '\0';
        serializer_impl.saveString(name, element, hex_length, hex_ptr);

        free(hex_ptr);
    }

    void saveFixedBinary(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        char[256] small_hex_optimization;

        ubyte[] binary = data.ptr[0..element.size];
        size_t hex_length = getSizeInHex(binary);
        bool use_small_buffer = hex_length < small_hex_optimization.length;

        char* hex_ptr = small_hex_optimization.ptr;
        if(use_small_buffer == false) {
            hex_ptr = cast(char*)malloc(hex_length + 1);
        }
        binaryToHex(binary, hex_ptr[0..hex_length]);
        hex_ptr[hex_length] = '\0';
        serializer_impl.saveString(name, element, hex_length, hex_ptr);

        if(use_small_buffer == false){
            free(hex_ptr);
        }
    }

    SerializerReturnCode deserialize(DeserializerInput* input, DeserializerOutput* output)
    {
        user_data = input.user_data;
        ubyte[] serialized_data =(cast(ubyte*)input.in_buffer)[0..input.in_buffer_length];
        deserializer_impl.free = free;
        deserializer_impl.malloc = malloc;
        deserializer_impl.initialize(serialized_data);
        ubyte[] runtime_data = cast(ubyte[])(input.runtime_data_ptr[0..meta.meta_elements[input.meta_index].size]);
        loadElement("", runtime_data, meta.meta_elements[input.meta_index]);
        deserializer_impl.finalize();

        output.buffer_read_length = input.in_buffer_length; // TODO fix: buffer_read_length
        return SerializerReturnCode.ok;
    }

    extern(C) static SerializerReturnCode loadCustomCallback(ContextCustom* context, void* runtime_data_ptr){
        SerializerMetaElement element = context.serializer.meta.meta_elements[context.meta_index];
        element.on_load = null;
        ubyte* ptr = cast(ubyte*)runtime_data_ptr;
        context.loaded = context.serializer.loadElement(context.name, ptr[0..element.size], element);
        return SerializerReturnCode.ok;
    }

    bool loadElement(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        if(element.on_load){
            ContextCustom context = ContextCustom(&this, name, data, element.meta_index);
            DeserializeCustomInput input = DeserializeCustomInput(CallbackCommonData(meta, element.meta_index, data.ptr, user_data, &context));
            input.deserialize = cast(SerializeLoadSaveCallback)&loadCustomCallback;
            element.on_load(&input);
            return context.loaded;
        }
        bool loaded;
        switch(element.type) {
            case SerializerElementType.none: break;
            case SerializerElementType.signedInteger:
            case SerializerElementType.unsignedInteger:
            case SerializerElementType.floatingPointNumber:
                loaded = deserializer_impl.loadSimpleElement(name, data, element);
                break;
            case SerializerElementType.container:
                loaded = loadContainer(name, data, element);
                break;
            case SerializerElementType.array:
                loaded = loadArray(name, data, element);
                break;
            case SerializerElementType.arrayCustom:
                loaded = loadCustomArray(name, data, element);
                break;
            case SerializerElementType.arrayFixed:
                loaded = loadFixedArray(name, data, element);
                break;
            case SerializerElementType.string:
                loaded = loadString(name, data, element);
                break;
            case SerializerElementType.stringCustom:
                loaded = loadCustomString(name, data, element);
                break;
            case SerializerElementType.containerCustom:
                loaded = loadContainerCustom(name, data, element);
                break;
            case SerializerElementType.pointer:
                loaded = loadPointer(name, data, element);
                break;
            case SerializerElementType.binary:
                loaded = loadBinary(name, data, element);
                break;
            case SerializerElementType.binaryFixed:
                loaded = loadFixedBinary(name, data, element);
                break;
            default: assert(0);
        }
        if(loaded == false && element.on_missing_field){
            element.on_missing_field(meta, data.ptr, element.meta_index, user_data);
        }
        if(loaded == true && element.on_load_finish){
            element.on_load_finish(meta, data.ptr, element.meta_index, user_data);
        }
        return loaded;
    }

    bool loadContainer(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        memset(data.ptr, 0, element.size);
        bool ok = deserializer_impl.loadStartContainer(name, element);
        if(ok == false){
            return false;
        }
        foreach(SerializerSubobject reference; element.subobjects) {
            MetaIndex index = reference.meta_index;
            loadElement(reference.name, data[reference.offset..$], meta.meta_elements[index]);
        }
        deserializer_impl.loadEndContainer();
        return true;
    }

    bool loadFixedArray(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        SerializerSubobject data_ref = element.subobjects[0];
        MetaIndex index = data_ref.meta_index;
        SerializerMetaElement data_element = meta.meta_elements[index];
        size_t elements_num = element.size / data_element.size;

        memset(data.ptr, 0, element.size);

        size_t length;
        bool ok = deserializer_impl.loadStartArray(name, data_element, length);
        if(ok == false){
            return false;
        }
        if(elements_num < length){
            length = elements_num;
        }

        foreach(i; 0..length) {
            ubyte[] data_to_load = data[(i+0)*data_element.size..(i+1)*data_element.size];
            loadElement(data_ref.name, data_to_load, meta.meta_elements[index]);
        }
        deserializer_impl.loadEndArray();

        return true;
    }

    bool loadArray(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        SerializerSubobject data_ref = element.subobjects[0];
        SerializerSubobject length_ref = element.subobjects[1];

        MetaIndex index = data_ref.meta_index;
        SerializerMetaElement data_element = meta.meta_elements[index];
        size_t length;
        bool ok = deserializer_impl.loadStartArray(name, data_element, length);
        if(ok == false){
            return false;
        }

        CallbackCommonData cd = CallbackCommonData(meta, element.meta_index, data.ptr, user_data, malloc);
        ubyte[] array_data = cast(ubyte[])(getAllocate(element)(&cd, length*data_element.size)[0..length*data_element.size]);
        memset(array_data.ptr, 0, length*data_element.size);

        foreach(i; 0..length) {
            ubyte[] data_to_load = array_data[(i+0)*data_element.size..(i+1)*data_element.size];
            loadElement(data_ref.name, data_to_load, meta.meta_elements[index]);
        }
        deserializer_impl.loadEndArray();

        *cast(size_t*)&data[length_ref.offset] = length;
        *cast(void**)&data[data_ref.offset] = array_data.ptr;
        return true;
    }

    extern(C) static SerializerReturnCode loadCustomArrayElementCallback(ContextCustomArray* context, void* runtime_data_ptr){
        SerializerMetaElement element = context.serializer.meta.meta_elements[context.meta_index_element];
        ubyte[] data_to_load = cast(ubyte[])runtime_data_ptr[0..element.size];
        context.serializer.loadElement(null, data_to_load, element);
        return SerializerReturnCode.ok;
    }

    bool loadCustomArray(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {

        SerializerSubobject data_ref = element.subobjects[0];


        size_t length;
        bool ok = deserializer_impl.loadStartArray(name, element, length);
        if(ok == false){
            return false;
        }

        MetaIndex index = data_ref.meta_index;
        ContextCustomArray context = ContextCustomArray(&this, name, data, element.meta_index, index, length);


        DeserializeArrayInput input = DeserializeArrayInput(CallbackCommonData(meta, element.meta_index, data.ptr, user_data, &context));
        input.length = length;
        input.deserialize_element = cast(SerializeLoadSaveArrayElementCallback)&loadCustomArrayElementCallback;

        element.on_load_array(&input);
        deserializer_impl.loadEndArray();
        return true;
    }

    bool loadString(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        SerializerSubobject ptr_ref = element.subobjects[0];
        SerializerSubobject length_ref = element.subobjects[1];
        // SerializerMetaElement ptr_element = meta.meta_elements[ptr_ref.meta_index];
        SerializerMetaElement length_element = meta.meta_elements[length_ref.meta_index];

        char** ptr_location = cast(char**)(data.ptr + ptr_ref.offset);
        size_t* length_location = cast(size_t*)(data.ptr + length_ref.offset); // Might be smaller than size_t

        size_t length;
        bool ok = deserializer_impl.loadString(name, element, length, *ptr_location);
        if(ok == false){
            return false;
        }

        memcpy(length_location, &length, length_element.size);
        return true;
    }

    bool loadCustomString(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        ContextCustomArray context = ContextCustomArray(&this, name, data, element.meta_index, -1);

        size_t length;
        char* str;
        bool ok = deserializer_impl.loadString(name, element, length, str);
        if(ok == false){
            return false;
        }
        DeserializeStringInput input = DeserializeStringInput(CallbackCommonData(meta, element.meta_index, data.ptr, user_data, null));
        input.length = length;
        input.str = str;

        element.on_load_string(&input);
        free(str);

        return true;
    }

    extern(C) static SerializerReturnCode loadContainerCustomNameCallback(ContextMap* context, MetaIndex* name_index){
        SerializerMetaElement map_meta = context.serializer.meta.meta_elements[context.meta_index_map];

        *name_index = -1;

        const(char)* str = context.serializer.deserializer_impl.loadKey(context.current_load_index);
        size_t length = strlen(str);
        context.current_load_index++;
        assert(str != null);
        foreach(i, ref sub; map_meta.subobjects){
            if(sub.name == str[0..length]){
                *name_index = cast(MetaIndex)i;
                context.meta_index_name = cast(MetaIndex)i;
                break;
            }
        }
        context.serializer.free(cast(void*)str);
        return SerializerReturnCode.ok;
    }

    extern(C) static SerializerReturnCode loadContainerCustomElementCallback(ContextMap* context, void* runtime_data_ptr){
        if(runtime_data_ptr == null){
            return SerializerReturnCode.ok; // TODO: this is proper ignore?
        }
        SerializerMetaElement map_meta = context.serializer.meta.meta_elements[context.meta_index_map];
        MetaIndex meta_index = map_meta.subobjects[context.meta_index_name].meta_index;

        SerializerMetaElement element = context.serializer.meta.meta_elements[meta_index];
        ubyte[] data_to_load = cast(ubyte[])runtime_data_ptr[0..element.size];
        context.serializer.loadElement(null, data_to_load, element);
        return SerializerReturnCode.ok;
    }

    bool loadContainerCustom(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {

        SerializerSubobject data_ref = element.subobjects[0];

        size_t length;
        bool ok = deserializer_impl.loadStartMap(name, element, length);
        if(ok == false){
            return false;
        }

        MetaIndex index = data_ref.meta_index;
        ContextMap context = ContextMap(&this, name, data, element.meta_index, index, length, 0);


        DeserializeContainerCustomInput input = DeserializeContainerCustomInput(CallbackCommonData(meta, element.meta_index, data.ptr, user_data, &context));
        input.length = length;
        input.deserialize_name = cast(SerializerLoadContainerNameCallback)&loadContainerCustomNameCallback;
        input.deserialize_element = cast(SerializerLoadContainerElementCallback)&loadContainerCustomElementCallback;

        element.on_load_container(&input);
        deserializer_impl.loadEndMap();
        return true;
    }


    bool loadPointer(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        SerializerMetaElement* pointee = &meta.meta_elements[element.subobjects[0].meta_index];

        CallbackCommonData cd = CallbackCommonData(meta, element.meta_index, data.ptr, user_data, malloc);
        ubyte* pointee_data_ptr = cast(ubyte*)getAllocate(element)(&cd, pointee.size);
        memset(pointee_data_ptr, 0, pointee.size);
        ubyte[] pointee_data = pointee_data_ptr[0..pointee.size];

        bool loaded = loadElement(name, pointee_data, *pointee);
        ubyte** pointer = cast(ubyte**)data.ptr;

        if(loaded == false) {
            free(pointee_data_ptr);
            *pointer = null;
            return true;
        }
        *pointer = pointee_data_ptr;
        return true;
    }

    bool loadBinary(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        SerializerSubobject ptr_ref = element.subobjects[0];
        SerializerSubobject length_ref = element.subobjects[1];
        // SerializerMetaElement ptr_element = meta.meta_elements[ptr_ref.meta_index];
        SerializerMetaElement length_element = meta.meta_elements[length_ref.meta_index];

        ubyte** ptr_location = cast(ubyte**)(data.ptr + ptr_ref.offset);
        size_t* length_location = cast(size_t*)(data.ptr + length_ref.offset); // Might be smaller than size_t

        size_t hex_length;
        char* hex_ptr;
        bool ok = deserializer_impl.loadString(name, element, hex_length, hex_ptr);
        if(ok == false){
            return false;
        }
        char[] hex = hex_ptr[0..hex_length];
        size_t binary_length = getSizeInBinary(hex);

        CallbackCommonData cd = CallbackCommonData(meta, element.meta_index, data.ptr, user_data, malloc);
        ubyte* binary_ptr = cast(ubyte*)getAllocate(element)(&cd, binary_length);
        ubyte[] binary = binary_ptr[0..binary_length];
        hexToBinary(hex, binary);
        free(hex.ptr);

        *ptr_location = binary.ptr;
        size_t length = binary.length;
        memcpy(length_location, &length, length_element.size);
        return true;
    }


    bool loadFixedBinary(const(char)[] name, ubyte[] data, ref SerializerMetaElement element) {
        size_t hex_length;
        char* hex_ptr;
        bool ok = deserializer_impl.loadString(name, element, hex_length, hex_ptr);
        if(ok == false){
            return false;
        }
        scope(exit)free(hex_ptr);
        char[] hex = hex_ptr[0..hex_length];
        size_t binary_length = getSizeInBinary(hex);
        if(binary_length >  element.size) {
            binary_length = element.size;
        } else if(binary_length <  element.size){
            memset(data.ptr, 0, element.size);
        }
        hexToBinary(hex, data[0..binary_length]);
        return true;
    }

}
