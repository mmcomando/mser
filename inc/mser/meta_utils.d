/// Helpers to manipulate meta
/// Defined in header as they are not essential APIs and to allow compile time evaluation
module mser.meta_utils;

import mser;


SerializerReturnCode mser_meta_compute_indices(SerializerMeta* meta){
    foreach(i, ref SerializerMetaElement meta_element; meta.meta_elements){
        meta_element.meta_index = cast(MetaIndex)i;
        foreach(ref SerializerSubobject subobject; meta_element.subobjects){
            bool found = subobject.meta_id == null;
            foreach(i_inner, ref SerializerMetaElement meta_element_inner; meta.meta_elements){
                // printf("(%s) == (%s)\n", subobject.meta_id.ptr, meta_element_inner.id.ptr);
                if(subobject.meta_id == meta_element_inner.id){
                    subobject.meta_index = cast(MetaIndex)i_inner;
                    found = true;
                    break;
                }
            }
            if(found == false){
                // printf("%s\n", subobject.meta_id.ptr);
                return SerializerReturnCode.metaElementNotFound;
            }
        }
    }

    return SerializerReturnCode.ok;
}