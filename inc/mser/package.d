/**
 * mser - Marvelous serializer library - simple interface many possibilities
 *
 * mser library allows to describe the runtime data (ex. int/struct variable in C language) in such a way that serialization and deserialization of data to many formats is very easy. This library often uses or creates metadata, to reduce name lengths we simply call it meta.
 * This properties are achieved by describing runtime data using metadata (type name, alignment, offset). Same meta is used for serialization and deserialization which reduces bugs or amount of code which have to be written. For languages with good introspection meta might be generated automatically.
 * Library contains many backends which allow to serialize/deserialize to many data formats like json, xml or simple binary.
 *
 * Supported formats:
 * - binary simple - embedded implementation
 * - binary compatible - embedded implementation
 * - json - embedded implementation and through cjson library
 * - xml - through libxml2 library
 *
 * Example struct:
 * struct A{
 *     int b;
 * }
 * struct C{
 *     ubyte d;
 *     A e;
 * }
 *
 * Example meta:
 *  SerializerMeta meta_C = SerializerMeta(
 *  [
 *      // SerializerMetaElement(index, id, type, size, alignment, [SerializerSubobject(name, sub_meta_id, offset, meta_index)])
 *      SerializerMetaElement(0, "C", SerializerElementType.container, 8, 4, [SerializerSubobject("d", "u8", 0, 3), SerializerSubobject("e", "A", 4, 1)]),
 *      SerializerMetaElement(1, "A", SerializerElementType.container, 4, 4, [SerializerSubobject("b", "i32", 0, 2)]),
 *      SerializerMetaElement(2, "i32", SerializerElementType.signedInteger, 4, 4, null),
 *      SerializerMetaElement(3, "u8", SerializerElementType.unsignedInteger, 1, 1, null),
 *  ];
 *
 * Example create serializer:
 *  SerializerHandle handle;
    MserCreateInput input_create = SerializerInput(&meta_C);
 *  SerializerReturnCode ret_code = mser_create_cjson(&handle, &input_create); // for json
 *  // SerializerReturnCode ret_code = mser_create_libxml2(&handle, &input_create); // for xml
 *  // SerializerReturnCode ret_code = mser_create_embedded_binary(&handle, &input_create); // for binary
 *
 * Example serialization:
 *  C var;
 *  SerializerInput input = SerializerInput(&var); // SerializerInput(runtime_object_ptr)
 *  SerializerOutput output; // has out_buffer and out_buffer_length
 *  SerializerReturnCode ret_code = mser_serialize(handle, &input, &output);
 *
 * Example deserialization:
 *  C var;
 *  DeserializerInput input = DeserializerInput(&var, 0, serialized_data, serialized_data_length); // DeserializerInput(runtime_object_ptr, meta_index, serialized_data, serialized_data_length);
 *  DeserializerOutput output; // DeserializerOutput(buffer_read_length)
 *  SerializerReturnCode ret_code = mser_deserialize(handle, &input, &output);
 *
 * TODO list:
 * - forward/backward compatibility of api with backends
 * - handling of all possible errors (allocation, backend apis)
 * - const semantic
 * - compatibility: what when loading ubyte[2] to ubyte[1]? Error of silent ignore?
 * - compatibility: what when loading ubyte(200) to byte?
 */
module mser;

extern(C):


////////////////////////////////////////
/////// Runtime Meta API ///////////
////////////////////////////////////////

/// Basic types used to describe runtime objects
enum SerializerElementType : ubyte {
    none = 0, /// No data
    signedInteger = 1, /// Signed integer number
    unsignedInteger = 2, /// Unsigned integer number
    floatingPointNumber = 3, /// Floating point number
    binary = 4, /// Array of bytes; first subobject marks pointer location, second subobjects marks array length location
    binaryCustom_reserved = 5, /// TODO: binary, but memory from callbacks
    binaryFixed = 6, /// Array of N bytes; size of object defines number of bytes
    array = 7, /// Array of elements; first subobject marks pointer location and type, second subobjects marks array length location; TODO: for now length is always considered to be size_t
    arrayCustom = 8, /// Custom array of elements, data is provided in callbacks; first subobject defines array type
    arrayFixed = 9, /// Array of N elements, where N is constant, like int[3] in C language, N is deduced by formula (object.size/subobjects[0].size); first subobject defines array type
    container = 10, /// Object containing subobjects
    containerCustom = 11, /// Container with dynamic number of elements, element contains name and value, each value might have different type, names have to be unique, data is provided in callbacks; subobjects define name value (name field) and value type for this key (meta_index field)
    string = 12, /// Array of bytes, which should be interpreted as string; first subobject marks pointer location, second subobjects marks array length location
    stringCustom = 13, /// Array of bytes, which should be interpreted as string; data is provided in callbacks
    stringFixed_reserved = 14, /// TODO: string, but constant length (char[3])
    pointer = 15, /// Pointer to element, element might be absent, if absent pointer is set to null; first subobject defines pointee type
    pointerCustom_reserved = 16, /// TODO: pointerCustom, but memory from callbacks
}

/// Type to represent index of meta/subobject
alias MetaIndex = short;

/// Description of subobjects stored in runtime object
struct SerializerSubobject {
    const(char)[] name; /// Name of subobject
    const(char)[] meta_id; /// Optional field used for autogenerating meta_index
    uint offset;/// Runtime offset of subobject from start of object
    MetaIndex meta_index; /// Index of meta element in metadata of root describing this subobject
}

/// Description of runtime object
struct SerializerMetaElement {
    MetaIndex meta_index; /// Index of this meta element in metadata of root
    const(char)[] id; /// ID describing type of object. In most cases type name
    SerializerElementType type; /// Serialization element type of this meta element
    int size; /// Runtime size of this meta element
    int alignment; /// Alignment size of this meta element
    /// Subobjects of this meta element
    /// Values here might have different meaning depending on SerializerElementType value
    SerializerSubobject[] subobjects;

    /// Callback triggered when object allocation has to be made
    SerializerAllocationCallback on_allocate;
    /// Callback triggered when object has to be freed
    SerializerFreeCallback on_free;

    /// Callback triggered on deserialization failure when data has to be cleaned up
    SerializerDestroy on_destroy;
    /// Callback triggered when object loading was finished
    SerializerOnLoadFinishCallback on_load_finish;
    /// Callback triggered when object was not found during deserialization
    SerializerOnMissingFieldCallback on_missing_field;

    /// Callback triggered on saving of containerCustom
    SerializerSaveContainer on_save_container;
    /// Callback triggered on saving of containerCustom
    SerializerLoadContainer on_load_container;

    /// Callback triggered on saving of arrayCustom
    SerializerSaveArrayCallback on_save_array;
    /// Callback triggered on loading of arrayCustom
    SerializerLoadArrayCallback on_load_array;

    /// Callback triggered on saving of stringCustom
    SerializerSaveStringCallback on_save_string;
    /// Callback triggered on loading of stringCustom
    SerializerLoadStringCallback on_load_string;

    /// Callback triggered on custom save
    SerializerSaveCallback on_save;
    /// Callback triggered on custom load
    SerializerLoadCallback on_load;
}

/// Array of meta elements
struct SerializerMeta {
    /// All meta elements stored in one array
    /// Fields like SerializerSubobject.meta_index and SerializerMetaElement.meta_index refer to element index in this array
    SerializerMetaElement[] meta_elements;
}

version(mser_backend_implementation){
    // Shouldn't be known for api users, only for backend implementations
    struct SerializerHandleImpl {
        void* backend_impl;
        SerializerReturnCode function(SerializerHandle handle) destroy;
        SerializerReturnCode function(SerializerHandle handle, SerializerInput* input, SerializerOutput* output) serialize;
        SerializerReturnCode function(SerializerHandle handle, DeserializerInput* input, DeserializerOutput* output) deserialize;
        SerializerReturnCode function(SerializerHandle handle, SerializerProperties properties) has_properties;
    }
} else {
    /// Handle to serializer/deserializer
    // TODO: This should be forward reference but in dub build there is error with missing type info
    struct SerializerHandleImpl
    {

    }
}

/// Handle to backend instance
alias SerializerHandle = SerializerHandleImpl*;

/// Return codes returned by all mser apis
enum SerializerReturnCode : uint {
    ok,
    notInitialized,
    alreadyInitialized,
    noSerializerBackend,
    operationNotSupportedByImpl,
    allocationError,
    badParameter,
    serImplInitializationError,
    propertiesNotSupported,
    metaElementNotFound,
    notEnoughData,
    metaNotFound,
}


////////////////////////////////////////
///// Main Interface Functions /////////
////////////////////////////////////////


alias MallocFunc = void* function(size_t size);
alias FreeFunc = void function(void* ptr);

/// Input parameters used during serialization
struct MserCreateInput{
    SerializerMeta* meta; /// All meta used in serialization
    /// Pointer to function used instead of malloc in given handle, if null malloc is used
    MallocFunc malloc;
    /// Pointer to function used instead of free in given handle, by default it is free
    FreeFunc free;
    // TODO improve: allocation callback so user can allocate different objects with different allocators
    //  but for now use mser_malloc for all allocations
}
/// Create handle to serializer object for json loading, implementation embedded in mser library
SerializerReturnCode mser_create_embedded_json(SerializerHandle* handle, MserCreateInput* input);
/// Create handle to serializer object for json, implementation uses cjson library
SerializerReturnCode mser_create_cjson(SerializerHandle* handle, MserCreateInput* input);
/// Create handle to serializer object for xml, implementation uses libxml2 library
SerializerReturnCode mser_create_libxml2(SerializerHandle* handle, MserCreateInput* input);
/// Create handle to serializer object for simple binary format, implementation embedded in mser library
SerializerReturnCode mser_create_embedded_binary(SerializerHandle* handle, MserCreateInput* input);
/// Create handle to serializer object for binary format, implementation embedded in mser library
SerializerReturnCode mser_create_embedded_binary_compatible(SerializerHandle* handle, MserCreateInput* input);
/// Create handle to serializer object for toml loading
SerializerReturnCode mser_create_toml(SerializerHandle* handle, MserCreateInput* input);
// Create handle to serializer object for user custom format, user format might be supported by implementing SerializerHandleImpl interface
// SerializerReturnCode mser_create_my_special_format(SerializerHandle* handle, MserCreateInput* input);

/// Destroy serializer object pointed by handle
SerializerReturnCode mser_destroy(SerializerHandle* handle);

/// Input parameters used during serialization
struct SerializerInput{
    const void* runtime_data_ptr; /// Pointer to runtime data
    void* user_data; /// Pointer to user data
    MetaIndex meta_index; /// Index of meta element which describes runtime_data_ptr
}

/// Output parameters used during deserialization
struct SerializerOutput{
    ubyte* out_buffer; /// Serialization buffer allocated by mser using mser_malloc, has to be freed using mser_free by user
    size_t out_buffer_length; /// Length of out buffer
}
/// Serialize runtime object specified in input to output byte array using serializer from handle
SerializerReturnCode mser_serialize(SerializerHandle handle, SerializerInput* input, SerializerOutput* output);

/// Input parameters used during deserialization
struct DeserializerInput{
    void* runtime_data_ptr; /// Pointer to runtime data
    void* user_data; /// Pointer to user data
    MetaIndex meta_index; /// Index of meta element which describes runtime_data_ptr
    ubyte* in_buffer; /// Buffer to serialized data, data have to be in format supported by handle backend
    size_t in_buffer_length; /// Buffer length
}

/// Output parameters used during deserialization
struct DeserializerOutput{
    size_t buffer_read_length;/// Number of bytes read from input buffer
}

/// Deserialize input byte array to runtime object specified in output using serializer from handle
SerializerReturnCode mser_deserialize(SerializerHandle handle, DeserializerInput* input, DeserializerOutput* output);

/// Flags describing properties of specific backend
enum SerializerProperties : ulong {
    none = 0,
    canSerialize = 1,
    canDeserialize = 2,
    isBinary = 4,
    supportsNotPresentData = 8,
    // TODO improvement: more properties
    // forwardCompatible,
    // backwardCompatible,
    // isEndianStable,
    // makesAllocations,
}

/// Queries properties supported by handle
/// returns SerializerReturnCode.operationNotSupportedByImpl if backend doesn't know about properties
/// returns SerializerReturnCode.propertiesNotSupported if backend doesn't support properties
/// returns SerializerReturnCode.ok if backend supports properties
SerializerReturnCode mser_has_properties(SerializerHandle handle, SerializerProperties properties);

/// Calls destroy callbacks and frees memory allocated by 'runtime_data_ptr' with type represented by 'index'
/// It is assumed that variable is created by serializer
/// Primary use case is destroying objects during cleanup in case of serialization failure
SerializerReturnCode mser_destroy_variable(SerializerMeta* meta, MetaIndex index, void* runtime_data_ptr, FreeFunc free);

/// Compute meta indices in SerializerMeta
SerializerReturnCode mser_meta_compute_indices(SerializerMeta* meta);
/// Return index of meta element with name
SerializerReturnCode mser_meta_get_index(SerializerMeta* meta, const(char)* str, size_t str_length, MetaIndex* index);
/// Validate if meta doesn't have simple errors (missing subobjects, etc), prints errors to stdio, returns badParameter if there is error otherwise ok
SerializerReturnCode mser_meta_validate(SerializerMeta* meta);

/// Returns string representation of SerializerElementType enum value or UNKNOWN if value not recognized
const(char)* mser_enum_str_SerializerElementType(SerializerElementType val);
/// Returns string representation of SerializerReturnCode enum value or UNKNOWN if value not recognized
const(char)* mser_enum_str_SerializerReturnCode(SerializerReturnCode val);
/// Returns string representation of SerializerProperties enum value or UNKNOWN if value not recognized
const(char)* mser_enum_str_SerializerProperties(SerializerProperties val);

////////////////////////////////////////
///////////// Callbacks ////////////////
////////////////////////////////////////

/// Callback executed when element is not found
alias SerializerOnMissingFieldCallback = void function(SerializerMeta* meta, void* runtime_data_ptr, MetaIndex element_index, void* user_data);
alias SerializerOnLoadFinishCallback = void function(SerializerMeta* meta, void* runtime_data_ptr, MetaIndex element_index, void* user_data);

/// Data provided by custom deserialization callback
struct CallbackCommonData {
    SerializerMeta* meta; /// All meta used in serialization
    MetaIndex element_index; /// Meta index for current element //TODO: rename
    void* runtime_data_ptr; /// Pointer to runtime data for current element
    void* user_data; /// Pointer to user data
    void* context; /// Context of serialization, to be passed in callbacks
}

alias SerializerSaveContainerStartCallback = void function(void* context, size_t length);

/// Callback for object memory allocation, returns memory
alias SerializerAllocationCallback = void* function(CallbackCommonData* input, size_t length);
/// Callback to free object memory
alias SerializerFreeCallback = void function(CallbackCommonData* input, void* data);

/// Data provided by on_destroy serialization callback
struct DestroyInput{
    CallbackCommonData cd;
}

alias SerializerDestroy = void function(DestroyInput* input);

/// Data provided by containerCustom serialization callback
struct SerializeContainerCustomInput {
    CallbackCommonData cd; /// Common callback data
    // TODO: maybe end callback so serializer can cleanup/fix its data?
    SerializerSaveContainerStartCallback serialize_start; /// Inform serializer about number of elements to be saved
    SerializerSaveContainerNameElementCallback serialize_element; /// Serialize one element of a container
}
/// Data provided by ContainerCustom deserialization callback
struct DeserializeContainerCustomInput {
    CallbackCommonData cd; /// Common callback data
    size_t length; /// Number of elements in container, user has to call deserialize_name and deserialize_element that many times
    SerializerLoadContainerNameCallback deserialize_name; /// Deserialization of one name in container
    SerializerLoadContainerElementCallback deserialize_element; /// Deserialization of one element in container
}

alias SerializerSaveContainerNameElementCallback = SerializerReturnCode function(void* context, MetaIndex name_index, void* runtime_data_ptr);
alias SerializerLoadContainerNameCallback = SerializerReturnCode function(void* context, MetaIndex* name_index);
alias SerializerLoadContainerElementCallback = SerializerReturnCode function(void* context, void* runtime_data_ptr);

alias SerializerSaveContainer = void function(SerializeContainerCustomInput* input);
alias SerializerLoadContainer = void function(DeserializeContainerCustomInput* input);


/// Data provided by custom array serialization callback
struct SerializeArrayInput {
    CallbackCommonData cd; /// Common callback data
    SerializerLoadArrayStartCallback serialize_start; /// Inform serializer about number of elements to be saved
    SerializeLoadSaveArrayElementCallback serialize_element; /// Serialize one element of an array
}
/// Data provided by custom array deserialization callback
struct DeserializeArrayInput {
    CallbackCommonData cd; /// Common callback data
    size_t length; /// Number of elements in array, user has to call deserialize_element that many times
    SerializeLoadSaveArrayElementCallback deserialize_element; /// Deserialization of one element in array
}

alias SerializerLoadArrayStartCallback = SerializerReturnCode function(void* context, size_t length);
alias SerializeLoadSaveArrayElementCallback = SerializerReturnCode function(void* context, void* runtime_data_ptr);

alias SerializerSaveArrayCallback = void function(SerializeArrayInput* input);
alias SerializerLoadArrayCallback = void function(DeserializeArrayInput* input);

/// Data provided by custom string serialization callback
struct SerializeStringInput {
    CallbackCommonData cd; /// Common callback data
    SerializeSaveStringElementCallback serialize_string; /// Serialize string
}
/// Data provided by custom string deserialization callback
struct DeserializeStringInput {
    CallbackCommonData cd; /// Common callback data
    char* str; /// Pointer to loaded string, memory is owned by serializer and is invalid after callback execution
    size_t length; /// Length of string
}

alias SerializeSaveStringElementCallback = SerializerReturnCode function(void* context, const (char)* str, size_t length);

alias SerializerSaveStringCallback = void function(SerializeStringInput* input);
alias SerializerLoadStringCallback = void function(DeserializeStringInput* input);


/// Data provided by custom serialization callback
struct SerializeCustomInput {
    CallbackCommonData cd; /// Common callback data
    SerializeLoadSaveCallback serialize; /// Serialize data
}
/// Data provided by custom deserialization callback
struct DeserializeCustomInput {
    CallbackCommonData cd; /// Common callback data
    SerializeLoadSaveCallback deserialize; /// Deserialize data
}

alias SerializeLoadSaveCallback = SerializerReturnCode function(void* context, void* runtime_data_ptr);

alias SerializerSaveCallback = void function(SerializeCustomInput* input);
alias SerializerLoadCallback = void function(DeserializeCustomInput* input);
