/// Helper to generate meta in D language
module mser.meta_generator;

import std.traits : isDynamicArray;

import mser;


static bool isString(T)() {
    return is(T==const(char)[]) || is(T==char[]) || is(T==string);
}


private int getMetaTypesNumMax(T)() {
    enum bool isContainer = is(T == struct);
    static if(isContainer) {
        int num = 1;
        foreach (i, a; T.init.tupleof) {
            alias Type = typeof(a);
            num += getMetaTypesNumMax!Type;
        }
        return num;
    } else static if(isString!T) {
        return 2; // Main type + length
    } else static if(isDynamicArray!T) {
        return 2 + getMetaTypesNumMax!(typeof(T.init[0])); // Main type + length + array element
    } else {
        return 1;
    }

}

// auto getStr(string a, string b)(){
//     enum size_t len = a.length+b.length+1;
//     char[len] arr;
//     foreach(i, char c; a){
//         arr[i] = c;
//     }
//     foreach(i, char c; b){
//         arr[a.length+i] = c;
//     }
//     arr[len-1] = '\0';
//     return arr;
// }


struct SerializeMetaGenerator(MainType) {
    __gshared SerializerMetaElement[getMetaTypesNumMax!MainType] meta_elements;
    MetaIndex metas_generated_num;
    __gshared SerializerSubobject[100] subobjects_pool;
    int subobject_pool_used;

    static const(char)[] getIdentifier(T)() {
        static if(is(T==enum)) {
            return T.stringof;
        } else static if(__traits(isIntegral, T) && __traits(isUnsigned, T)) {
            enum size = T.sizeof;
            static if(size == 1) return "u8";
            else static if(size == 2) return "u16";
            else static if(size == 4) return "u32";
            else static if(size == 8) return "u64";
            else static assert(0, "No name for type with given size");
        } else static if(__traits(isIntegral, T) && __traits(isUnsigned, T) == false) {
            enum size = T.sizeof;
            static if(size == 1) return "i8";
            else static if(size == 2) return "i16";
            else static if(size == 4) return "i32";
            else static if(size == 8) return "i64";
            else static assert(0, "No name for type with given size");
        } else static if(is(T == struct)){
            return __traits(identifier, T);
        } else static if(isString!(T)){
            return "STRING";
        } else static if(isDynamicArray!(T)){
            return T.stringof;
        } else{
            static assert(0, "Type not supported");
        }
    }

    static SerializerElementType getType(T)() {
        static if(__traits(isIntegral, T) && __traits(isUnsigned, T)){
            return SerializerElementType.unsignedInteger;
        } else static if(__traits(isIntegral, T) && __traits(isUnsigned, T) == false){
            return SerializerElementType.signedInteger;
        } else static if(is(T == struct)){
            return SerializerElementType.container;
        } else static if(isString!(T)){
            return SerializerElementType.string;
        } else static if(isDynamicArray!(T)){
            return SerializerElementType.array;
        } else{
            static assert(0, "Type not supported");
        }
    }

    MetaIndex getIndexForType(T)() {
        auto id = getIdentifier!T;

        foreach(i, ref element; meta_elements[0..metas_generated_num]) {
            if(element.id == id) return cast(MetaIndex)i;
        }

        MetaIndex index = generateBasicMeta!(T)();
        enum SerializerElementType type = getType!T;
        meta_elements[index].type = type;
        meta_elements[index].id = id;

        static if(type==SerializerElementType.container) {
            generateMetaForContainer!T(meta_elements[index]);
        }else static if(type==SerializerElementType.array) {
            generateMetaForArray!T(meta_elements[index]);
        }else static if(type==SerializerElementType.string) {
            generateMetaForString!T(meta_elements[index]);
        }else static if(type == SerializerElementType.unsignedInteger || type == SerializerElementType.signedInteger) {
            // All fields are properly set by common configuration
        }else{
            static assert(0, "Type not supported");
        }

        return index;
    }

    MetaIndex generateBasicMeta(T)() {
        MetaIndex meta_index = metas_generated_num;
        metas_generated_num = cast(MetaIndex)(metas_generated_num + 1);

        SerializerMetaElement meta;
        meta.meta_index = meta_index;
        meta.size = T.sizeof;
        meta.alignment = T.alignof;

        meta_elements[meta_index] = meta;
        return meta_index;
    }

    void generateMetaForContainer(T)(ref SerializerMetaElement meta) {
        SerializerSubobject[] subobjects = subobjects_pool[subobject_pool_used .. subobject_pool_used + T.init.tupleof.length];
        subobject_pool_used += T.init.tupleof.length;
        T var;
        foreach (i, ref a; var.tupleof) {
            alias Type = typeof(a);
            enum SerializerElementType type = getType!Type;
            enum int offset = T.init.tupleof[i].offsetof;
            enum string varNameTmp = __traits(identifier, var.tupleof[i]);
            MetaIndex index = getIndexForType!Type;
            subobjects[i] = SerializerSubobject(varNameTmp, meta_elements[index].id, offset, index);
        }
        meta.subobjects = subobjects;
    }

    void generateMetaForString(T)(ref SerializerMetaElement meta) {
        MetaIndex index_size = getIndexForType!size_t;
        auto id_size = getIdentifier!size_t;

        SerializerSubobject[] subobjects = subobjects_pool[subobject_pool_used .. subobject_pool_used + 2];
        subobject_pool_used += 2;

        subobjects[0] = SerializerSubobject("PTR", "PTR", size_t.sizeof, -1);
        subobjects[1] = SerializerSubobject("LENGTH", id_size, 0, index_size);
        meta.subobjects = subobjects;
    }

    void generateMetaForArray(T)(ref SerializerMetaElement meta) {
        MetaIndex index_size = getIndexForType!size_t;
        auto id_size = getIdentifier!size_t;

        MetaIndex index_value = getIndexForType!(typeof(T.init[0]));
        auto id_value = getIdentifier!size_t;

        SerializerSubobject[] subobjects = subobjects_pool[subobject_pool_used .. subobject_pool_used + 2];
        subobject_pool_used += 2;

        subobjects[0] = SerializerSubobject("PTR", id_value, size_t.sizeof, index_value);
        subobjects[1] = SerializerSubobject("LENGTH", id_size, 0, index_size);
        meta.subobjects = subobjects;
    }

    static auto getGeneratedObject() {
        auto gen = SerializeMetaGenerator!(MainType)();
        gen.getIndexForType!MainType();
        return gen;
    }
    static auto getMeta() {
        auto  gen = getGeneratedObject();
        return SerializerMeta(gen.meta_elements[0..gen.metas_generated_num]);
    }

}

