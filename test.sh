#! /usr/bin/env nix-shell
#! nix-shell -i bash --pure -p meson ninja ldc clang_12 gdb pkg-config cjson libxml2 python39Full python39Packages.pandas python39Packages.numpy python39Packages.matplotlib linuxPackages.perf

set -euo pipefail
IFS=$'\n\t'

dubug_path="build"
test_optimize_path="test_optimize"
fuzzing_path="test_fuzzing"
cmd_tool_path="cmd_tool"

function list_pkg_config_packages() {
    # Useful for finding meson dependencies
    pkg-config --list-all
}

function build_debug_clean() {
    rm -rf $dubug_path
    meson $dubug_path -Dbuild_tests=true -Denable_asan=true -Dbuild_benchmark=true
}

function build_debug() {
    ninja -C $dubug_path
}

function build_test_optimization_clean() {
    rm -rf $test_optimize_path
    meson $test_optimize_path -Dbuild_tests=true -Dbuildtype=debugoptimized -Dbuild_benchmark=true
}

function build_test_optimization() {
    ninja -C $test_optimize_path
}

function build_fuzzing_clean() {
    rm -rf $fuzzing_path
    meson $fuzzing_path -Dbuild_tests=true -Denable_fuzzing=true
}

function build_fuzzing() {
    ninja -C $fuzzing_path
}

function build_cmd_tool_clean() {
    rm -rf $cmd_tool_path
    meson $cmd_tool_path -Denable_asan=true -Dbuild_cmd_tool=true -Ddefault_library=static
}

function build_cmd_tool() {
    ninja -C $cmd_tool_path
}

function run_gdb() {
    exe_path=$1

    gdb \
        -ex "break _d_assert_msg" \
        -ex run \
        $exe_path
}


export DATA_PATH="./test/data/"

###################### Fuzzing ######################
function run_fuzzing() {
    # build_fuzzing_clean
    build_fuzzing
    ./$fuzzing_path/test/mser_fuzzing -max_len=9 ./test/data/fuzzerA ./test/data/fuzzerB

}

###################### cmd tool ######################
function run_cmd_tool() {
    # build_cmd_tool_clean
    build_cmd_tool
    # readelf -d ./$cmd_tool_path/src_cmd_tool/mser_cmd_tool # Print shared library dependencies
    ./$cmd_tool_path/src_cmd_tool/mser_cmd_tool --input=game_scene.bincp --output=game_scene_cmd.json --out_format=json

}

###################### Optimized ######################
function run_optimized() {
    # build_test_optimization_clean
    build_test_optimization
    # run_gdb ./$test_optimize_path/test/mser_benchmark
    ./$test_optimize_path/test/mser_benchmark
    perf record ./$test_optimize_path/test/mser_benchmark
    # sh ./test/make_graphs.sh
}

###################### Debug ######################
function run_debug() {
    # build_debug_clean
    build_debug
    # run_gdb ./$dubug_path/test/mser_tests
    ./$dubug_path/test/mser_tests
}


build_cmd_tool_clean

###################### Run ######################
# list_pkg_config_packages
# run_fuzzing
# run_cmd_tool
# run_optimized
run_debug
