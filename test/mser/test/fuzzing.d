module mser.test.fuzing;

import core.stdc.stdlib;

import mser;

struct TestRoot {
    ubyte a;
    TestChild b;
    TestChild c;
    TestChild[] arr;
}

struct TestChild {
    ubyte d;
    int e;
}

enum SerializerMeta g_meta_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "TestRoot", SerializerElementType.container, 40, 8, [SerializerSubobject("a", "u8", 0u, 3), SerializerSubobject("b", "TestChild", 4u, 1), SerializerSubobject("c", "TestChild", 12u, 1), SerializerSubobject("arr", "__ARRAY_TestChild", 24u, 2)]),
        SerializerMetaElement(1, "TestChild", SerializerElementType.container, 8, 8, [SerializerSubobject("d", "u8", 0u, 3), SerializerSubobject("e", "i32", 4u, 4)]),
        SerializerMetaElement(2, "__ARRAY_TestChild", SerializerElementType.array, 16, 8, [SerializerSubobject("", "TestChild", 8u, 1),SerializerSubobject("", "", 0u, -1)]),
        SerializerMetaElement(3, "u8", SerializerElementType.unsignedInteger, 1, 1, null),
        SerializerMetaElement(4, "i32", SerializerElementType.signedInteger, 4, 4, null),
    ],
    null
);
 __gshared SerializerMeta g_meta = g_meta_enum;

void fuzzDeserialize(const(ubyte[]) data){

    SerializerHandle handle;
    MserCreateInput create_input = MserCreateInput(&g_meta);
    SerializerReturnCode ret_code = mser_create_embedded_binary_compatible(&handle, &create_input);
    if(ret_code != SerializerReturnCode.ok) exit(1);
    scope(exit) mser_destroy(&handle);

    TestRoot obj;
    DeserializerInput input_de = DeserializerInput(&obj, null, 0, cast(ubyte*)data.ptr, data.length);
    DeserializerOutput output_de;

    mser_deserialize(handle, &input_de, &output_de);
}


extern (C) int LLVMFuzzerTestOneInput(const(ubyte*) data, size_t size)
{
    fuzzDeserialize(data[0 .. size]);
    return 0;
}
