module mser.test.game_scene;

import core.stdc.stdlib;
import core.stdc.string;
import std.file : readText;
import std.process : environment;
import std.stdio;
import std.datetime.stopwatch;

import mser;
import mser.test.structs;


private:
//// Basic game engine types

struct vec3
{
    // union
    // {
        float[3] data;
    //     struct
    //     {
    //         float x,y,z;
    //     }
    //     struct
    //     {
    //         float r,g,b;
    //     }
    // }
}

struct quat
{
    // union
    // {
    //     float[4] data;
        struct
        {
            float x,y,z,w;
        }
    // }
}

struct mat4
{
    float[16] data;
}

struct AABB
{

	vec3 min = vec3(-1);
	vec3 max = vec3(1);
}


//// Components

struct CName
{
    const(char)[] value;
}

struct CLocation
{
    vec3 value;
}

struct CRotation
{
    quat value;
}

struct CScale
{
    vec3 value;
}

struct Mesh3DModule {
    const(char)[] mesh;
    const(char)[][] materials;
}

struct CMesh
{
    byte[] data; // No serialize
    uint[] offsets; // No serialize
    // AABB aabb; // No serialize

private:
    Mesh3DModule m_mesh; // Custom serialize using UUID-s
}

struct CAABB
{
    AABB aabb;
}

struct CScene
{
    // SceneObject* object; // No serialize
}

struct CRenderMatrix
{
    mat4 matrix;
}

struct CRenderMask
{
    uint masks;
    uint layer_mask;
    uint order;
}

struct CStatic
{
    enum Type : ubyte
    {
        static_ = 0,
        mixed = 1,
        baked = 2
    }

    Type type;
}

// struct CAnimation
// {
//     hAnimation animation; // no serialize
//     Animation.FinalPose pose; // no serialize
//     UUID uuid;
// }

//// High level types

struct Component{
    int type;
    // Without union writeln prints all fields so it is easier to debug
    // union {
        CName name;
        CLocation location;
        CRotation rotation;
        CScale scale;
        CMesh mesh;
    // }
}

struct Entity{
    Component[] components;
}

struct Plugin2{
    const(char)[] data;
}

struct TileMapPlugin{
    const(char)[][2][] tile_sets;
    ubyte[] tile_map;
}

struct Plugin{
    int type;
    Plugin2 plugin2;
    TileMapPlugin tile_map_plugin;
}

struct Description{
    const(char)[] name;
    const(char)[] description;
    const(char)[] engine;
}

struct GameScene {
    const(char)[] file_type;
    const(char)[] version_;
    Description description;
    Entity[] entities;
    Plugin[] plugins;
    static destroy(ref GameScene data){
        // This is correct but causes big perf regression in tests
        // mser_destroy_variable(&g_meta, 0, &data, &free);

        foreach(ref Entity ent; data.entities){
            foreach(ref Component comp; ent.components){
                foreach(ref mat; comp.mesh.m_mesh.materials){
                    free(cast(void*)mat.ptr);
                }
                free(cast(void*)comp.name.value.ptr);
                free(cast(void*)comp.mesh.m_mesh.mesh.ptr);
                free(cast(void*)comp.mesh.m_mesh.materials.ptr);
            }
            free(cast(void*)ent.components.ptr);

        }
        foreach(ref Plugin pl; data.plugins){
            free(cast(void*)pl.plugin2.data.ptr);
            free(cast(void*)pl.tile_map_plugin.tile_map.ptr);
            foreach(ref tile; pl.tile_map_plugin.tile_sets){
                free(cast(void*)tile[0].ptr);
                free(cast(void*)tile[1].ptr);
            }
            free(cast(void*)pl.tile_map_plugin.tile_sets.ptr);
        }
        free(cast(void*)data.plugins.ptr);

        free(cast(void*)data.file_type.ptr);
        free(cast(void*)data.version_.ptr);
        free(cast(void*)data.description.name.ptr);
        free(cast(void*)data.description.description.ptr);
        free(cast(void*)data.description.engine.ptr);
        free(cast(void*)data.entities.ptr);
    }
}

// csme => CreateSerializerMetaElement
// Helpers for creating meta object

SerializerMetaElement csme(EL, string name, SerializerElementType type, string[3][] subobjects_str)(){
    SerializerMetaElement el = SerializerMetaElement(-1, name, type);
    el.size = EL.sizeof;
    el.alignment = EL.alignof;
    SerializerSubobject[] subobjects;
    SerializerSubobject subobject;
    static foreach(string[3] subobject_str; subobjects_str){
        subobject.name = subobject_str[0];
        subobject.meta_id = subobject_str[1];
        subobject.offset = __traits(getMember, EL, subobject_str[2]).offsetof;
        subobjects ~= subobject;
    }
    el.subobjects = subobjects;
    return el;
}

SerializerMetaElement csme_array(string name, string array_element_id)(){
    SerializerMetaElement el = SerializerMetaElement(-1, name, SerializerElementType.array);
    el.size = (ubyte[]).sizeof;
    el.alignment = (ubyte[]).alignof;
    el.subobjects ~= SerializerSubobject("", array_element_id, 8u, -1);
    el.subobjects ~= SerializerSubobject("", "", 0, -1);
    return el;
}

SerializerMetaElement csme_fixed_array(EL, string name, string array_element_id, int array_length)(){
    SerializerMetaElement el = SerializerMetaElement(-1, name, SerializerElementType.arrayFixed);
    el.size = EL.sizeof*array_length;
    el.alignment = EL.alignof;
    el.subobjects ~= SerializerSubobject("", array_element_id, 0, -1);
    return el;
}

SerializerMetaElement csme_container_custom(EL, string name, alias FUNC_load, alias FUNC_save, alias FUNC_destroy, string[2][] subobjects_str)(){
    SerializerMetaElement el = SerializerMetaElement(-1, name, SerializerElementType.containerCustom);

    SerializerSubobject[] subobjects;
    SerializerSubobject subobject;
    static foreach(string[2] subobject_str; subobjects_str){
        subobject.name = subobject_str[0];
        subobject.meta_id = subobject_str[1];
        subobjects ~= subobject;
    }
    el.subobjects = subobjects;

    el.on_load_container = &FUNC_load;
    el.on_save_container = &FUNC_save;
    el.on_destroy = &FUNC_destroy;
    return el;
}

enum SerializerMeta g_meta_enum = SerializerMeta(
    [
        // Higher level objects
        csme!(GameScene, "GameScene", SerializerElementType.container, [["Entities", "ARRAY_Entity", "entities"], ["FileType", "STRING", "file_type"], ["Version", "STRING", "version_"], ["Description", "Description", "description"], ["Plugins", "PluginsCustom", "plugins"]]),
        csme!(Description, "Description", SerializerElementType.container, [["Name", "STRING", "name"], ["Description", "STRING", "description"], ["Engine", "STRING", "engine"]]),
        csme!(Entity, "Entity", SerializerElementType.container, [["Components", "ComponentsCustom", "components"]]),
        // Components
        csme!(CName, "bubel.entity.g3d.components.name.CName", SerializerElementType.container, [["value", "STRING", "value"]]),
        csme!(CMesh, "bubel.entity.g3d.components.mesh.CMesh", SerializerElementType.container, [["m_mesh", "Mesh3DModule", "m_mesh"]]),
        csme!(Mesh3DModule, "Mesh3DModule", SerializerElementType.container, [["mesh", "STRING", "mesh"], ["materials", "ARRAY_Materials", "materials"]]),
        csme!(CAABB, "bubel.entity.g3d.components.aabb.CAABB", SerializerElementType.container, null),
        csme!(CRenderMatrix, "bubel.entity.g3d.components.render_matrix.CRenderMatrix", SerializerElementType.container, null),
        csme!(CRenderMask, "bubel.entity.g3d.components.render_mask.CRenderMask", SerializerElementType.container, [["masks", "u32", "masks"], ["order", "u32", "order"]]),
        csme!(CStatic, "bubel.entity.g3d.components.static_.CStatic", SerializerElementType.container, [["type", "u8", "type"]]),
        csme!(CLocation, "bubel.entity.g3d.components.location.CLocation", SerializerElementType.container, [["value", "ARRAY_3_f32", "value"]]),
        csme!(CRotation, "bubel.entity.g3d.components.rotation.CRotation", SerializerElementType.container, [["value", "ARRAY_4_f32", "value"]]),
        csme!(CScale, "bubel.entity.g3d.components.scale.CScale", SerializerElementType.container, [["value", "ARRAY_3_f32", "value"]]),
        // Plugins
        csme!(TileMapPlugin, "TileMapPlugin", SerializerElementType.container, [["TileMap", "ARRAY_u8", "tile_map"], ["TileSets", "ARRAY_ARRAY_2_STRING", "tile_sets"]]),
        csme!(Plugin2, "Plugin2", SerializerElementType.container, [["Data", "STRING", "data"]]),
        // Custom containers
        csme_container_custom!(Component, "ComponentsCustom", on_load_components, on_save_components, on_destroy_components, [
            ["none", "none"],
            ["bubel.entity.g3d.components.name.CName", "bubel.entity.g3d.components.name.CName"],
            ["bubel.entity.g3d.components.location.CLocation", "bubel.entity.g3d.components.location.CLocation"],
            ["bubel.entity.g3d.components.rotation.CRotation", "bubel.entity.g3d.components.rotation.CRotation"],
            ["bubel.entity.g3d.components.scale.CScale", "bubel.entity.g3d.components.scale.CScale"],
            ["bubel.entity.g3d.components.mesh.CMesh", "bubel.entity.g3d.components.mesh.CMesh"],
        ]),
        csme_container_custom!(Plugin, "PluginsCustom", on_load_plugins, on_save_plugins, on_destroy_plugins, [
            ["none", "none"],
            ["TileMapPlugin", "TileMapPlugin"],
            ["Plugin2", "Plugin2"]
        ]),
        // Arrays
        csme_array!("ARRAY_Entity", "Entity"),
        csme_array!("ARRAY_Materials", "STRING"),
        csme_array!("ARRAY_u8", "u8"),
        csme_array!("ARRAY_ARRAY_2_STRING", "ARRAY_2_STRING"),
        csme_array!("ARRAY_STRING", "STRING"),
        // Fixed Arrays
        csme_fixed_array!(string, "ARRAY_2_STRING", "STRING", 2),
        csme_fixed_array!(float, "ARRAY_3_f32", "f32", 3),
        csme_fixed_array!(float, "ARRAY_4_f32", "f32", 4),
        // String
        SerializerMetaElement(-1, "STRING", SerializerElementType.string, 16, 8, [SerializerSubobject(null, null, size_t.sizeof, -1), SerializerSubobject(null, "_STRING_LENGTH", 0, -1)]),
        csme!(size_t, "_STRING_LENGTH", SerializerElementType.unsignedInteger, null),
        // Basic types
        csme!(float, "f32", SerializerElementType.floatingPointNumber, null),
        csme!(uint, "u32", SerializerElementType.unsignedInteger, null),
        csme!(int, "i32", SerializerElementType.signedInteger, null),
        csme!(ubyte, "u8", SerializerElementType.signedInteger, null),
        SerializerMetaElement(-1, "none", SerializerElementType.none, 0, 0, null),
    ],
);


__gshared SerializerMeta g_meta = g_meta_enum;

__gshared MetaIndex g_TileMapPlugin_container_index= 1;
__gshared MetaIndex g_Plugin2_container_index= 2;

__gshared MetaIndex g_TileMapPlugin_index;
__gshared MetaIndex g_Plugin2_index;

// TODO: This indices are strange, maybe use full name, but then what about performance?
__gshared MetaIndex g_none_container_index= 0;
__gshared MetaIndex g_name_container_index= 1;
__gshared MetaIndex g_location_container_index= 2;
__gshared MetaIndex g_rotation_container_index= 3;
__gshared MetaIndex g_scale_container_index= 4;
__gshared MetaIndex g_mesh_container_index= 5;

__gshared MetaIndex g_name_index;
__gshared MetaIndex g_location_index;
__gshared MetaIndex g_rotation_index;
__gshared MetaIndex g_scale_index;
__gshared MetaIndex g_mesh_index;


// Initialize indexes
shared static this()
{
    assert(mser_meta_compute_indices(&g_meta) == SerializerReturnCode.ok); // So we don't have to fill them correctly in meta

    assert(mser_meta_get_index(&g_meta, "TileMapPlugin", "TileMapPlugin".length, &g_TileMapPlugin_index) == SerializerReturnCode.ok);
    assert(mser_meta_get_index(&g_meta, "Plugin2", "Plugin2".length, &g_Plugin2_index) == SerializerReturnCode.ok);

    assert(mser_meta_get_index(&g_meta, "bubel.entity.g3d.components.name.CName", "bubel.entity.g3d.components.name.CName".length, &g_name_index) == SerializerReturnCode.ok);
    assert(mser_meta_get_index(&g_meta, "bubel.entity.g3d.components.location.CLocation", "bubel.entity.g3d.components.location.CLocation".length, &g_location_index) == SerializerReturnCode.ok);
    assert(mser_meta_get_index(&g_meta, "bubel.entity.g3d.components.rotation.CRotation", "bubel.entity.g3d.components.rotation.CRotation".length, &g_rotation_index) == SerializerReturnCode.ok);
    assert(mser_meta_get_index(&g_meta, "bubel.entity.g3d.components.scale.CScale", "bubel.entity.g3d.components.scale.CScale".length, &g_scale_index) == SerializerReturnCode.ok);
    assert(mser_meta_get_index(&g_meta, "bubel.entity.g3d.components.mesh.CMesh", "bubel.entity.g3d.components.mesh.CMesh".length, &g_mesh_index) == SerializerReturnCode.ok);
}

bool flCmp(float val, float expected){
    float dt = (val > expected) ? val - expected : expected - val;
    return dt < 0.1;
}

void extendArray(T)(T[]* arr, size_t new_length){
    void* old_ptr = arr.ptr;
    T* new_arr = cast(T*)malloc(T.sizeof * new_length);
    memcpy(new_arr, old_ptr, arr.length * T.sizeof);
    memset(new_arr + arr.length, 0, (new_length - arr.length) * T.sizeof);
    free(old_ptr);
    *arr = new_arr[0..new_length];
}


extern(C) void on_save_components(SerializeContainerCustomInput* input){
    Component[] components = *cast(Component[]*)(input.cd.runtime_data_ptr);
    input.serialize_start(input.cd.context, components.length);

    foreach(ref comp; components){
        if(comp.type == 1) {
            input.serialize_element(input.cd.context, g_name_container_index, cast(ubyte*)&comp.name);
        } else if(comp.type == 2) {
            input.serialize_element(input.cd.context, g_location_container_index, cast(ubyte*)&comp.location);
        } else if(comp.type == 3) {
            input.serialize_element(input.cd.context, g_rotation_container_index, cast(ubyte*)&comp.rotation);
        } else if(comp.type == 4) {
            input.serialize_element(input.cd.context, g_scale_container_index, cast(ubyte*)&comp.scale);
        } else if(comp.type == 5) {
            input.serialize_element(input.cd.context, g_mesh_container_index, cast(ubyte*)&comp.mesh);
        } else {
            assert(0);
            // input.serialize_element(input.cd.context, cast(ubyte*)&g_none_name, null, g_none_index); // TODO: this should be allowed?
        }
    }
}

void destroy_components(SerializerMeta* serializer_meta, Component[] components){
    foreach(ref comp; components){
        if(comp.type == 1) {
            mser_destroy_variable(serializer_meta, g_name_index, &comp.name, &free);
        } else if(comp.type == 2) {
            mser_destroy_variable(serializer_meta, g_location_index, &comp.location, &free);
        } else if(comp.type == 3) {
            mser_destroy_variable(serializer_meta, g_rotation_index, &comp.rotation, &free);
        } else if(comp.type == 4) {
            mser_destroy_variable(serializer_meta, g_scale_index, &comp.scale, &free);
        } else if(comp.type == 5) {
            mser_destroy_variable(serializer_meta, g_mesh_index, &comp.mesh, &free);
        } else {
            assert(0);
        }
    }
    free(components.ptr);
}

extern(C) void on_load_components(DeserializeContainerCustomInput* input){
    Component[]* components = cast(Component[]*)(input.cd.runtime_data_ptr);
    extendArray(components, input.length);

    foreach(comp_index, ref comp; *components){
        MetaIndex name_index;
        SerializerReturnCode status_code = input.deserialize_name(input.cd.context, &name_index);
        if(status_code != SerializerReturnCode.ok){
            destroy_components(input.cd.meta, (*components)[0..comp_index]);
            *components = null;
            return;
        }

        if(name_index == g_name_container_index){
            comp.type = 1;
            status_code = input.deserialize_element(input.cd.context, cast(void*)&comp.name);
        } else if(name_index == g_location_container_index){
            comp.type = 2;
            status_code = input.deserialize_element(input.cd.context, cast(void*)&comp.location);
        } else if(name_index == g_rotation_container_index){
            comp.type = 3;
            status_code = input.deserialize_element(input.cd.context, cast(void*)&comp.rotation);
        } else if(name_index == g_scale_container_index){
            comp.type = 4;
            status_code = input.deserialize_element(input.cd.context, cast(void*)&comp.scale);
        } else if(name_index == g_mesh_container_index){
            comp.type = 5;
            status_code = input.deserialize_element(input.cd.context, cast(void*)&comp.mesh);
        } else {
            input.deserialize_element(input.cd.context, null);
        }
        if(status_code != SerializerReturnCode.ok){
            destroy_components(input.cd.meta, (*components)[0..comp_index]);
            *components = null;
            return;
        }
    }
}

extern(C) void on_destroy_components(DestroyInput* input){
    Component[]* components = cast(Component[]*)(input.cd.runtime_data_ptr);
    destroy_components(input.cd.meta, *components);
    *components = null;
}

extern(C) void on_save_plugins(SerializeContainerCustomInput* input){
    Plugin[] plugins = *cast(Plugin[]*)(input.cd.runtime_data_ptr);
    input.serialize_start(input.cd.context, plugins.length);

    foreach(ref plugin; plugins){
        if(plugin.type == 1) {
            input.serialize_element(input.cd.context, g_TileMapPlugin_container_index, cast(ubyte*)&plugin.tile_map_plugin);
        } else if(plugin.type == 2) {
            input.serialize_element(input.cd.context, g_Plugin2_container_index, cast(ubyte*)&plugin.plugin2);
        } else {
            assert(0);
            // input.serialize_element(input.cd.context, cast(ubyte*)&g_none_name, null, g_none_index); // TODO: this should be allowed?
        }
    }
}

void destroy_plugins(SerializerMeta* serializer_meta, Plugin[] plugins){
    foreach(plugin_index, ref plugin; plugins){
        if(plugin.type == 1) {
            mser_destroy_variable(serializer_meta, g_TileMapPlugin_index, &plugin.tile_map_plugin, &free);
        } else if(plugin.type == 2) {
            mser_destroy_variable(serializer_meta, g_Plugin2_index, &plugin.plugin2, &free);
        } else {
            assert(0);
        }
    }
    free(plugins.ptr);
}

extern(C) void on_load_plugins(DeserializeContainerCustomInput* input){
    Plugin[]* plugins = cast(Plugin[]*)(input.cd.runtime_data_ptr);
    extendArray(plugins, input.length);
    foreach(plugin_index, ref plugin; *plugins){
        SerializerReturnCode status_code = SerializerReturnCode.ok;
        MetaIndex name_index;
        status_code = input.deserialize_name(input.cd.context, &name_index);

        if(status_code != SerializerReturnCode.ok){
            destroy_plugins(input.cd.meta, (*plugins)[0..plugin_index]);
            *plugins = null;
            return;
        }

        if(name_index == g_TileMapPlugin_container_index){
            plugin.type = 1;
            status_code = input.deserialize_element(input.cd.context, cast(void*)&plugin.tile_map_plugin);
        } else if(name_index == g_Plugin2_container_index){
            plugin.type = 2;
            status_code = input.deserialize_element(input.cd.context, cast(void*)&plugin.plugin2);
        } else {
            status_code = input.deserialize_element(input.cd.context, null);
        }

        if(status_code != SerializerReturnCode.ok){
            destroy_plugins(input.cd.meta, (*plugins)[0..plugin_index]);
            *plugins = null;
            return;
        }
    }
}

extern(C) void on_destroy_plugins(DestroyInput* input){
    Plugin[]* plugins = cast(Plugin[]*)(input.cd.runtime_data_ptr);
    destroy_plugins(input.cd.meta, *plugins);
    *plugins = null;
}


__gshared GameScene data;


public void loadGameScene(string path = "json/game_scene.json", bool check_data = true){
    string file = readText(environment["DATA_PATH"] ~ path);

    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta, SerializerBackend.cjson) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle);

    if(mser_has_properties(handle, SerializerProperties.canDeserialize | SerializerProperties.supportsNotPresentData) != SerializerReturnCode.ok){
        return;
    }

    // GameScene data;
    DeserializerInput input_de = DeserializerInput(&data, null, 0, cast(ubyte*)file.ptr, file.length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);
    if(check_data){
        checkData(data);
    }
}

public void unloadGameScene(){
    GameScene.destroy(data);
}


version(mser_benchmark){
GameScene generateTestData(){
    GameScene bb;
    bb.file_type = "BSCN";
    bb.version_ = "1";
    bb.description.name ="Map";
    bb.description.description ="This is map";
    bb.description.engine ="0.8.123.4728.214562937";

    CMesh mm;
    mm.m_mesh.mesh = "79dfef4db1a593bb4a46e2812a4a33f7";
    mm.m_mesh.materials = [
                            "3b81f0ced3e902b98e41ea41ec0cb0f4",
                            "3b81f0ced3e902b98e41ea41ec0cb0f4_22"
                        ];
    {
        Component cc0 = Component(1, CName("ccc"), CLocation(vec3([1, 2, 3])), CRotation(quat(1, 2, 3, 4)), CScale(vec3([1, 2, 3])), mm);
        Component cc1 = Component(2, CName("ccc"), CLocation(vec3([1, 2, 3])), CRotation(quat(1, 2, 3, 4)), CScale(vec3([1, 2, 3])), mm);
        Component cc2 = Component(3, CName("ccc"), CLocation(vec3([1, 2, 3])), CRotation(quat(1, 2, 3, 4)), CScale(vec3([1, 2, 3])), mm);
        Component cc3 = Component(4, CName("ccc"), CLocation(vec3([1, 2, 3])), CRotation(quat(1, 2, 3, 4)), CScale(vec3([1, 2, 3])), mm);
        Component cc4 = Component(5, CName("ccc"), CLocation(vec3([1, 2, 3])), CRotation(quat(1, 2, 3, 4)), CScale(vec3([1, 2, 3])), mm);

        Entity ee;
        ee.components ~= cc0;
        ee.components ~= cc1;
        ee.components ~= cc2;
        ee.components ~= cc3;
        ee.components ~= cc4;

        bb.entities ~= ee;
        bb.entities ~= bb.entities; // 2
        bb.entities ~= bb.entities; // 4
        bb.entities ~= bb.entities; // 8
        bb.entities ~= bb.entities; // 16
        bb.entities ~= bb.entities; // 32
        bb.entities ~= bb.entities; // 64
        bb.entities ~= bb.entities; // 128
        bb.entities ~= bb.entities; // 256
        bb.entities ~= bb.entities; // 512
        bb.entities ~= bb.entities; // 1024
        bb.entities ~= bb.entities; // 2048
    }

    {
        TileMapPlugin tt;
        tt.tile_sets = [
                [
                    "Data/Models/TileSets/Snow.3db",
                    "Data/Materials/TileSets/Snow.bmt"
                ],
                [
                    "Data/Models/TileSets/Ground.3db",
                    "Data/Materials/TileSets/Ground.bmt"
                ],
                [
                    "Data/Models/TileSets/Ice.3db",
                    "Data/Materials/TileSets/Ice.bmt"
                ]
        ];
        tt.tile_map = [
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
            0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2
        ];
        Plugin2 pp22 = Plugin2("Data");
        bb.plugins ~= Plugin(1, pp22, tt);
        bb.plugins ~= Plugin(2, pp22, tt);

        bb.plugins ~= bb.plugins; // 4
        bb.plugins ~= bb.plugins; // 8
        bb.plugins ~= bb.plugins; // 16
        bb.plugins ~= bb.plugins; // 32
        bb.plugins ~= bb.plugins; // 64
        bb.plugins ~= bb.plugins; // 128
        bb.plugins ~= bb.plugins; // 256
        bb.plugins ~= bb.plugins; // 512
        bb.plugins ~= bb.plugins; // 1024
        bb.plugins ~= bb.plugins; // 2048
    }

    return bb;
}

public void benchmarkSerializeGameScene(SerializerBackend impl, ref BenchmarkData[2] bench){

    enum GameScene bb_enum = generateTestData();
    __gshared GameScene bb = bb_enum;
    // writeln(bb.entities.length);
    // writeln(bb.plugins.length);


    auto sw = StopWatch(AutoStart.yes);

    SerializerHandle handle;
    create_serializer(&handle, &g_meta, impl);
    scope(exit)mser_destroy(&handle);

    bench[0].serialize = true;
    bench[1].serialize = false;

    bench[0].create_time_ns = sw.peek.total!"nsecs";
    bench[1].create_time_ns = bench[0].create_time_ns;

    SerializerOutput output_last;
    scope(exit)free(output_last.out_buffer);

    foreach(i; 0..bench[0].iterationNum){
        SerializerOutput output;
        SerializerInput input = SerializerInput(&bb);
        mser_serialize(handle, &input, &output);
        bench[0].size = output.out_buffer_length;
        bench[1].size = bench[0].size;
        if(i == bench[0].iterationNum - 1){
            output_last = output;
        }else {
            free(output.out_buffer);
        }
    }
    long mid = sw.peek.total!"nsecs";
    bench[0].process_time_total_ns = mid - bench[0].create_time_ns;

    foreach(i; 0..bench[1].iterationNum){
        GameScene test;
        DeserializerInput input_de = DeserializerInput(&test, null, 0, output_last.out_buffer, output_last.out_buffer_length);
        DeserializerOutput output_de;
        mser_deserialize(handle, &input_de, &output_de);
        // writeln(test);
        GameScene.destroy(test);
    }

    bench[1].process_time_total_ns = sw.peek.total!"nsecs" - mid;
}
}


public void testGameScene(SerializerBackend impl){
    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle);

    if(mser_has_properties(handle, SerializerProperties.canSerialize) != SerializerReturnCode.ok){
        return;
    }

    SerializerInput input = SerializerInput(&data);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);
    // if(mser_has_properties(handle, SerializerProperties.isBinary) == SerializerReturnCode.propertiesNotSupported){
    //     printf("%s\n", cast(char*)output.out_buffer);
    // }
    import std.file : write;
    if(impl == SerializerBackend.binary){
        write("game_scene.bin", output.out_buffer[0..output.out_buffer_length]);
    }
    if(impl == SerializerBackend.binary_compatible){
        write("game_scene.bincp", output.out_buffer[0..output.out_buffer_length]);
    }
    if(impl == SerializerBackend.cjson){
        write("game_scene.json", output.out_buffer[0..output.out_buffer_length]);
    }
    if(impl == SerializerBackend.libxml2){
        write("game_scene.xml", output.out_buffer[0..output.out_buffer_length]);
    }
    if(impl == SerializerBackend.json){
        write("game_scene.2.json", output.out_buffer[0..output.out_buffer_length]);
    }
    if(impl == SerializerBackend.toml){
        write("game_scene.toml", output.out_buffer[0..output.out_buffer_length]);
    }


    if(mser_has_properties(handle,SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }
    GameScene tt22;
    DeserializerInput input_de = DeserializerInput(&tt22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);
    scope(exit)GameScene.destroy(tt22);
    checkData(tt22);
}

public void testGameSceneInvalid(SerializerBackend impl){
    if(impl != SerializerBackend.binary_compatible){
        return;
    }
    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle);

    if(mser_has_properties(handle, SerializerProperties.canSerialize) != SerializerReturnCode.ok){
        return;
    }

    SerializerInput input = SerializerInput(&data);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);

    if(mser_has_properties(handle,SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }

    foreach(i; 0..(output.out_buffer_length - 1)) {
        ubyte* mem = cast(ubyte*)malloc(i); // For more probable crash
        memcpy(mem, output.out_buffer, i);
        GameScene loaded_data;
        DeserializerInput input_de = DeserializerInput(&loaded_data, null, 0, mem, i);
        DeserializerOutput output_de;
        assert(mser_deserialize(handle, &input_de, &output_de) != SerializerReturnCode.ok);
        free(mem);
    }
}


public void checkData(ref GameScene data){
    assert(data.file_type == "BSCN");
    assert(data.version_ == "1");
    assert(data.description.name == "Map");
    assert(data.description.description == "This is map");
    assert(data.description.engine == "0.8.123.4728.214562937");

    assert(data.entities.length == 1);
    assert(data.entities[0].components.length == 5);
    {
        auto c = data.entities[0].components[0].name;
        assert(data.entities[0].components[0].type == 1);
        assert(c.value == "This is name");

    }
    {
        auto c = data.entities[0].components[1].location;
        assert(data.entities[0].components[1].type == 2);
        assert(flCmp(c.value.data[0], 6));
        assert(flCmp(c.value.data[1], 5));
        assert(flCmp(c.value.data[2], 0.146203));

    }
    {
        auto c = data.entities[0].components[2].rotation;
        assert(data.entities[0].components[2].type == 3);
        assert(flCmp(c.value.x, 0));
        assert(flCmp(c.value.y, 0));
        assert(flCmp(c.value.z, 0));
        assert(flCmp(c.value.w, 1));

    }
    {
        auto c = data.entities[0].components[3].scale;
        assert(data.entities[0].components[3].type == 4);
        assert(flCmp(c.value.data[0], 1));
        assert(flCmp(c.value.data[1], 1));
        assert(flCmp(c.value.data[2], 1));

    }
    {
        auto c = data.entities[0].components[4].mesh;
        assert(data.entities[0].components[4].type == 5);
        assert(c.m_mesh.mesh == "79dfef4db1a593bb4a46e2812a4a33f7");
        assert(c.m_mesh.materials.length == 2);
        assert(c.m_mesh.materials[0] == "3b81f0ced3e902b98e41ea41ec0cb0f4");
        assert(c.m_mesh.materials[1] == "3b81f0ced3e902b98e41ea41ec0cb0f4_22");

    }
    assert(data.plugins.length == 2);
    assert(data.plugins[0].tile_map_plugin.tile_sets.length == 3);
    assert(data.plugins[0].tile_map_plugin.tile_sets[0][0] == "Data/Models/TileSets/Snow.3db");
    assert(data.plugins[0].tile_map_plugin.tile_sets[0][1] == "Data/Materials/TileSets/Snow.bmt");
    assert(data.plugins[0].tile_map_plugin.tile_sets[1][0] == "Data/Models/TileSets/Ground.3db");
    assert(data.plugins[0].tile_map_plugin.tile_sets[1][1] == "Data/Materials/TileSets/Ground.bmt");
    assert(data.plugins[0].tile_map_plugin.tile_sets[2][0] == "Data/Models/TileSets/Ice.3db");
    assert(data.plugins[0].tile_map_plugin.tile_sets[2][1] == "Data/Materials/TileSets/Ice.bmt");
    assert(data.plugins[0].tile_map_plugin.tile_map == [
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
        0,0,1,0,2,3,2,4,2,30,0,4,3,2,2,3,4,3,3,2,3,103,2,0,13,0,0,3,3,2,1,3,4,2,4,5,4,3,67,5,4,6,2,4,2,
    ]);
    assert(data.plugins[1].plugin2.data == "Data");
}

