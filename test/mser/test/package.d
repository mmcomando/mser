module mser.test;

import mser;
import mser.test.allocator;
import mser.test.array;
import mser.test.basic;
import mser.test.callbacks;
import mser.test.compatible;
import mser.test.custom;
import mser.test.game_scene;
import mser.test.map;
import mser.test.missing;
import mser.test.pointer;
import mser.test.print_enum;
import mser.test.serialize_deserialize;
import mser.test.serialize;
import mser.test.string;
import mser.test.structs;


SerializerBackend[] all_serializers = [
    SerializerBackend.binary,
    SerializerBackend.cjson,
    SerializerBackend.json,
    SerializerBackend.libxml2,
    SerializerBackend.binary_compatible,
    SerializerBackend.toml,
];

unittest {
    foreach(imp; all_serializers) testCreate(imp);
}

unittest {
    foreach(imp; all_serializers) testSerialize(imp);
}

unittest {
    foreach(imp; all_serializers) testSerializeDeserialize(imp);
}

unittest {
    foreach(imp; all_serializers) testMissing(imp);
}


unittest {
    foreach(imp; all_serializers) testContainerCustom(imp);
}
unittest {
    foreach(imp; all_serializers) testCustomArray(imp);
}
unittest {
    foreach(imp; all_serializers) testCustomString(imp);
}
unittest {
    foreach(imp; all_serializers) testCompatible(imp);
    foreach(imp; all_serializers) testCompatibleArray(imp);
    foreach(imp; all_serializers) testCompatibleNumbers(imp);
    foreach(imp; all_serializers) testCompatibleCustomContainer(imp);
}
unittest {
    foreach(imp; all_serializers) testOnLoadFinish(imp);
}
unittest {
    foreach(imp; all_serializers) testCustom(imp);
}
unittest {
    foreach(imp; all_serializers) testPointer(imp);
}
unittest {
    foreach(imp; all_serializers) testBinary(imp);
}
unittest {
    foreach(imp; all_serializers) testCallbacks(imp);
}
unittest {
    test_print_enum();
}
unittest {
    foreach(imp; all_serializers) testAllocator(imp);
}

unittest {
    loadGameScene();
    foreach(imp; all_serializers) testGameSceneInvalid(imp);
    foreach(imp; all_serializers) testGameScene(imp);
    unloadGameScene();
}

void main(){
}