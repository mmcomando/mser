module mser.test.compatible;

import core.stdc.stdio;
import core.stdc.stdlib : free, malloc;
import std.stdio;

import mser;
import mser.test.structs;

bool serializer_supports_compatible(SerializerBackend impl){
    switch(impl) {
    case SerializerBackend.cjson:
    case SerializerBackend.binary_compatible:
        return true;
    default: break;
    }
    return false;
}

void print_output()(SerializerHandle handle, SerializerOutput output){
    if(mser_has_properties(handle, SerializerProperties.isBinary) == SerializerReturnCode.propertiesNotSupported){
        printf("%s\n", cast(char*)output.out_buffer);
    } else {
        writeln(output.out_buffer[0..output.out_buffer_length]);
    }
}

/////// Structs version AA

struct TrivialCopy_AA {
    uint v_uint;
    int v_int;
}

struct NonTrivial_AA {
    short v_short;
    int v_int;
    ubyte v_ubyte;
}

struct WithArray_AA {
    ubyte v_ubyte;
    NonTrivial_AA not_trivial;
    TrivialCopy_AA[] arr_a;
    NonTrivial_AA[] arr_b;
    int v_int;
}

struct Test_AA {
    int v_int;
    TrivialCopy_AA trivial_a;
    WithArray_AA[] arr_with_array;
    TrivialCopy_AA trivial_b;
    // NonTrivial_AA not_trivial;
    const(char)[] str;
    char[2] fixed_arr;
}

enum SerializerMeta g_meta_test_AA_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "Test", SerializerElementType.container, Test_AA.sizeof, Test_AA.alignof, [
            SerializerSubobject("v_int", "i32", Test_AA.v_int.offsetof),
            SerializerSubobject("trivial_a", "TrivialCopy", Test_AA.trivial_a.offsetof),
            SerializerSubobject("arr_with_array", "ARRAY_WithArray", Test_AA.arr_with_array.offsetof),
            SerializerSubobject("trivial_b", "TrivialCopy", Test_AA.trivial_b.offsetof),
            // SerializerSubobject("not_trivial", "TrivialCopy", Test_AA.not_trivial.offsetof),
            SerializerSubobject("str", "STRING", Test_AA.str.offsetof),
            SerializerSubobject("fixed_arr", "ARRAY_fixed", Test_AA.fixed_arr.offsetof),
        ]),
        SerializerMetaElement(0, "WithArray", SerializerElementType.container, WithArray_AA.sizeof, WithArray_AA.alignof, [
            SerializerSubobject("v_ubyte", "u8", WithArray_AA.v_ubyte.offsetof),
            SerializerSubobject("not_trivial", "NonTrivial", WithArray_AA.not_trivial.offsetof),
            SerializerSubobject("arr_a", "ARRAY_TrivialCopy", WithArray_AA.arr_a.offsetof),
            SerializerSubobject("arr_b", "ARRAY_NonTrivial", WithArray_AA.arr_b.offsetof),
            SerializerSubobject("v_int", "i32", WithArray_AA.v_int.offsetof),
        ]),
        SerializerMetaElement(0, "NonTrivial", SerializerElementType.container, NonTrivial_AA.sizeof, NonTrivial_AA.alignof, [
            SerializerSubobject("v_short", "i16", NonTrivial_AA.v_short.offsetof),
            SerializerSubobject("v_int", "i32", NonTrivial_AA.v_int.offsetof),
            SerializerSubobject("v_ubyte", "u8", NonTrivial_AA.v_ubyte.offsetof),
        ]),
        SerializerMetaElement(0, "TrivialCopy", SerializerElementType.container, TrivialCopy_AA.sizeof, TrivialCopy_AA.alignof, [
            SerializerSubobject("v_uint", "u32", TrivialCopy_AA.v_uint.offsetof),
            SerializerSubobject("v_int", "i32", TrivialCopy_AA.v_int.offsetof),
        ]),

        SerializerMetaElement(0, "ARRAY_fixed", SerializerElementType.arrayFixed, 2, 1, [SerializerSubobject("", "u8", 0, 0)]),
        SerializerMetaElement(0, "ARRAY_TrivialCopy", SerializerElementType.array, 16, 8, [SerializerSubobject("", "TrivialCopy", 8u, 0),SerializerSubobject("", "", 0u, 1)]),
        SerializerMetaElement(0, "ARRAY_NonTrivial", SerializerElementType.array, 16, 8, [SerializerSubobject("", "NonTrivial", 8u, 0),SerializerSubobject("", "", 0u, 1)]),
        SerializerMetaElement(0, "ARRAY_WithArray", SerializerElementType.array, 16, 8, [SerializerSubobject("", "WithArray", 8u, 0),SerializerSubobject("", "", 0u, 1)]),

        SerializerMetaElement(0, "STRING", SerializerElementType.string, 16, 8, [SerializerSubobject(null, null, size_t.sizeof, -1), SerializerSubobject(null, "STRING_LENGTH", 0, -1)]),
        SerializerMetaElement(0, "STRING_LENGTH", SerializerElementType.unsignedInteger, size_t.sizeof, size_t.alignof, null),

        SerializerMetaElement(0, "u8", SerializerElementType.unsignedInteger, ubyte.sizeof, ubyte.alignof, null),
        SerializerMetaElement(0, "i16", SerializerElementType.unsignedInteger, short.sizeof, short.alignof, null),
        SerializerMetaElement(0, "u32", SerializerElementType.unsignedInteger, uint.sizeof, uint.alignof, null),
        SerializerMetaElement(0, "i32", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
    ],
);

__gshared SerializerMeta g_meta_test_AA = g_meta_test_AA_enum;

/////// Structs version BB

struct TrivialCopy_BB {
    int v_int;
    // uint v_uint;
}

struct NonTrivial_BB {
    int v_int;
    short v_short;
    // ubyte v_ubyte;
}

struct WithArray_BB {
    ubyte v_ubyte;
    NonTrivial_BB not_trivial;
    TrivialCopy_BB[] arr_a;
    // NonTrivial_BB[] arr_b;
    // int v_int;
}

struct Test_BB {
    int v_int;
    // TrivialCopy_BB trivial_a;
    WithArray_BB[] arr_with_array;
    const(char)[] str;
    TrivialCopy_BB trivial_b;
    NonTrivial_BB not_trivial;
    char[1] fixed_arr;
    ubyte check;
}



enum SerializerMeta g_meta_test_BB_enum = SerializerMeta(
 [
        SerializerMetaElement(0, "Test", SerializerElementType.container, Test_BB.sizeof, Test_BB.alignof, [
            SerializerSubobject("v_int", "i32", Test_BB.v_int.offsetof),
            // SerializerSubobject("trivial_a", "TrivialCopy", Test_BB.trivial_a.offsetof),
            SerializerSubobject("arr_with_array", "ARRAY_WithArray", Test_BB.arr_with_array.offsetof),
            SerializerSubobject("trivial_b", "TrivialCopy", Test_BB.trivial_b.offsetof),
            SerializerSubobject("not_trivial", "TrivialCopy", Test_BB.not_trivial.offsetof),
            SerializerSubobject("str", "STRING", Test_BB.str.offsetof),
            SerializerSubobject("fixed_arr", "ARRAY_fixed", Test_BB.fixed_arr.offsetof),
        ]),
        SerializerMetaElement(0, "WithArray", SerializerElementType.container, WithArray_BB.sizeof, WithArray_BB.alignof, [
            SerializerSubobject("v_ubyte", "u8", WithArray_BB.v_ubyte.offsetof),
            SerializerSubobject("not_trivial", "NonTrivial", WithArray_BB.not_trivial.offsetof),
            SerializerSubobject("arr_a", "ARRAY_TrivialCopy", WithArray_BB.arr_a.offsetof),
            // SerializerSubobject("arr_b", "ARRAY_NonTrivial", WithArray_BB.arr_b.offsetof),
            // SerializerSubobject("v_int", "i32", WithArray_BB.v_int.offsetof),
        ]),
        SerializerMetaElement(0, "NonTrivial", SerializerElementType.container, NonTrivial_BB.sizeof, NonTrivial_BB.alignof, [
            SerializerSubobject("v_short", "i16", NonTrivial_BB.v_short.offsetof),
            SerializerSubobject("v_int", "i32", NonTrivial_BB.v_int.offsetof),
            // SerializerSubobject("v_ubyte", "u8", NonTrivial_BB.v_ubyte.offsetof),
        ]),
        SerializerMetaElement(0, "TrivialCopy", SerializerElementType.container, TrivialCopy_BB.sizeof, TrivialCopy_BB.alignof, [
            // SerializerSubobject("v_uint", "u32", TrivialCopy_BB.v_uint.offsetof),
            SerializerSubobject("v_int", "i32", TrivialCopy_BB.v_int.offsetof),
        ]),

        SerializerMetaElement(0, "ARRAY_fixed", SerializerElementType.arrayFixed, 1, 1, [SerializerSubobject("", "u8", 0, 0)]),
        SerializerMetaElement(0, "ARRAY_TrivialCopy", SerializerElementType.array, 16, 8, [SerializerSubobject("", "TrivialCopy", 8u, 0),SerializerSubobject("", "", 0u, 1)]),
        SerializerMetaElement(0, "ARRAY_NonTrivial", SerializerElementType.array, 16, 8, [SerializerSubobject("", "NonTrivial", 8u, 0),SerializerSubobject("", "", 0u, 1)]),
        SerializerMetaElement(0, "ARRAY_WithArray", SerializerElementType.array, 16, 8, [SerializerSubobject("", "WithArray", 8u, 0),SerializerSubobject("", "", 0u, 1)]),

        SerializerMetaElement(0, "STRING", SerializerElementType.string, 16, 8, [SerializerSubobject(null, null, size_t.sizeof, -1), SerializerSubobject(null, "STRING_LENGTH", 0, -1)]),
        SerializerMetaElement(0, "STRING_LENGTH", SerializerElementType.unsignedInteger, size_t.sizeof, size_t.alignof, null),

        SerializerMetaElement(0, "u8", SerializerElementType.unsignedInteger, ubyte.sizeof, ubyte.alignof, null),
        SerializerMetaElement(0, "i16", SerializerElementType.unsignedInteger, short.sizeof, short.alignof, null),
        SerializerMetaElement(0, "u32", SerializerElementType.unsignedInteger, uint.sizeof, uint.alignof, null),
        SerializerMetaElement(0, "i32", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
    ],
);

__gshared SerializerMeta g_meta_test_BB = g_meta_test_BB_enum;
shared static this()
{
    assert(mser_meta_compute_indices(&g_meta_test_AA) == SerializerReturnCode.ok);
    assert(mser_meta_compute_indices(&g_meta_test_BB) == SerializerReturnCode.ok);
}


void testCompatible(SerializerBackend impl){
    if(serializer_supports_compatible(impl) == false){
        return;
    }
    SerializerHandle handle_AA;
    assert(create_serializer(&handle_AA, &g_meta_test_AA, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle_AA);


    Test_AA test_aa;
    {
        WithArray_AA with_array;
        with_array.v_ubyte = 4;
        with_array.not_trivial = NonTrivial_AA(5, 6, 7);
        with_array.arr_a = [TrivialCopy_AA(8, 9), TrivialCopy_AA(10, 11)];
        with_array.arr_b = [NonTrivial_AA(12, 13, 14)];
        with_array.v_int = 15;

        test_aa.v_int = 1;
        test_aa.trivial_a = TrivialCopy_AA(2, 3);
        test_aa.arr_with_array = [with_array];
        // foreach(i;0..1000) test_aa.arr_with_array ~= with_array;
        test_aa.trivial_b = TrivialCopy_AA(16, 17);
        test_aa.str = "AAA";
        test_aa.fixed_arr = [1, 2];
    }

    SerializerInput input = SerializerInput(&test_aa);
    SerializerOutput output;
    assert(mser_serialize(handle_AA, &input, &output) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);
    // print_output(handle_AA, output);


    SerializerHandle handle_BB;
    assert(create_serializer(&handle_BB, &g_meta_test_BB, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle_BB);

    Test_BB test_bb;
    test_bb.not_trivial = NonTrivial_BB(18, 19);
    DeserializerInput input_de = DeserializerInput(&test_bb, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;

    assert(mser_deserialize(handle_BB, &input_de, &output_de) == SerializerReturnCode.ok);
    scope(exit){
        foreach(ref arr; test_bb.arr_with_array){
            free(arr.arr_a.ptr);
        }
        free(test_bb.arr_with_array.ptr);
        free(cast(char*)test_bb.str.ptr);
    }

    assert(test_bb.v_int == 1);
    assert(test_bb.trivial_b == TrivialCopy_BB(17));
    assert(test_bb.arr_with_array.length == 1);
    assert(test_bb.arr_with_array[0].v_ubyte == 4);
    assert(test_bb.arr_with_array[0].not_trivial == NonTrivial_BB(6, 5));
    assert(test_bb.arr_with_array[0].arr_a.length == 2);
    assert(test_bb.arr_with_array[0].arr_a[0] == TrivialCopy_BB(9));
    assert(test_bb.arr_with_array[0].arr_a[1] == TrivialCopy_BB(11));
    assert(test_bb.arr_with_array[0].v_ubyte == 4);
    assert(test_bb.not_trivial == NonTrivial_BB(0, 0));
    assert(test_bb.str == "AAA");
    assert(test_bb.fixed_arr == [1]);
    assert(test_bb.check == 0);
    // assert(test_bb.not_trivial == NonTrivial_BB(18, 19)); // TODO: Fields here are set to 0, it should be like it (probably yes, as we want to initialize fields)?
}



struct CustomArray {
    ubyte length;
    ubyte val_1;
    ubyte val_2;
    ubyte val_3;
}

struct ArrayCompatibility_A {
    ubyte[2] arr_1; // Fixed array to smaller fixed
    ubyte[2] arr_2; // Fixed array to larger fixed
    ubyte[] arr_3; // Array  to smaller fixed
    ubyte[] arr_4; // Array to larger fixed
    ubyte[] arr_5; // Array to custom array
    CustomArray arr_6; // Custom array to array
    CustomArray arr_7; // Custom array to smaller fixed
    CustomArray arr_8; // Custom array to larger fixed
}

struct ArrayCompatibility_B {
    ubyte[1] arr_1; // Fixed array to smaller fixed
    ubyte[3] arr_2; // Fixed array to larger fixed
    ubyte[1] arr_3; // Array  to smaller fixed
    ubyte[3] arr_4; // Array to larger fixed
    CustomArray arr_5; // Array to custom array
    ubyte[] arr_6; // Custom array to array
    ubyte[1] arr_7; // Custom array to smaller fixed
    ubyte[3] arr_8; // Custom array to larger fixed
}

enum SerializerMeta g_meta_array_compatibility_AA_enum = SerializerMeta(
 [
        SerializerMetaElement(0, "ArrayCompatibility", SerializerElementType.container, ArrayCompatibility_A.sizeof, ArrayCompatibility_A.alignof, [
            SerializerSubobject("arr_1", "u8x2", ArrayCompatibility_A.arr_1.offsetof),
            SerializerSubobject("arr_2", "u8x2", ArrayCompatibility_A.arr_2.offsetof),
            SerializerSubobject("arr_3", "u8[]", ArrayCompatibility_A.arr_3.offsetof),
            SerializerSubobject("arr_4", "u8[]", ArrayCompatibility_A.arr_4.offsetof),
            SerializerSubobject("arr_5", "u8[]", ArrayCompatibility_A.arr_5.offsetof),
            SerializerSubobject("arr_6", "u8[custom]", ArrayCompatibility_A.arr_6.offsetof),
            SerializerSubobject("arr_7", "u8[custom]", ArrayCompatibility_A.arr_7.offsetof),
            SerializerSubobject("arr_8", "u8[custom]", ArrayCompatibility_A.arr_8.offsetof),
        ]),

        SerializerMetaElement(0, "u8[]", SerializerElementType.array, 16, 8, [SerializerSubobject("", "u8", 8u, -1), SerializerSubobject("", "", 0u, -1)]),
        SerializerMetaElement(0, "u8[custom]", SerializerElementType.arrayCustom, 0, 0, [SerializerSubobject("", "u8", 8u, -1)]),

        SerializerMetaElement(0, "u8x2", SerializerElementType.arrayFixed, 2, 1, [SerializerSubobject("", "u8", 0, -1)]),
        SerializerMetaElement(0, "u8", SerializerElementType.unsignedInteger, ubyte.sizeof, ubyte.alignof, null),
    ],
);

enum SerializerMeta g_meta_array_compatibility_BB_enum = SerializerMeta(
 [
        SerializerMetaElement(0, "ArrayCompatibility", SerializerElementType.container, ArrayCompatibility_B.sizeof, ArrayCompatibility_B.alignof, [
            SerializerSubobject("arr_1", "u8x1", ArrayCompatibility_B.arr_1.offsetof),
            SerializerSubobject("arr_2", "u8x3", ArrayCompatibility_B.arr_2.offsetof),
            SerializerSubobject("arr_3", "u8x1", ArrayCompatibility_B.arr_3.offsetof),
            SerializerSubobject("arr_4", "u8x3", ArrayCompatibility_B.arr_4.offsetof),
            SerializerSubobject("arr_5", "u8[custom]", ArrayCompatibility_B.arr_5.offsetof),
            SerializerSubobject("arr_6", "u8[]", ArrayCompatibility_B.arr_6.offsetof),
            SerializerSubobject("arr_7", "u8x1", ArrayCompatibility_B.arr_7.offsetof),
            SerializerSubobject("arr_8", "u8x3", ArrayCompatibility_B.arr_8.offsetof),
        ]),

        SerializerMetaElement(0, "u8[]", SerializerElementType.array, 16, 8, [SerializerSubobject("", "u8", 8u, -1), SerializerSubobject("", "", 0u, -1)]),
        SerializerMetaElement(0, "u8[custom]", SerializerElementType.arrayCustom, 0, 0, [SerializerSubobject("", "u8", 8u, -1)]),

        SerializerMetaElement(0, "u8x1", SerializerElementType.arrayFixed, 1, 1, [SerializerSubobject("", "u8", 0, -1)]),
        SerializerMetaElement(0, "u8x3", SerializerElementType.arrayFixed, 3, 1, [SerializerSubobject("", "u8", 0, -1)]),
        SerializerMetaElement(0, "u8", SerializerElementType.unsignedInteger, ubyte.sizeof, ubyte.alignof, null),
    ],
);


extern(C) void on_save_array_test_compatibility(SerializeArrayInput* input){
    CustomArray* arr = cast(CustomArray*)(input.cd.runtime_data_ptr);
    input.serialize_start(input.cd.context, arr.length);

    if(arr.length >= 1)
        assert(input.serialize_element(input.cd.context, cast(void*)&arr.val_1) == SerializerReturnCode.ok);
    if(arr.length >= 2)
        assert(input.serialize_element(input.cd.context, cast(void*)&arr.val_2) == SerializerReturnCode.ok);
    if(arr.length >= 3)
        assert(input.serialize_element(input.cd.context, cast(void*)&arr.val_3) == SerializerReturnCode.ok);
}

extern(C) void on_load_array_test_compatibility(DeserializeArrayInput* input){
    CustomArray* arr = cast(CustomArray*)(input.cd.runtime_data_ptr);
    arr.length = cast(ubyte)input.length;
    if(input.length >= 1 )
        assert(input.deserialize_element(input.cd.context, cast(void*)&arr.val_1) == SerializerReturnCode.ok);
    if(input.length >= 2 )
        assert(input.deserialize_element(input.cd.context, cast(void*)&arr.val_2) == SerializerReturnCode.ok);
    if(input.length >= 3 )
        assert(input.deserialize_element(input.cd.context, cast(void*)&arr.val_3) == SerializerReturnCode.ok);
}


__gshared SerializerMeta g_meta_array_compatibility_AA = g_meta_array_compatibility_AA_enum;
__gshared SerializerMeta g_meta_array_compatibility_BB = g_meta_array_compatibility_BB_enum;

shared static this()
{
    assert(mser_meta_compute_indices(&g_meta_array_compatibility_AA) == SerializerReturnCode.ok);
    assert(mser_meta_compute_indices(&g_meta_array_compatibility_BB) == SerializerReturnCode.ok);

    MetaIndex cai_AA;
    MetaIndex cai_BB;
    assert(mser_meta_get_index(&g_meta_array_compatibility_AA, "u8[custom]", "u8[custom]".length, &cai_AA) == SerializerReturnCode.ok);
    assert(mser_meta_get_index(&g_meta_array_compatibility_BB, "u8[custom]", "u8[custom]".length, &cai_BB) == SerializerReturnCode.ok);

    g_meta_array_compatibility_AA.meta_elements[cai_AA].on_save_array = &on_save_array_test_compatibility;
    g_meta_array_compatibility_AA.meta_elements[cai_AA].on_load_array = &on_load_array_test_compatibility;
    g_meta_array_compatibility_BB.meta_elements[cai_BB].on_save_array = &on_save_array_test_compatibility;
    g_meta_array_compatibility_BB.meta_elements[cai_BB].on_load_array = &on_load_array_test_compatibility;
}

void testCompatibleArray(SerializerBackend impl){
    if(serializer_supports_compatible(impl) == false){
        return;
    }
    SerializerHandle handle_AA;
    assert(create_serializer(&handle_AA, &g_meta_array_compatibility_AA, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle_AA);


    ArrayCompatibility_A test_aa;
    {
        test_aa.arr_1 = [1, 2];
        test_aa.arr_2 = [1, 2];
        test_aa.arr_3 = [1, 2];
        test_aa.arr_4 = [1, 2];
        test_aa.arr_5 = [1, 2];
        test_aa.arr_6 = CustomArray(2, 1, 2, 100);
        test_aa.arr_7 = CustomArray(2, 1, 2, 100);
        test_aa.arr_8 = CustomArray(2, 1, 2, 100);
    }

    SerializerInput input = SerializerInput(&test_aa);
    SerializerOutput output;
    assert(mser_serialize(handle_AA, &input, &output) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);
    // print_output(handle_AA, output);


    SerializerHandle handle_BB;
    assert(create_serializer(&handle_BB, &g_meta_array_compatibility_BB, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle_BB);

    ArrayCompatibility_B test_bb;
    {
        test_bb.arr_1 = [200];
        test_bb.arr_2 = [200, 200, 200];
        test_bb.arr_3 = [200];
        test_bb.arr_4 = [200, 200, 200];
        test_bb.arr_5 = CustomArray(2, 200, 200, 200);
        test_bb.arr_6 = null;
        test_bb.arr_7 = [200];
        test_bb.arr_8 = [200, 200, 200];
    }
    DeserializerInput input_de = DeserializerInput(&test_bb, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;

    assert(mser_deserialize(handle_BB, &input_de, &output_de) == SerializerReturnCode.ok);
    scope(exit){
        mser_destroy_variable(&g_meta_array_compatibility_BB, 0, &test_bb, &free);
    }

    assert(test_bb.arr_1 == [1]);
    assert(test_bb.arr_2 == [1, 2, 0]);
    assert(test_bb.arr_3 == [1]);
    assert(test_bb.arr_4 == [1, 2, 0]);
    // assert(test_bb.arr_5 == CustomArray(2, 1, 2, 200));
    assert(test_bb.arr_5 == CustomArray(2, 1, 2, 0)); // 0 is due to memset of whole test_bb structure
    assert(test_bb.arr_6.length == 2);
    assert(test_bb.arr_6 == [1, 2]);
    assert(test_bb.arr_7 == [1]);
    assert(test_bb.arr_8 == [1, 2, 0]);
}






struct NumbersCompatibility_A {
    short n_1; // To unsigned, overflow
    ushort n_2; // To signed, overflow
    short n_3; // To smaller
    int n_4; // To bigger
    double n_5; // To smaller
    float n_6; // To bigger
    float n_7; // To int
    float n_8; // To int, overflow positive
    float n_9; // To int, overflow negative

    short[] arr_1; // To arr with bytes
    short[] arr_2; // To arr with ints
}

struct NumbersCompatibility_B {
    ushort n_1; // To unsigned, overflow
    short n_2; // To signed, overflow
    byte n_3; // To smaller
    long n_4; // To bigger
    float n_5; // To smaller
    double n_6; // To bigger
    int n_7; // To int
    byte n_8; // To int, overflow positive
    byte n_9; // To int, overflow negative

    byte[] arr_1; // To arr with bytes
    int[] arr_2; // To arr with ints
}

enum SerializerMeta g_meta_number_compatibility_AA_enum = SerializerMeta(
 [
        SerializerMetaElement(0, "NumbersCompatibility", SerializerElementType.container, NumbersCompatibility_A.sizeof, NumbersCompatibility_A.alignof, [
            SerializerSubobject("n_1", "i16", NumbersCompatibility_A.n_1.offsetof),
            SerializerSubobject("n_2", "u16", NumbersCompatibility_A.n_2.offsetof),
            SerializerSubobject("n_3", "i16", NumbersCompatibility_A.n_3.offsetof),
            SerializerSubobject("n_4", "i32", NumbersCompatibility_A.n_4.offsetof),
            SerializerSubobject("n_5", "f64", NumbersCompatibility_A.n_5.offsetof),
            SerializerSubobject("n_6", "f32", NumbersCompatibility_A.n_6.offsetof),
            SerializerSubobject("n_7", "f32", NumbersCompatibility_A.n_7.offsetof),
            SerializerSubobject("n_8", "f32", NumbersCompatibility_A.n_8.offsetof),
            SerializerSubobject("n_9", "f32", NumbersCompatibility_A.n_9.offsetof),
            SerializerSubobject("arr_1", "i16[]", NumbersCompatibility_A.arr_1.offsetof),
            SerializerSubobject("arr_2", "i16[]", NumbersCompatibility_A.arr_2.offsetof),
        ]),

        SerializerMetaElement(0, "i8[]", SerializerElementType.array, 16, 8, [SerializerSubobject("", "i16", 8u, -1), SerializerSubobject("", "", 0u, -1)]),
        SerializerMetaElement(0, "i16[]", SerializerElementType.array, 16, 8, [SerializerSubobject("", "i16", 8u, -1), SerializerSubobject("", "", 0u, -1)]),
        SerializerMetaElement(0, "i32[]", SerializerElementType.array, 16, 8, [SerializerSubobject("", "i16", 8u, -1), SerializerSubobject("", "", 0u, -1)]),

        SerializerMetaElement(0, "i16", SerializerElementType.signedInteger, short.sizeof, short.alignof, null),
        SerializerMetaElement(0, "u16", SerializerElementType.unsignedInteger, ushort.sizeof, ushort.alignof, null),
        SerializerMetaElement(0, "i32", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
        SerializerMetaElement(0, "f64", SerializerElementType.floatingPointNumber, double.sizeof, double.alignof, null),
        SerializerMetaElement(0, "f32", SerializerElementType.floatingPointNumber, float.sizeof, float.alignof, null),
        SerializerMetaElement(0, "i8", SerializerElementType.signedInteger, byte.sizeof, byte.alignof, null),
        SerializerMetaElement(0, "i64", SerializerElementType.signedInteger, long.sizeof, long.alignof, null),
    ],
);

enum SerializerMeta g_meta_number_compatibility_BB_enum = SerializerMeta(
 [
        SerializerMetaElement(0, "NumbersCompatibility", SerializerElementType.container, NumbersCompatibility_B.sizeof, NumbersCompatibility_B.alignof, [
            SerializerSubobject("n_1", "u16", NumbersCompatibility_B.n_1.offsetof),
            SerializerSubobject("n_2", "i16", NumbersCompatibility_B.n_2.offsetof),
            SerializerSubobject("n_3", "i8", NumbersCompatibility_B.n_3.offsetof),
            SerializerSubobject("n_4", "i64", NumbersCompatibility_B.n_4.offsetof),
            SerializerSubobject("n_5", "f32", NumbersCompatibility_B.n_5.offsetof),
            SerializerSubobject("n_6", "f64", NumbersCompatibility_B.n_6.offsetof),
            SerializerSubobject("n_7", "i32", NumbersCompatibility_B.n_7.offsetof),
            SerializerSubobject("n_8", "i8", NumbersCompatibility_B.n_8.offsetof),
            SerializerSubobject("n_9", "i8", NumbersCompatibility_B.n_9.offsetof),
            SerializerSubobject("arr_1", "i8[]", NumbersCompatibility_B.arr_1.offsetof),
            SerializerSubobject("arr_2", "i32[]", NumbersCompatibility_B.arr_2.offsetof),
        ]),

        SerializerMetaElement(0, "i8[]", SerializerElementType.array, 16, 8, [SerializerSubobject("", "i8", 8u, -1), SerializerSubobject("", "", 0u, -1)]),
        SerializerMetaElement(0, "i16[]", SerializerElementType.array, 16, 8, [SerializerSubobject("", "i16", 8u, -1), SerializerSubobject("", "", 0u, -1)]),
        SerializerMetaElement(0, "i32[]", SerializerElementType.array, 16, 8, [SerializerSubobject("", "i32", 8u, -1), SerializerSubobject("", "", 0u, -1)]),

        SerializerMetaElement(0, "i16", SerializerElementType.signedInteger, short.sizeof, short.alignof, null),
        SerializerMetaElement(0, "u16", SerializerElementType.unsignedInteger, ushort.sizeof, ushort.alignof, null),
        SerializerMetaElement(0, "i32", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
        SerializerMetaElement(0, "f64", SerializerElementType.floatingPointNumber, double.sizeof, double.alignof, null),
        SerializerMetaElement(0, "f32", SerializerElementType.floatingPointNumber, float.sizeof, float.alignof, null),
        SerializerMetaElement(0, "i8", SerializerElementType.signedInteger, byte.sizeof, byte.alignof, null),
        SerializerMetaElement(0, "i64", SerializerElementType.signedInteger, long.sizeof, long.alignof, null),
    ],
);


__gshared SerializerMeta g_meta_number_compatibility_AA = g_meta_number_compatibility_AA_enum;
__gshared SerializerMeta g_meta_number_compatibility_BB = g_meta_number_compatibility_BB_enum;

shared static this()
{
    assert(mser_meta_compute_indices(&g_meta_number_compatibility_AA) == SerializerReturnCode.ok);
    assert(mser_meta_compute_indices(&g_meta_number_compatibility_BB) == SerializerReturnCode.ok);
}

void testCompatibleNumbers(SerializerBackend impl){
    if(serializer_supports_compatible(impl) == false){
        return;
    }
    SerializerHandle handle_AA;
    assert(create_serializer(&handle_AA, &g_meta_number_compatibility_AA, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle_AA);


    NumbersCompatibility_A test_aa;
    {
        test_aa.n_1 = -100;
        test_aa.n_2 = 50000;
        test_aa.n_3 = 100;
        test_aa.n_4 = 100;
        test_aa.n_5 = 1.0;
        test_aa.n_6 = 1.0f;
        test_aa.n_7 = 1.0f;
        test_aa.n_8 = 200.0f;
        test_aa.n_9 = -200.0f;
        test_aa.arr_1 = [100, 200];
        test_aa.arr_2 = [100, 200];
    }

    SerializerInput input = SerializerInput(&test_aa);
    SerializerOutput output;
    assert(mser_serialize(handle_AA, &input, &output) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);
    // print_output(handle_AA, output);

    SerializerHandle handle_BB;
    assert(create_serializer(&handle_BB, &g_meta_number_compatibility_BB, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle_BB);

    NumbersCompatibility_B test_bb;
    DeserializerInput input_de = DeserializerInput(&test_bb, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;

    assert(mser_deserialize(handle_BB, &input_de, &output_de) == SerializerReturnCode.ok);
    scope(exit){
        mser_destroy_variable(&g_meta_number_compatibility_BB, 0, &test_bb, &free);
    }

    assert(test_bb.n_1 == ushort.min);
    assert(test_bb.n_2 == short.max);
    assert(test_bb.n_3 == 100);
    assert(test_bb.n_4 == 100);
    assert(test_bb.n_5 == 1.0f);
    assert(test_bb.n_7 == 1);
    assert(test_bb.n_6 == 1.0);
    assert(test_bb.n_8 == 127);
    assert(test_bb.n_9 == -128);
    assert(test_bb.arr_1 == [100, 127]);
    assert(test_bb.arr_2 == [100, 200]);
}



struct CustomContainer_A {
    int custom_a;
    int custom_b;
    ubyte custom_num_a;
}

struct CustomContainer_B {
    ulong custom_num_a;
    int custom_a;
    int custom_d;
}


enum SerializerMeta g_meta_custom_container_compatibility_AA_enum = SerializerMeta(
 [
        SerializerMetaElement(0, "CustomContainer", SerializerElementType.containerCustom, 0, 0, [
            SerializerSubobject("a", "i32"),
            SerializerSubobject("b", "i32"),
            SerializerSubobject("num_a", "u8"),

        ]),
        SerializerMetaElement(0, "u8", SerializerElementType.unsignedInteger, ubyte.sizeof, ubyte.alignof, null),
        SerializerMetaElement(0, "i32", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
    ],
);

enum SerializerMeta g_meta_custom_container_compatibility_BB_enum = SerializerMeta(
 [
        SerializerMetaElement(0, "CustomContainer", SerializerElementType.containerCustom, 0, 0, [
            SerializerSubobject("num_a", "u64"),
            SerializerSubobject("a", "i32"),
            SerializerSubobject("d", "i32"),

        ]),
        SerializerMetaElement(0, "u64", SerializerElementType.unsignedInteger, ulong.sizeof, ulong.alignof, null),
        SerializerMetaElement(0, "i32", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
    ],
);


__gshared SerializerMeta g_meta_custom_container_compatibility_AA = g_meta_custom_container_compatibility_AA_enum;
__gshared SerializerMeta g_meta_custom_container_compatibility_BB = g_meta_custom_container_compatibility_BB_enum;

extern(C) void on_destroy_custom_container(DestroyInput* input) {}

extern(C) void on_save_custom_container_A(SerializeContainerCustomInput* input){
    CustomContainer_A* cont = cast(CustomContainer_A*)(input.cd.runtime_data_ptr);
    input.serialize_start(input.cd.context, 3);

    input.serialize_element(input.cd.context, 0, cast(ubyte*)&cont.custom_a);
    input.serialize_element(input.cd.context, 1, cast(ubyte*)&cont.custom_b);
    input.serialize_element(input.cd.context, 2, cast(ubyte*)&cont.custom_num_a);
}

extern(C) void on_load_custom_container_A(DeserializeContainerCustomInput* input){
    CustomContainer_A* cont = cast(CustomContainer_A*)(input.cd.runtime_data_ptr);

    foreach(comp_index; 0..input.length){
        MetaIndex name_index;
        SerializerReturnCode status_code = input.deserialize_name(input.cd.context, &name_index);
        if(status_code != SerializerReturnCode.ok){
            return;
        }

        if(name_index == 0){
            status_code = input.deserialize_element(input.cd.context, cast(void*)&cont.custom_a);
        } else if(name_index == 1){
            status_code = input.deserialize_element(input.cd.context, cast(void*)&cont.custom_b);
        } else if(name_index == 2){
            status_code = input.deserialize_element(input.cd.context, cast(void*)&cont.custom_num_a);
        } else {
            input.deserialize_element(input.cd.context, null);
        }
        if(status_code != SerializerReturnCode.ok){
            return;
        }
    }
}

extern(C) void on_save_custom_container_B(SerializeContainerCustomInput* input){
    CustomContainer_B* cont = cast(CustomContainer_B*)(input.cd.runtime_data_ptr);
    input.serialize_start(input.cd.context, 3);

    input.serialize_element(input.cd.context, 0, cast(ubyte*)&cont.custom_num_a);
    input.serialize_element(input.cd.context, 1, cast(ubyte*)&cont.custom_a);
    input.serialize_element(input.cd.context, 2, cast(ubyte*)&cont.custom_d);
}

extern(C) void on_load_custom_container_B(DeserializeContainerCustomInput* input){
    CustomContainer_B* cont = cast(CustomContainer_B*)(input.cd.runtime_data_ptr);

    foreach(comp_index; 0..input.length){
        MetaIndex name_index;
        SerializerReturnCode status_code = input.deserialize_name(input.cd.context, &name_index);
        if(status_code != SerializerReturnCode.ok){
            return;
        }

        if(name_index == 0){
            status_code = input.deserialize_element(input.cd.context, cast(void*)&cont.custom_num_a);
        } else if(name_index == 1){
            status_code = input.deserialize_element(input.cd.context, cast(void*)&cont.custom_a);
        } else if(name_index == 2){
            status_code = input.deserialize_element(input.cd.context, cast(void*)&cont.custom_d);
        } else {
            input.deserialize_element(input.cd.context, null);
        }
        if(status_code != SerializerReturnCode.ok){
            return;
        }
    }
}

shared static this()
{
    assert(mser_meta_compute_indices(&g_meta_custom_container_compatibility_AA) == SerializerReturnCode.ok);
    assert(mser_meta_compute_indices(&g_meta_custom_container_compatibility_BB) == SerializerReturnCode.ok);

    g_meta_custom_container_compatibility_AA.meta_elements[0].on_load_container = &on_load_custom_container_A;
    g_meta_custom_container_compatibility_AA.meta_elements[0].on_save_container = &on_save_custom_container_A;
    g_meta_custom_container_compatibility_AA.meta_elements[0].on_destroy = &on_destroy_custom_container;
    g_meta_custom_container_compatibility_BB.meta_elements[0].on_load_container = &on_load_custom_container_B;
    g_meta_custom_container_compatibility_BB.meta_elements[0].on_save_container = &on_save_custom_container_B;
    g_meta_custom_container_compatibility_BB.meta_elements[0].on_destroy = &on_destroy_custom_container;
}

void testCompatibleCustomContainer(SerializerBackend impl){
    if(serializer_supports_compatible(impl) == false){
        return;
    }

    SerializerHandle handle_AA;
    assert(create_serializer(&handle_AA, &g_meta_custom_container_compatibility_AA, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle_AA);

    SerializerHandle handle_BB;
    assert(create_serializer(&handle_BB, &g_meta_custom_container_compatibility_BB, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle_BB);


    CustomContainer_A test_aa;
    {
        test_aa.custom_a = 1;
        test_aa.custom_b = 2;
        test_aa.custom_num_a = 100;
    }

    SerializerInput input = SerializerInput(&test_aa);
    SerializerOutput output;
    assert(mser_serialize(handle_AA, &input, &output) == SerializerReturnCode.ok);
    // print_output(handle_AA, output);

    CustomContainer_B test_bb;
    DeserializerInput input_de = DeserializerInput(&test_bb, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;

    assert(mser_deserialize(handle_BB, &input_de, &output_de) == SerializerReturnCode.ok);
    free(output.out_buffer);
    mser_destroy_variable(&g_meta_custom_container_compatibility_BB, 0, &test_bb, &free);

    assert(test_bb.custom_a == 1);
    assert(test_bb.custom_d == 0);
    assert(test_bb.custom_num_a == 100);


    {
        test_bb.custom_a = 1;
        test_bb.custom_d = 2;
        test_bb.custom_num_a = 1000;
    }

    SerializerInput input2 = SerializerInput(&test_bb);
    assert(mser_serialize(handle_BB, &input2, &output) == SerializerReturnCode.ok);
    // print_output(handle_BB, output);

    test_aa = test_aa.init;
    input_de = DeserializerInput(&test_aa, null, 0, output.out_buffer, output.out_buffer_length);
    assert(mser_deserialize(handle_AA, &input_de, &output_de) == SerializerReturnCode.ok);
    free(output.out_buffer);
    mser_destroy_variable(&g_meta_custom_container_compatibility_AA, 0, &test_aa, &free);

    assert(test_aa.custom_a == 1);
    assert(test_aa.custom_b == 0);
    assert(test_aa.custom_num_a == 255);
}
