module mser.test.custom;

import core.stdc.stdio;
import core.stdc.string;
import core.stdc.stdlib : free, malloc;
// import std.stdio;

import mser;
import mser.test.structs;


struct Obj{
    ubyte valA;
    ubyte valB;
}

enum SerializerMeta g_meta_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "Obj", SerializerElementType.container, Obj.sizeof, Obj.alignof, [SerializerSubobject("valA", "Obj_i32_A", 0, 1), SerializerSubobject("valB", "Obj_i32_B", 0, 2)]),
        SerializerMetaElement(1, "Obj_i32_A", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
        SerializerMetaElement(2, "Obj_i32_B", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
    ],
);
__gshared SerializerMeta g_meta = g_meta_enum;

extern(C) void on_save_A(SerializeCustomInput* input){
    assert(input.cd.element_index == 1);
    Obj* obj = cast(Obj*)(input.cd.runtime_data_ptr);
    int valA = obj.valA;
    input.serialize(input.cd.context, &valA);
}
extern(C) void on_save_B(SerializeCustomInput* input){
    assert(input.cd.element_index == 2);
    Obj* obj = cast(Obj*)(input.cd.runtime_data_ptr);
    int valB = obj.valB;
    input.serialize(input.cd.context, &valB);
}

extern(C) void on_load_A(DeserializeCustomInput* input){
    assert(input.cd.element_index == 1);
    Obj* obj = cast(Obj*)(input.cd.runtime_data_ptr);
    int valA = obj.valA;
    input.deserialize(input.cd.context, &valA);
    obj.valA = cast(ubyte)valA;
}
extern(C) void on_load_B(DeserializeCustomInput* input){
    assert(input.cd.element_index == 2);
    Obj* obj = cast(Obj*)(input.cd.runtime_data_ptr);
    int valB = obj.valB;
    input.deserialize(input.cd.context, &valB);
    obj.valB = cast(ubyte)valB;
}


void testCustom(SerializerBackend impl){
    g_meta.meta_elements[1].on_save = &on_save_A;
    g_meta.meta_elements[1].on_load = &on_load_A;
    g_meta.meta_elements[2].on_save = &on_save_B;
    g_meta.meta_elements[2].on_load = &on_load_B;


    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta, impl) == SerializerReturnCode.ok);
    if(mser_has_properties(handle, SerializerProperties.canSerialize) != SerializerReturnCode.ok){
        return;
    }
    scope(exit)mser_destroy(&handle);

    Obj tt = Obj(1, 2);
    SerializerInput input = SerializerInput(&tt);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit) free(output.out_buffer);

    if(mser_has_properties(handle,SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }

    Obj tt22;
    DeserializerInput input_de = DeserializerInput(&tt22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);

    assert(tt.valA == 1);
    assert(tt.valB == 2);
}