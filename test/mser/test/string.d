module mser.test.string;

import core.stdc.stdio;
import core.stdc.string;
import core.stdc.stdlib : free, malloc;

import mser;
import mser.test.structs;


struct Obj{
    TestCustomString strange_str;
}

struct TestCustomString{
    char[5] part_a;
    int num;
    char[2] part_b;
}

enum SerializerMeta g_meta_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "Obj", SerializerElementType.container, Obj.sizeof, Obj.alignof, [SerializerSubobject("strange_str", "TestCustomString", Obj.strange_str.offsetof, 1)]),
        SerializerMetaElement(1, "TestCustomString", SerializerElementType.stringCustom, TestCustomString.sizeof, TestCustomString.alignof, null),
    ],
);
__gshared SerializerMeta g_meta = g_meta_enum;

extern(C) void on_save_string(SerializeStringInput* input){
    assert(input.cd.element_index == 1);
    TestCustomString* strange_str = cast(TestCustomString*)(input.cd.runtime_data_ptr);

    char[TestCustomString.part_a.length + TestCustomString.part_b.length + 1] str_whole;
    memcpy(str_whole.ptr, strange_str.part_a.ptr, strange_str.part_a.length);
    memcpy(str_whole.ptr + strange_str.part_a.length, strange_str.part_b.ptr, strange_str.part_b.length);
    str_whole[$-1] = '\0';
    input.serialize_string(input.cd.context, str_whole.ptr, str_whole.length - 1);
}

extern(C) void on_load_string(DeserializeStringInput* input){
    assert(input.cd.element_index == 1);
    TestCustomString* strange_str = cast(TestCustomString*)(input.cd.runtime_data_ptr);

    // Here goes strange logic to copy from continuous string provided from serializer to strange string
    size_t length = input.length;
    char* str = input.str;
    if(length > strange_str.part_a.length){
        memcpy(strange_str.part_a.ptr, str, strange_str.part_a.length);
        length -= strange_str.part_a.length;
        str += strange_str.part_a.length;
    } else {
        memcpy(strange_str.part_a.ptr, str, length);
        return;
    }

    if(length > strange_str.part_b.length){
        length = strange_str.part_b.length;
    }
    memcpy(strange_str.part_b.ptr, str, length);
}


void testCustomString(SerializerBackend impl){
    g_meta.meta_elements[1].on_save_string = &on_save_string;
    g_meta.meta_elements[1].on_load_string = &on_load_string;


    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta, impl) == SerializerReturnCode.ok);
    if(mser_has_properties(handle, SerializerProperties.canSerialize) != SerializerReturnCode.ok){
        return;
    }
    scope(exit)mser_destroy(&handle);

    Obj tt;
    tt.strange_str.part_a = ['a', 'b', 'c', 'd', 'e'];
    tt.strange_str.part_b = ['f', 'g'];
    SerializerInput input = SerializerInput(&tt);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit) free(output.out_buffer);

    if(mser_has_properties(handle,SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }

    Obj tt22;
    DeserializerInput input_de = DeserializerInput(&tt22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);

    assert(tt22.strange_str.part_a == ['a', 'b', 'c', 'd', 'e']);
    assert(tt22.strange_str.part_b == ['f', 'g']);
}