module mser.test.structs;

import mser;

/// Backends to test
enum SerializerBackend : int {
    json,
    cjson,
    libxml2,
    binary,
    binary_compatible,
    toml,
}

struct BenchmarkDataArray {
    BenchmarkData[] data;
}
struct BenchmarkData {
    string name;
    string serializer;
    long date;
    bool serialize;
    int iterationNum;
    long create_time_ns;
    long process_time_total_ns;
    long process_time_ns;
    long size;
}

enum SerializerMeta g_meta_benchmark_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "BenchmarkDataArray", SerializerElementType.container, BenchmarkDataArray.sizeof, BenchmarkDataArray.alignof, [SerializerSubobject("data", "ARRAY_BenchmarkData", BenchmarkDataArray.data.offsetof, 1)]),
        SerializerMetaElement(1, "ARRAY_BenchmarkData", SerializerElementType.array, 16, 8, [SerializerSubobject("", "BenchmarkData", 8u, 2),SerializerSubobject("", "", 0u, -1)]),
        SerializerMetaElement(2, "BenchmarkData", SerializerElementType.container, BenchmarkData.sizeof, BenchmarkData.alignof, [
            SerializerSubobject("name", "STRING", BenchmarkData.name.offsetof, 6),
            SerializerSubobject("serializer", "STRING", BenchmarkData.serializer.offsetof, 6),
            SerializerSubobject("date", "u64", BenchmarkData.date.offsetof, 3),
            SerializerSubobject("serialize", "u8", BenchmarkData.serialize.offsetof, 5),
            SerializerSubobject("iterationNum", "i32", BenchmarkData.iterationNum.offsetof, 4),
            SerializerSubobject("create_time_ns", "u64", BenchmarkData.create_time_ns.offsetof, 3),
            SerializerSubobject("process_time_total_ns", "u64", BenchmarkData.process_time_total_ns.offsetof, 3),
            SerializerSubobject("process_time_ns", "u64", BenchmarkData.process_time_ns.offsetof, 3),
            SerializerSubobject("size", "u64", BenchmarkData.size.offsetof, 3),
        ]),
        SerializerMetaElement(3, "u64", SerializerElementType.unsignedInteger, 8, 8, null),
        SerializerMetaElement(4, "u8", SerializerElementType.unsignedInteger, 1, 1, null),
        SerializerMetaElement(5, "i32", SerializerElementType.signedInteger, 4, 4, null),
        SerializerMetaElement(6, "STRING", SerializerElementType.string, 16, 8, [SerializerSubobject(null, null, size_t.sizeof, -1), SerializerSubobject(null, "__STRING_LENGTH", 0, 7)]),
        SerializerMetaElement(7, "STRING_LENGTH", SerializerElementType.unsignedInteger, size_t.sizeof, size_t.alignof, null),
    ]
);

__gshared SerializerMeta g_meta_benchmark = g_meta_benchmark_enum;

SerializerReturnCode create_serializer(SerializerHandle* handle, SerializerMeta* meta, SerializerBackend backend){
    MserCreateInput create_input = MserCreateInput(meta);
    switch(backend){
        case SerializerBackend.json: return mser_create_embedded_json(handle, &create_input);
        case SerializerBackend.cjson: return mser_create_cjson(handle, &create_input);
        case SerializerBackend.libxml2: return mser_create_libxml2(handle, &create_input);
        case SerializerBackend.binary: return mser_create_embedded_binary(handle, &create_input);
        case SerializerBackend.binary_compatible: return mser_create_embedded_binary_compatible(handle, &create_input);
        case SerializerBackend.toml: return mser_create_toml(handle, &create_input);
        default: assert(0);
    }
}
struct TestChild {
    ubyte d;
    int e;
}

struct TestRoot {
    ubyte a;
    TestChild b;
    TestChild c;
    TestChild[] arr;
}

struct TestRootMissing {
    // ubyte a;
    TestChild b;
    // TestChild c;
    // TestChild[] arr;
}

enum SerializerMeta g_meta_test_root_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "TestRoot", SerializerElementType.container, 40, 8, [SerializerSubobject("a", "u8", 0u, 3), SerializerSubobject("b", "TestChild", 4u, 1), SerializerSubobject("c", "TestChild", 12u, 1), SerializerSubobject("arr", "__ARRAY_TestChild", 24u, 2)]),
        SerializerMetaElement(1, "TestChild", SerializerElementType.container, 8, 8, [SerializerSubobject("d", "u8", 0u, 3), SerializerSubobject("e", "i32", 4u, 4)]),
        SerializerMetaElement(2, "__ARRAY_TestChild", SerializerElementType.array, 16, 8, [SerializerSubobject("", "TestChild", 8u, 1),SerializerSubobject("", "", 0u, -1)]),
        SerializerMetaElement(3, "u8", SerializerElementType.unsignedInteger, 1, 1, null),
        SerializerMetaElement(4, "i32", SerializerElementType.signedInteger, 4, 4, null),
    ],
);

enum SerializerMeta g_meta_test_root_missing_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "TestRootMissing", SerializerElementType.container, 40, 8, [SerializerSubobject("b", "TestChild", 0u, 1)]),
        SerializerMetaElement(1, "TestChild", SerializerElementType.container, 8, 8, [SerializerSubobject("d", "u8", 0u, 2), SerializerSubobject("e", "i32", 4u, 3)]),
        SerializerMetaElement(2, "u8", SerializerElementType.unsignedInteger, 1, 1, null),
        SerializerMetaElement(3, "i32", SerializerElementType.signedInteger, 4, 4, null),
    ],
);

__gshared SerializerMeta g_meta_test_root = g_meta_test_root_enum;
__gshared SerializerMeta g_meta_test_root_missing = g_meta_test_root_missing_enum;

shared static this()
{
    assert(mser_meta_compute_indices(&g_meta_test_root) == SerializerReturnCode.ok);
    assert(mser_meta_compute_indices(&g_meta_test_root_missing) == SerializerReturnCode.ok);
}


enum TestRoot g_test_root_example = TestRoot(1, TestChild(2, 3), TestChild(4, 5), [TestChild(6, 7), TestChild(8, 9)]);
enum TestRootMissing g_test_root_example_missing = TestRootMissing(TestChild(2, 3));

