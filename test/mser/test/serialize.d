module mser.test.serialize;

import core.stdc.stdio;
import core.stdc.stdlib : free, malloc;

import mser;
import mser.test.structs;


void testSerialize(SerializerBackend impl){
    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta_test_root, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle);

    if(mser_has_properties(handle, SerializerProperties.canSerialize) != SerializerReturnCode.ok){
        return;
    }

    TestRoot tt = g_test_root_example;
    SerializerInput input = SerializerInput(&tt);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);

    // if(mser_has_properties(handle, SerializerProperties.isBinary) == SerializerReturnCode.propertiesNotSupported){
    //     printf("size(%d) (%s)\n", cast(int)output.out_buffer_length, cast(char*)output.out_buffer);
    // }
}