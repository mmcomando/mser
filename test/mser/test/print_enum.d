module mser.test.print_enum;

import mser;


void test_print_enum(){
    const(char)* str;

    str = mser_enum_str_SerializerElementType(SerializerElementType.none);
    assert(str != null && str[0..4] == "none");
    str = mser_enum_str_SerializerElementType(SerializerElementType.pointerCustom_reserved);
    assert(str != null && str[0..22] == "pointerCustom_reserved");
    str = mser_enum_str_SerializerElementType(cast(SerializerElementType)255);
    assert(str != null && str[0..7] == "UNKNOWN");


    str = mser_enum_str_SerializerReturnCode(SerializerReturnCode.ok);
    assert(str != null && str[0..2] == "ok");
    str = mser_enum_str_SerializerReturnCode(SerializerReturnCode.metaNotFound);
    assert(str != null && str[0..12] == "metaNotFound");
    str = mser_enum_str_SerializerReturnCode(cast(SerializerReturnCode)255);
    assert(str != null && str[0..7] == "UNKNOWN");


    str = mser_enum_str_SerializerProperties(SerializerProperties.none);
    assert(str != null && str[0..4] == "none");
    str = mser_enum_str_SerializerProperties(SerializerProperties.supportsNotPresentData);
    assert(str != null && str[0..22] == "supportsNotPresentData");
    str = mser_enum_str_SerializerProperties(SerializerProperties.canSerialize | SerializerProperties.supportsNotPresentData);
    assert(str != null && str[0..14] == "MULTIPLE_FLAGS");
    str = mser_enum_str_SerializerProperties(cast(SerializerProperties)255);
    assert(str != null && str[0..7] == "UNKNOWN");
}