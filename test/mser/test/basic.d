module mser.test.basic;


import mser;
import mser.test.structs;

void testCreate(SerializerBackend impl){
    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta_test_root, impl) == SerializerReturnCode.ok);
    assert(handle != null);
    assert(mser_destroy(&handle) == SerializerReturnCode.ok);
    assert(handle == null);
}