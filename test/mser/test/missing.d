// Very crude implementation of checking if field is missing by storing adresses of variables which were missing
// But it ilustrates flexibility of callbacks with index to meta element
module mser.test.missing;

import core.stdc.stdlib : free, malloc;

import mser;
import mser.test.structs;


enum g_maxMissingFields = 128;
__gshared void*[128] g_missing_fields;
__gshared size_t g_missing_used;

extern(C) void onMissing(SerializerMeta* meta, void* runtime_data_ptr, MetaIndex element_index, void* user_data){
    g_missing_fields[g_missing_used] = runtime_data_ptr;
    g_missing_used++;
    // printf("%d\n", element_index);
}

bool isMissing(void* runtime_data_ptr){
    foreach(ptr; g_missing_fields[0..g_missing_used]) {
        if(ptr == runtime_data_ptr){
            return true;
        }
    }
    return false;
}


void testMissing(SerializerBackend impl){
    SerializerHandle handle_missing_fields;
    SerializerHandle handle_all_fields;
    assert(create_serializer(&handle_missing_fields, &g_meta_test_root_missing, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle_missing_fields);

    if(mser_has_properties(handle_missing_fields, SerializerProperties.canSerialize | SerializerProperties.canDeserialize | SerializerProperties.supportsNotPresentData) != SerializerReturnCode.ok){
        return;
    }

    TestRootMissing tt = g_test_root_example_missing;
    SerializerInput input = SerializerInput(&tt);
    SerializerOutput output;
    assert(mser_serialize(handle_missing_fields, &input, &output) == SerializerReturnCode.ok);
    // printf("%s\n", cast(char*)output.out_buffer);


    SerializerMeta meta_with_callback = g_meta_test_root;
    foreach(ref element; meta_with_callback.meta_elements) element.on_missing_field = &onMissing;
    assert(create_serializer(&handle_all_fields, &meta_with_callback, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle_all_fields);

    TestRoot tt22;
    DeserializerInput input_de = DeserializerInput(&tt22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle_all_fields, &input_de, &output_de) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);
    assert(0 == tt22.a);
    assert(tt.b == tt22.b);
    assert(0 == tt22.c.d);
    assert(0 == tt22.c.e);
    assert(0 == tt22.arr.length);
    assert(isMissing(&tt22.a));
    assert(isMissing(&tt22.b) == false);
    assert(isMissing(&tt22.b.d) == false);
    assert(isMissing(&tt22.b.e) == false);
    assert(isMissing(&tt22.c));
    assert(isMissing(&tt22.arr));
}


__gshared MetaIndex g_TestChild_index;
__gshared size_t g_finished_num;

extern(C) void onFinished(SerializerMeta* meta, void* runtime_data_ptr, MetaIndex element_index, void* user_data){
    assert(element_index == g_TestChild_index);
    g_finished_num++;
}

void testOnLoadFinish(SerializerBackend impl){
    g_finished_num = 0;
    SerializerHandle handle;

    SerializerMeta meta_with_callback = g_meta_test_root;
    assert(mser_meta_get_index(&meta_with_callback, "TestChild", "TestChild".length, &g_TestChild_index) == SerializerReturnCode.ok);
    meta_with_callback.meta_elements[g_TestChild_index].on_load_finish = &onFinished;

    assert(create_serializer(&handle, &meta_with_callback, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle);

    if(mser_has_properties(handle, SerializerProperties.canSerialize | SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }

    TestRoot tt = g_test_root_example;
    SerializerInput input = SerializerInput(&tt);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);

    TestRoot tt22;
    DeserializerInput input_de = DeserializerInput(&tt22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);
    scope(exit)free(tt22.arr.ptr);

    assert(g_finished_num == 4);
}


enum SerializerMetaElement[] aa =     [
        {0, "TestRootMissing", SerializerElementType.container, 40, 8, [SerializerSubobject("b", "TestChild", 0u, 1)], on_load_finish:&onFinished},
        {1, "TestChild", SerializerElementType.container, 8, 8, [SerializerSubobject("d", "u8", 0u, 2), SerializerSubobject("e", "i32", 4u, 3)]},
        {2, "u8", SerializerElementType.unsignedInteger, 1, 1, null},
        {3, "i32", SerializerElementType.signedInteger, 4, 4, null},
    ];