module mser.test.serialize_deserialize;

import core.stdc.stdio;
import core.stdc.stdlib : free, malloc;

import mser;
import mser.test.structs;


void testSerializeDeserialize(SerializerBackend impl){
    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta_test_root, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle);

    if(mser_has_properties(handle, SerializerProperties.canSerialize | SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }

    TestRoot tt = g_test_root_example;
    SerializerInput input = SerializerInput(&tt);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);
    if(mser_has_properties(handle, SerializerProperties.isBinary) == SerializerReturnCode.propertiesNotSupported){
        printf("%s\n", cast(char*)output.out_buffer);
    }
    TestRoot tt22;
    DeserializerInput input_de = DeserializerInput(&tt22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);
    scope(exit)free(tt22.arr.ptr);
    assert(tt.a == tt22.a);
    assert(tt.b == tt22.b);
    assert(tt.c == tt22.c);
    assert(tt.arr.length == 2);
    assert(tt.arr.length == tt22.arr.length);
    assert(tt.arr[0].d == tt22.arr[0].d);
    assert(tt.arr[0].e == tt22.arr[0].e);
    assert(tt.arr[1].d == tt22.arr[1].d);
    assert(tt.arr[1].e == tt22.arr[1].e);
}