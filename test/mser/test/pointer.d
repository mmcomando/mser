module mser.test.pointer;

import core.stdc.stdio;
import core.stdc.stdlib : free, malloc;

import mser;
import mser.test.structs;


struct Obj{
    TestData* data;
    TestData* data2;
}

struct TestData{
    int a;
    int b;
}

enum SerializerMeta g_meta_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "Obj", SerializerElementType.container, Obj.sizeof, Obj.alignof, [SerializerSubobject("data", "TestDataPointer", Obj.data.offsetof, 1), SerializerSubobject("data2", "TestDataPointer", Obj.data2.offsetof, 1)]),
        SerializerMetaElement(1, "TestDataPointer", SerializerElementType.pointer, (TestData*).sizeof, (TestData*).alignof, [SerializerSubobject("TestData", "i32", 0, 2)]),
        SerializerMetaElement(2, "TestData", SerializerElementType.container, TestData.sizeof, TestData.alignof, [SerializerSubobject("a", "i32", TestData.a.offsetof, 3), SerializerSubobject("b", "i32", TestData.b.offsetof, 3)]),
        SerializerMetaElement(3, "i32", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
    ],
);
__gshared SerializerMeta g_meta = g_meta_enum;


void testPointer(SerializerBackend impl){
    if(impl == SerializerBackend.binary){
        return; // TODO: support for SerializerElementType.pointer in simple binary format
    }
    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta, impl) == SerializerReturnCode.ok);
    if(mser_has_properties(handle, SerializerProperties.canSerialize) != SerializerReturnCode.ok){
        return;
    }
    scope(exit) mser_destroy(&handle);

    Obj obj;
    obj.data = cast(TestData*)malloc(TestData.sizeof);
    scope(exit) free(obj.data);
    obj.data.a = 1;
    obj.data.b = 2;
    SerializerInput input = SerializerInput(&obj);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit) free(output.out_buffer);

    if(mser_has_properties(handle,SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }
    Obj obj22;
    DeserializerInput input_de = DeserializerInput(&obj22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);
    scope(exit) free(obj22.data);

    assert(obj22.data != null);
    assert(obj22.data2 == null);
    assert(obj22.data.a == 1);
    assert(obj22.data.b == 2);
}