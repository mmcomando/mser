module mser.test.map;

import core.stdc.stdio;
import core.stdc.stdlib : free, malloc;
// import std.stdio;

import mser;
import mser.test.structs;

// alias Str = const(char)[];

// struct MapTest{
//     Str[Str] values;
// }

// enum SerializerMeta g_meta_enum = SerializerMeta(
//     [
//         SerializerMetaElement(0, "MapTest", SerializerElementType.map, MapTest.sizeof, MapTest.alignof, [SerializerSubobject(null, "__STRING", MapTest.values.offsetof, 1), SerializerSubobject(null, "__STRING", MapTest.values.offsetof, 1)]),
//         SerializerMetaElement(1, "__STRING", SerializerElementType.string, 16, 8, [SerializerSubobject(null, null, size_t.sizeof, -1), SerializerSubobject(null, "__STRING_LENGTH", 0, 2)]),
//         SerializerMetaElement(2, "__STRING_LENGTH", SerializerElementType.unsignedInteger, size_t.sizeof, size_t.alignof, null),
//     ],
// );
// __gshared SerializerMeta g_meta = g_meta_enum;


// extern(C) void on_save_map(SerializeMapInput* input){
//     assert(input.cd.element_index == 0);
//     Str[Str] map = *cast(Str[Str]*)(input.cd.runtime_data_ptr);
//     input.serialize_start(input.cd.context, map.length);
//     foreach(key_val; map.byKeyValue){
//         input.serialize_element(input.cd.context, cast(ubyte*)(&key_val.key()), cast(ubyte*)(&key_val.value()), 1);
//     }
// }

// extern(C) void on_load_map(DeserializeMapInput* input){
//     assert(input.cd.element_index == 0);
//     Str[Str]* map = cast(Str[Str]*)(input.cd.runtime_data_ptr);
//     foreach(i; 0..input.length){
//         const(char)[] key;
//         const(char)[] value;
//         input.deserialize_name(input.cd.context, cast(void*)&key);
//         input.deserialize_element(input.cd.context, cast(void*)&value, 1);
//         (*map)[key] = value;
//     }
// }

// void testMap(SerializerBackend impl){
//     if(impl != SerializerBackend.cjson && impl != SerializerBackend.binary && impl != SerializerBackend.binary_compatible){
//         return;
//     }
//     g_meta.meta_elements[0].on_save_map = &on_save_map;
//     g_meta.meta_elements[0].on_load_map = &on_load_map;


//     SerializerHandle handle;
//     assert(create_serializer(&handle, &g_meta, impl) == SerializerReturnCode.ok);

//     scope(exit)mser_destroy(&handle);

//     MapTest tt;
//     tt.values["aa"] = "aaa";
//     tt.values["bb"] = "bbb";
//     tt.values["cc"] = "ccc";
//     SerializerInput input = SerializerInput(&tt);
//     SerializerOutput output;
//     assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
//     scope(exit) mser_free(output.out_buffer);
//     // writeln(cast(char[])output.out_buffer[0..output.out_buffer_length]);

//     MapTest tt22;
//     DeserializerInput input_de = DeserializerInput(&tt22, null, 0, output.out_buffer, output.out_buffer_length);
//     DeserializerOutput output_de;
//     assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);
//     scope(exit){
//         foreach(key_val; tt22.values.byKeyValue){
//             mser_free(cast(void*)key_val.key.ptr);
//             mser_free(cast(void*)key_val.value.ptr);
//         }

//     }
//     assert(tt22.values.length == 3);
//     assert(tt22.values["aa"] == "aaa");
//     assert(tt22.values["bb"] == "bbb");
//     assert(tt22.values["cc"] == "ccc");
// }


struct MapKnownKeysTest{
    int a;
    ubyte b;
    byte c;
}

enum SerializerMeta g_meta_known_keys_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "MapKnownKeysTest", SerializerElementType.containerCustom, MapKnownKeysTest.sizeof, MapKnownKeysTest.alignof, [
            SerializerSubobject("a", "i32", 0, 1),
            SerializerSubobject("b", "u8", 0, 2),
            SerializerSubobject("c", "i8", 0, 3),
        ]),
        SerializerMetaElement(1, "i32", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
        SerializerMetaElement(2, "u8", SerializerElementType.unsignedInteger, ubyte.sizeof, ubyte.alignof, null),
        SerializerMetaElement(3, "i8", SerializerElementType.signedInteger, byte.sizeof, byte.alignof, null),
    ],
);
__gshared SerializerMeta g_meta_known_keys = g_meta_known_keys_enum;


extern(C) void on_save_container(SerializeContainerCustomInput* input){
    assert(input.cd.element_index == 0);
    MapKnownKeysTest* map = cast(MapKnownKeysTest*)(input.cd.runtime_data_ptr);
    input.serialize_start(input.cd.context, 3);
    input.serialize_element(input.cd.context, 0, &map.a);
    input.serialize_element(input.cd.context, 1, &map.b);
    input.serialize_element(input.cd.context, 2, &map.c);

}

extern(C) void on_load_container(DeserializeContainerCustomInput* input){
    assert(input.cd.element_index == 0);
    MapKnownKeysTest* map = cast(MapKnownKeysTest*)(input.cd.runtime_data_ptr);
    assert(input.length == 3);
    MetaIndex name_index;
    input.deserialize_name(input.cd.context, &name_index); assert(name_index == 0);
    input.deserialize_element(input.cd.context, cast(void*)&map.a);
    input.deserialize_name(input.cd.context, &name_index); assert(name_index == 1);
    input.deserialize_element(input.cd.context, cast(void*)&map.b);
    input.deserialize_name(input.cd.context, &name_index); assert(name_index == 2);
    input.deserialize_element(input.cd.context, cast(void*)&map.c);
}

void testContainerCustom(SerializerBackend impl){
    if(impl != SerializerBackend.cjson && impl != SerializerBackend.binary && impl != SerializerBackend.binary_compatible){
        return;
    }
    g_meta_known_keys.meta_elements[0].on_save_container = &on_save_container;
    g_meta_known_keys.meta_elements[0].on_load_container = &on_load_container;


    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta_known_keys, impl) == SerializerReturnCode.ok);

    scope(exit)mser_destroy(&handle);

    MapKnownKeysTest tt = MapKnownKeysTest(1, 2, 3);
    SerializerInput input = SerializerInput(&tt);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit) free(output.out_buffer);

    MapKnownKeysTest tt22;
    DeserializerInput input_de = DeserializerInput(&tt22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);

    assert(tt22.a == 1);
    assert(tt22.b == 2);
    assert(tt22.c == 3);
}