module mser.test.array;

import core.stdc.stdio;
import core.stdc.stdlib : free, malloc;

import mser;
import mser.test.structs;


struct Obj{
    TestArray arr; // Save as array of 3 ints
}

struct TestArray{
    int a;
    int b;
    bool not_used;
    int c;
}

enum SerializerMeta g_meta_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "Obj", SerializerElementType.container, Obj.sizeof, Obj.alignof, [SerializerSubobject("arr", "TestArray", Obj.arr.offsetof, 1)]),
        SerializerMetaElement(1, "TestArray", SerializerElementType.arrayCustom, TestArray.sizeof, TestArray.alignof, [SerializerSubobject(null, "i32", 0, 2)]),
        SerializerMetaElement(2, "i32", SerializerElementType.signedInteger, int.sizeof, int.alignof, null),
    ],
);
__gshared SerializerMeta g_meta = g_meta_enum;

extern(C) void on_save_array(SerializeArrayInput* input){
    assert(input.cd.element_index == 1);
    TestArray* arr = cast(TestArray*)(input.cd.runtime_data_ptr);
    input.serialize_start(input.cd.context, 3);
    input.serialize_element(input.cd.context, cast(void*)&arr.a);
    input.serialize_element(input.cd.context, cast(void*)&arr.b);
    input.serialize_element(input.cd.context, cast(void*)&arr.c);
}

extern(C) void on_load_array(DeserializeArrayInput* input){
    assert(input.cd.element_index == 1);
    TestArray* arr = cast(TestArray*)(input.cd.runtime_data_ptr);
    assert(input.length == 3);
    input.deserialize_element(input.cd.context, cast(void*)&arr.a);
    input.deserialize_element(input.cd.context, cast(void*)&arr.b);
    input.deserialize_element(input.cd.context, cast(void*)&arr.c);
}


void testCustomArray(SerializerBackend impl){
    g_meta.meta_elements[1].on_save_array = &on_save_array;
    g_meta.meta_elements[1].on_load_array = &on_load_array;


    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta, impl) == SerializerReturnCode.ok);
    if(mser_has_properties(handle, SerializerProperties.canSerialize) != SerializerReturnCode.ok){
        return;
    }
    scope(exit)mser_destroy(&handle);

    Obj tt;
    tt.arr.a = 1;
    tt.arr.b = 2;
    tt.arr.c = 3;
    SerializerInput input = SerializerInput(&tt);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit) free(output.out_buffer);

    if(mser_has_properties(handle,SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }

    Obj tt22;
    DeserializerInput input_de = DeserializerInput(&tt22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);

    assert(tt22.arr.a == 1);
    assert(tt22.arr.b == 2);
    assert(tt22.arr.c == 3);
}


struct ObjBinary{
    ubyte[] data;
    ubyte[4] data_fixed;
}

enum SerializerMeta g_meta_binary_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "ObjBinary", SerializerElementType.container, ObjBinary.sizeof, ObjBinary.alignof, [SerializerSubobject("data", "Binary", ObjBinary.data.offsetof, 1), SerializerSubobject("data_fixed", "BinaryFixed", ObjBinary.data_fixed.offsetof, 2)]),
        SerializerMetaElement(1, "Binary", SerializerElementType.binary, 16, 8, [SerializerSubobject(null, null, size_t.sizeof, -1), SerializerSubobject(null, "Length", 0, 3)]),
        SerializerMetaElement(2, "BinaryFixed", SerializerElementType.binaryFixed, ObjBinary.data_fixed.sizeof, ObjBinary.data_fixed.alignof, null),
        SerializerMetaElement(3, "Length", SerializerElementType.unsignedInteger, size_t.sizeof, size_t.alignof, null),
    ],
);
__gshared SerializerMeta g_meta_binary = g_meta_binary_enum;


void testBinary(SerializerBackend impl){
    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta_binary, impl) == SerializerReturnCode.ok);
    if(mser_has_properties(handle, SerializerProperties.canSerialize) != SerializerReturnCode.ok){
        return;
    }
    scope(exit)mser_destroy(&handle);

    ObjBinary obj;
    obj.data = [0, 1, 2, 254];
    obj.data_fixed = [6, 7, 8, 9];
    SerializerInput input = SerializerInput(&obj);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit) free(output.out_buffer);

    if(mser_has_properties(handle,SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }

    ObjBinary obj22;
    DeserializerInput input_de = DeserializerInput(&obj22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);
    scope(exit) free(obj22.data.ptr);

    assert(obj22.data.length == 4);
    assert(obj22.data[0] == 0);
    assert(obj22.data[1] == 1);
    assert(obj22.data[2] == 2);
    assert(obj22.data[3] == 254);
    assert(obj22.data_fixed[0] == 6);
    assert(obj22.data_fixed[1] == 7);
    assert(obj22.data_fixed[2] == 8);
    assert(obj22.data_fixed[3] == 9);
}