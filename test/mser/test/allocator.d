module mser.test.allocator;

import core.stdc.stdio;
import core.stdc.stdlib : free, malloc;

import mser;
import mser.test.structs;


__gshared int counter_allocate;
__gshared int counter_free;

extern(C) void* my_allocate(CallbackCommonData* input, size_t length){
    counter_allocate++;
    return malloc(length);
}

extern(C) void my_free(CallbackCommonData* input, void* data){
    counter_free++;
    free(data);
}

void testAllocator(SerializerBackend impl){
    counter_allocate = 0;
    counter_free = 0;
    SerializerMeta meta = g_meta_test_root;
    meta.meta_elements[2].on_allocate = &my_allocate;
    meta.meta_elements[2].on_free = &my_free;

    SerializerHandle handle;
    assert(create_serializer(&handle, &meta, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle);
    if(mser_has_properties(handle, SerializerProperties.canSerialize | SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }

    TestRoot tt = g_test_root_example;
    SerializerInput input = SerializerInput(&tt);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);

    assert(counter_allocate == 0);
    assert(counter_free == 0);

    TestRoot tt22;
    DeserializerInput input_de = DeserializerInput(&tt22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);

    assert(counter_allocate == 1);
    assert(counter_free == 0);

    mser_destroy_variable(&meta, 0, &tt22, &free);
    assert(counter_allocate == 1);
    assert(counter_free == 1);
}
