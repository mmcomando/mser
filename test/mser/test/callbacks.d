module mser.test.callbacks;

import core.stdc.stdlib : free, malloc;
import std.stdio;

import mser;
import mser.test.structs;


struct Test {
    ubyte[] arr;
}

enum SerializerMeta g_meta_enum = SerializerMeta(
    [
        SerializerMetaElement(0, "Test", SerializerElementType.container, Test.sizeof, Test.alignof, [SerializerSubobject("arr", "ARRAY", 0u, 1)]),
        SerializerMetaElement(1, "ARRAY", SerializerElementType.array, 16, 8, [SerializerSubobject("", "u8", 8u, 2),SerializerSubobject("", "", 0u, -1)]),
        SerializerMetaElement(2, "u8", SerializerElementType.unsignedInteger, 1, 1, null),
    ],
);

__gshared SerializerMeta g_meta = g_meta_enum;
__gshared int g_counter_save;
__gshared int g_counter_load;


extern(C) void on_save(SerializeCustomInput* input){
    g_counter_save++;
    input.serialize(input.cd.context, input.cd.runtime_data_ptr);
}

extern(C) void on_load(DeserializeCustomInput* input){
    g_counter_load++;
    input.deserialize(input.cd.context, input.cd.runtime_data_ptr);
}

void testCallbacks(SerializerBackend impl){
    g_counter_save = 0;
    g_counter_load = 0;
    g_meta.meta_elements[2].on_save = &on_save;
    g_meta.meta_elements[2].on_load = &on_load;


    SerializerHandle handle;
    assert(create_serializer(&handle, &g_meta, impl) == SerializerReturnCode.ok);
    scope(exit)mser_destroy(&handle);

    if(mser_has_properties(handle, SerializerProperties.canSerialize) != SerializerReturnCode.ok){
        return;
    }

    Test obj = Test([1, 2, 3]);
    SerializerInput input = SerializerInput(&obj);
    SerializerOutput output;
    assert(mser_serialize(handle, &input, &output) == SerializerReturnCode.ok);
    scope(exit)free(output.out_buffer);
    assert(g_counter_save == 3);

    if(mser_has_properties(handle, SerializerProperties.canDeserialize) != SerializerReturnCode.ok){
        return;
    }

    Test obj22;
    DeserializerInput input_de = DeserializerInput(&obj22, null, 0, output.out_buffer, output.out_buffer_length);
    DeserializerOutput output_de;
    assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);
    scope(exit)free(obj22.arr.ptr);

    assert(obj22.arr.length == 3);
    assert(obj22.arr[0] == 1);
    assert(obj22.arr[1] == 2);
    assert(obj22.arr[2] == 3);
    assert(g_counter_load == 3);
}

