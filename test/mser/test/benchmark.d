module mser.test.benchmark;

import core.stdc.stdlib : free;
import std.conv;
import std.datetime;
import std.stdio;

import mser;
import mser.test.game_scene;
import mser.test.structs;


SerializerBackend[] all_serializers = [
    SerializerBackend.binary,
    // SerializerBackend.cjson,
    // SerializerBackend.libxml2,
    SerializerBackend.binary_compatible,
];

void saveResult(BenchmarkDataArray bench, string path){
    import std.file;
    BenchmarkDataArray bench_old;
    {
        string file = readText(path);

        SerializerHandle handle;
        create_serializer(&handle, &g_meta_benchmark, SerializerBackend.cjson);
        scope(exit)mser_destroy(&handle);

        DeserializerInput input_de = DeserializerInput(&bench_old, null, 0, cast(ubyte*)file.ptr, file.length);
        DeserializerOutput output_de;
        assert(mser_deserialize(handle, &input_de, &output_de) == SerializerReturnCode.ok);
    }
    bench.data = bench_old.data ~ bench.data;
    SerializerHandle handle;
    create_serializer(&handle, &g_meta_benchmark, SerializerBackend.cjson);
    scope(exit)mser_destroy(&handle);

    SerializerInput input = SerializerInput(&bench);
    SerializerOutput output;
    mser_serialize(handle, &input, &output);
    scope(exit)free(output.out_buffer);

    write(path, output.out_buffer[0..output.out_buffer_length]);

    foreach(ref data; bench_old.data){
        free(cast(void*)data.name.ptr);
        free(cast(void*)data.serializer.ptr);
    }
    free(bench_old.data.ptr);
}



BenchmarkData[2] testImpl(SerializerBackend imp){
    auto dt = Clock.currTime();
    BenchmarkData[2] bench;
    bench[0].name = "SerializeGameScene";
    bench[0].date = dt.toUnixTime();
    bench[0].serializer = imp.to!string ~ "\0";

    bench[1] = bench[0];

    bench[0].iterationNum = 10;
    bench[1].iterationNum = 10;

    benchmarkSerializeGameScene(imp, bench);

    bench[0].process_time_ns = bench[0].process_time_total_ns / bench[0].iterationNum;
    bench[1].process_time_ns = bench[1].process_time_total_ns / bench[1].iterationNum;
    // writeln(bench);
    return bench;
}

void main(){
    BenchmarkDataArray res;
    foreach(i; 0..10){
        foreach(imp; all_serializers){
            BenchmarkData[2] bench = testImpl(imp);
            res.data ~= bench;
        }
    }
    saveResult(res, "test/benchmark_result.json");
}