import matplotlib.pyplot as plt
import json
file_data = json.loads(open("test/benchmark_result.json", "r").read())
vv = file_data['data']
data_serialize = {}
data_deserialize = {}

for i, el in enumerate(vv):
    data = data_serialize
    if el['serialize'] == 0:
        data = data_deserialize

    if el['serializer'] not in data:
        data[el['serializer']] = {'x':[], 'y':[]}

    data[el['serializer']]['x'].append(len(data[el['serializer']]['x']))
    data[el['serializer']]['y'].append(el['process_time_ns']/1000)


fig, (ax1, ax2) = plt.subplots( 2, 1 )

for el in data_serialize:
    if 'binary' not in el:
        continue
    ax1.plot(data_serialize[el]['x'], data_serialize[el]['y'], label=el)


for el in data_deserialize:
    if 'binary' not in el:
        continue
    ax2.plot(data_deserialize[el]['x'], data_deserialize[el]['y'], label=el)

ax1.legend(loc="upper right")
ax1.set_title("Serialize")
ax1.set(ylabel='Execution time [us]')
ax1.set_ylim(bottom=0)

ax2.set_title("Deserialize")
ax2.set(xlabel='Nr', ylabel='Execution time [us]')
ax2.set_ylim(bottom=0)


fig.set_size_inches(5, 7)
fig.savefig('benchmark.png')
plt.close(fig)
