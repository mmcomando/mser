#! /usr/bin/env nix-shell
#! nix-shell -i bash --pure -p python39Full python39Packages.pandas python39Packages.numpy python39Packages.matplotlib



set -euo pipefail
IFS=$'\n\t'


# Draw performance graph
python3 ./test/make_graphs.py