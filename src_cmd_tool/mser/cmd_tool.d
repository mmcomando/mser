module mser.cmd_tool;

import core.memory;
import core.stdc.stdio : printf;
import core.stdc.stdlib : malloc, free, exit;
import std.file : write;
import std.getopt;
import std.stdio : writeln, writefln, File;

import mser;


extern(C) SerializerMeta mser_embedded_binary_compatible_get_meta(ubyte* data_ptr, size_t data_length);

enum OutputFormat{
    none,
    json,
    xml,
}
__gshared SerializerMeta g_metadata;


int main(string[] args)
{
    GC.disable();
    writeln(args);

    string input;
    string output;
    OutputFormat out_format;

    auto helpInformation = getopt(
        args,
        "input",    &input,
        "output",    &output,
        "out_format",    &out_format,
    );

    if (helpInformation.helpWanted)
    {
    defaultGetoptPrinter("Program to convert data from one format to another. Currently only binary_compatible input format is supported as input. Output migth be json or xml.",
        helpInformation.options);
        return 0;
    }

    if(input == null || output == null || out_format == OutputFormat.none){
        writeln("Not all parameters provided. Required parameters: input, output, out_format. Ex. --input=in.bin --output=out.json --out_format=json");
    }

    writeln("Parameters:");
    writeln(input);
    writeln(output);
    writeln(out_format);


    auto file = File(input, "r");
    ubyte[] buffer = new ubyte[file.size()];
    ubyte[] input_data = file.rawRead(buffer);
    // string input_data = readText(input);
    g_metadata = mser_embedded_binary_compatible_get_meta(cast(ubyte*)input_data, input_data.length);
    transform_meta(g_metadata);

    SerializerHandle handle_deserialize;
    MserCreateInput create_input = MserCreateInput(&g_metadata);
    SerializerReturnCode ret_code = mser_create_embedded_binary_compatible(&handle_deserialize, &create_input);
    if(ret_code != SerializerReturnCode.ok){
        writeln("Failed to create handle_deserialize.");
        return 1;
    }
    scope(exit)mser_destroy(&handle_deserialize);

    SerializerHandle handle_serialize;
    final switch(out_format){
    case OutputFormat.json:
        ret_code = mser_create_cjson(&handle_serialize, &create_input);
        break;
    case OutputFormat.xml:
        ret_code = mser_create_libxml2(&handle_serialize, &create_input);
        break;
    case OutputFormat.none:
        writeln("Out format none not supported");
        return 1;
    }

    if(ret_code != SerializerReturnCode.ok){
        writeln("Failed to create handle_serialize.");
        return 1;
    }
    scope(exit)mser_destroy(&handle_serialize);


    // ubyte* input_object_ptr = cast(ubyte*)malloc(1024);
    // ubyte[] input_object = input_object_ptr[0..1024];
    ubyte[1024] input_object;
    DeserializerInput input_de = DeserializerInput(input_object.ptr, null, 0, cast(ubyte*)input_data, input_data.length);
    DeserializerOutput output_de;
    ret_code = mser_deserialize(handle_deserialize, &input_de, &output_de);
    if(ret_code != SerializerReturnCode.ok){
        writeln("Failed to deserialize data from input.");
        return 1;
    }



    SerializerInput input_se = SerializerInput(input_object.ptr);
    SerializerOutput output_se;
    ret_code = mser_serialize(handle_serialize, &input_se, &output_se);
    if(ret_code != SerializerReturnCode.ok){
        writeln("Failed to serialize data.");
        return 1;
    }
    scope(exit)mser_free(output_se.out_buffer);

    if(mser_has_properties(handle_serialize, SerializerProperties.isBinary) == SerializerReturnCode.propertiesNotSupported){
        printf("%s\n", cast(char*)output_se.out_buffer);
    }else{
        writeln(output_se.out_buffer[0..output_se.out_buffer_length]);
    }

    write(output, output_se.out_buffer[0..output_se.out_buffer_length]);
    writefln("Saved to %s", output);
    free_pointer_map();
    free_element(g_metadata, 0, input_object[], false);
    return 0;
}


union SliceUnion{
    struct SliceElements {
        size_t length;
        void* ptr;
    }
    ubyte[] slice;
    SliceElements el;
}

void free_element(ref SerializerMeta meta, int meta_index, ubyte[] data, bool free_this = true, const(char)[] name = ""){
    auto element = meta.meta_elements[meta_index];
    // writefln("free_element(%s) id(%s) type(%s) size(%s)",name, element.id, element.type, element.size);
    if(element.type == SerializerElementType.array){
        auto data_ref = element.subobjects[0];
        SerializerMetaElement* data_element = &g_metadata.meta_elements[data_ref.meta_index];
        ubyte[] array_data = *cast(ubyte[]*)data.ptr;
        SliceUnion helper;
        helper.slice = array_data;
        int ss = data_element.size;
        foreach(i; 0..helper.el.length){
            free_element(meta, data_ref.meta_index, array_data.ptr[i*ss..(i+1)*ss], false, [cast(char)('0')]);
        }
        mser_free(array_data.ptr);
    }
    if(element.type == SerializerElementType.arrayFixed){
        auto data_ref = element.subobjects[0];
        SerializerMetaElement* data_element = &g_metadata.meta_elements[data_ref.meta_index];
        int ss = data_element.size;
        size_t length = element.size / data_element.size;
        foreach(i; 0..length){
            free_element(meta, data_ref.meta_index, data.ptr[i*ss..(i+1)*ss], false, [cast(char)('0')]);
        }
    }
    if(element.type == SerializerElementType.string){
        ubyte[] ptr = *cast(ubyte[]*)data.ptr;
        mser_free(ptr.ptr);
    }
    if(element.type == SerializerElementType.binary){
        ubyte[] ptr = *cast(ubyte[]*)data.ptr;
        mser_free(ptr.ptr);
    }
    if(element.type == SerializerElementType.container){
        foreach(ref sub; element.subobjects){
            free_element(meta, sub.meta_index, data[sub.offset..$], false, sub.name);
        }
    }
    if(free_this){
        mser_free(data.ptr);
    }
}





// Lower types like cusomArray to simple array, so we can load them without callbacks, they should have the same binary representation in file
void lower_to_simpler_type(ref SerializerMetaElement meta){
    SerializerSubobject[] subobjects_realloc(SerializerSubobject[] subs, size_t new_length){
        SerializerSubobject* subobjects_ptr = cast(SerializerSubobject*)mser_malloc(new_length * SerializerSubobject.sizeof);
        SerializerSubobject[] new_subs = subobjects_ptr[0..new_length];

        foreach(i, ref sub; subs){
            SerializerSubobject* sub_new = subobjects_ptr + i;
            *sub_new = sub;
        }
        free(subs.ptr);
        return new_subs;
    }

    if(meta.type == SerializerElementType.arrayCustom || meta.type == SerializerElementType.stringCustom ||  meta.type == SerializerElementType.binaryCustom_reserved){
        meta.subobjects = subobjects_realloc(meta.subobjects, 2);
        meta.subobjects[0].offset = 8; // Pointer location
        meta.subobjects[1].offset = 0; // Length location
        meta.size = 16;
        if(meta.type == SerializerElementType.arrayCustom){
            meta.type = SerializerElementType.array;
        }
        if(meta.type == SerializerElementType.stringCustom){
            meta.type = SerializerElementType.string;
        }
        if(meta.type == SerializerElementType.binaryCustom_reserved){
            meta.type = SerializerElementType.binary;
        }
    }

}

// Makes few operations to meta so any input data can be deserialized to runtime data and then serialized
// 1. Make custom offsets as offsets in meta might point to data which is used in user callbacks and does'n point to particular type
// 2. Change custom types to simpler types
// 3. Implement callbacks where necessary
void transform_meta(ref SerializerMeta metas){
    foreach(ref meta; metas.meta_elements){
        if(meta.type != SerializerElementType.arrayFixed){
            continue;
        }
        // arrayFixed is special as number of elements is computed usind size of data type, but it might change
        // so we have to store somewhere this number of elements before we do recompute sizes
        // store it in subojects[0].offset as it is not used in arrayFixed
        SerializerMetaElement* submeta = &g_metadata.meta_elements[meta.subobjects[0].meta_index];
        int elements_num = meta.size / submeta.size;
        meta.subobjects[0].offset = elements_num;
        meta.size = -1;
    }
    // Set size for variables we know about and reset for this we don't know
    foreach(ref meta; metas.meta_elements){
        switch(meta.type){
        case SerializerElementType.none:
            meta.size = 0;
            break;
        case SerializerElementType.signedInteger:
        case SerializerElementType.unsignedInteger:
        case SerializerElementType.floatingPointNumber:
        case SerializerElementType.binaryFixed:
        case SerializerElementType.stringFixed_reserved:
            break; // Size is already ok

        case SerializerElementType.containerCustom: // Special case, here we use callbacks but we need uniq location for this object
        case SerializerElementType.pointer:
            meta.size = size_t.sizeof;
            break;
        case SerializerElementType.binary:
        case SerializerElementType.array:
        case SerializerElementType.string:
            meta.size = size_t.sizeof * 2;
            meta.subobjects[0].offset = 8; // Pointer location
            meta.subobjects[1].offset = 0; // Length location
            break;
        case SerializerElementType.container:
            foreach(ref subobject; meta.subobjects){
                subobject.offset = -1; // Reset offset
            }
            meta.size = -1;
            break;
        case SerializerElementType.arrayFixed: // Handled in previous loop
            break;
        default:
            assert(0, "Unknown type");
        }
    }
    foreach(ref meta; metas.meta_elements){
        lower_to_simpler_type(meta);
    }
    update_offsets(metas);
    set_callbacks(metas);
}

void update_offsets(ref SerializerMeta metas){

    foreach(i; 0..1000){ // Iteration to prevent infinite loop
        bool all_offsets_computed = true;
        foreach(ref meta; metas.meta_elements){
            if(meta.size == -1){
                all_offsets_computed = false;
                break;
            }
        }
        if(all_offsets_computed){
            return; // Success
        }
        foreach(ref meta; metas.meta_elements){
            update_offsets_meta(meta);
        }
    }
    writeln("Can't compute data offsets. Probably due to cyclic dependency.");
    exit(1);
}

void update_offsets_meta(ref SerializerMetaElement meta){
    bool all_sub_have_size(SerializerSubobject[] subobjects){
        foreach(ref sub; subobjects){
            SerializerMetaElement* submeta = &g_metadata.meta_elements[sub.meta_index];
            if(submeta.size == -1){
                return false;
            }
        }
        return true;
    }
    if(meta.size != -1){
        return; // Already computed
    }
    if(all_sub_have_size(meta.subobjects) == false){
        return; // Ignore
    }
    if(meta.type == SerializerElementType.arrayFixed){
        SerializerMetaElement* submeta = &g_metadata.meta_elements[meta.subobjects[0].meta_index];
        int elements_num = meta.subobjects[0].offset;
        meta.size = elements_num * submeta.size;
        return;
    }
    assert(meta.type == SerializerElementType.container);

    int offset;
    int total_size;
    foreach(ref sub; meta.subobjects){
        sub.offset = offset;

        SerializerMetaElement* submeta = &g_metadata.meta_elements[sub.meta_index];
        offset += submeta.size;
        total_size += submeta.size;
    }
    meta.size = total_size;
}






struct PointerDataContainerCustom{
    MetaIndex meta_index;
    void*[MetaIndex] map;
}

__gshared PointerDataContainerCustom[void*] pointer_map;

void free_pointer_map(){
    foreach(key_value; pointer_map.byKeyValue()){
        SerializerMetaElement* meta = &g_metadata.meta_elements[key_value.value.meta_index];
        foreach(key_value2; key_value.value.map.byKeyValue()){
            SerializerMetaElement* meta_element = &g_metadata.meta_elements[meta.subobjects[key_value2.key].meta_index];
            ubyte* data = cast(ubyte*)key_value2.value;
            // free(data);
            free_element(g_metadata, meta_element.meta_index, data[0..1024]);
        }
    }
    pointer_map = null;
}


extern(C) void on_load_container_custom(DeserializeContainerCustomInput* input){
    SerializerMetaElement* meta = &g_metadata.meta_elements[input.cd.element_index];
    PointerDataContainerCustom* pointer_data = &pointer_map.require(input.cd.runtime_data_ptr, PointerDataContainerCustom());
    pointer_data.meta_index = input.cd.element_index;

    foreach(i; 0..input.length){
        MetaIndex name_index;
        input.deserialize_name(input.cd.context, &name_index);
        SerializerMetaElement* meta_load = &g_metadata.meta_elements[meta.subobjects[name_index].meta_index];
        void* data_load = malloc(meta_load.size);
        foreach(ref b; (cast(ubyte*)data_load)[0..meta_load.size])b=0;
        pointer_data.map[name_index] = data_load;
        input.deserialize_element(input.cd.context, data_load);
    }
}

extern(C) void on_save_container_custom(SerializeContainerCustomInput* input){
    SerializerMetaElement* meta = &g_metadata.meta_elements[input.cd.element_index];
    PointerDataContainerCustom* pointer_data = &pointer_map[input.cd.runtime_data_ptr];

    input.serialize_start(input.cd.context, pointer_data.map.length);

    foreach(key_value; pointer_data.map.byKeyValue()){
        input.serialize_element(input.cd.context, key_value.key, key_value.value);
    }
}

void set_callbacks(ref SerializerMeta metas){
    foreach(ref meta; metas.meta_elements){
        if(meta.type == SerializerElementType.containerCustom){
            meta.on_load_container = &on_load_container_custom;
            meta.on_save_container = &on_save_container_custom;
        }
    }
}
