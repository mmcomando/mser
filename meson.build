project('mser', 'd', version: '0.5.0')

src = files(
  'src/mser/backend/binary_compatible.d',
  'src/mser/backend/binary.d',
  'src/mser/backend/cjson.d',
  'src/mser/backend/json.d',
  'src/mser/backend/libxml2.d',
  'src/mser/backend/toml.d',
  'src/mser/bindings/cjson.d',
  'src/mser/bindings/libxml2.d',
  'src/mser/serializer_helper.d',
  'src/mser/serializer_impl.d',
  'src/mser/serializer_template.d',
)


inc = include_directories(
    'src'
)

inc_pub = include_directories(
    'inc'
)


libxml_2_0_dep = dependency('libxml-2.0', required : false)
libcjson_dep = dependency('libcjson')


lib_args =  [
    '-betterC',
    '-checkaction=C',
]

test_args = [
    '-checkaction=context',
]

link_args = []

compiler = meson.get_compiler('d')

if compiler.get_id() == 'llvm'
  add_project_arguments('--verror-style=gnu', language : 'd')
endif

if get_option('enable_asan')
  # Meson doesn't support '-Db_sanitize=address' for ldc so we have to manually add sanitizer flags
  lib_args += ['-fsanitize=address']
  test_args += ['-fsanitize=address']
  link_args += ['-fsanitize=address', '-lasan']
endif

if get_option('enable_fuzzing')
  lib_args += ['-fsanitize=fuzzer']
endif


mser_lib = library('mser', src,
  link_args : link_args,
  include_directories : [inc, inc_pub],
  d_module_versions : 'mser_backend_implementation',
  d_args : lib_args,
  dependencies: [
    libcjson_dep,
    libxml_2_0_dep,
  ]
)

mser_dep = declare_dependency(
  link_with : mser_lib,
  sources: ['inc/mser/package.d'],
  include_directories : inc_pub,
)


meson.override_dependency('mser', mser_dep)

if get_option('build_tests')
  subdir('test')
endif

if get_option('build_cmd_tool')
  subdir('src_cmd_tool')
endif